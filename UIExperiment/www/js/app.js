// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('uiExperiment', ['ionic', 'ngAnimate', 'ngCordova'])

.config(function($stateProvider, $urlRouterProvider) {

  $stateProvider
  .state('home', {
    url: '/home', 
    templateUrl: 'home.html'
  })
  .state('user-info-search', {
    url: "/user-info-search",
    templateUrl: "user-info-search.html"
  })
  .state('questionnaire', {
    url: "/questionnaire", 
    templateUrl: 'questionnaire.html'
  })
  .state('experiment-kb', {
    url: "/experiment-kb", 
    templateUrl: 'experiment-kb.html'
  })
  .state('about', {
    url: "/about", 
    templateUrl: "about.html"
  })
  .state('authors', {
    url: "/authors", 
    templateUrl: "authors.html"
  })
  .state('post-questionnaire', {
    url: "/post-questionnaire", 
    templateUrl: "post-questionnaire.html"
  })
  .state('info', {
    url: "/info", 
    templateUrl: "info.html"
  });

  $urlRouterProvider.otherwise("/home");

})

.controller('experimentController', function($scope, $ionicSideMenuDelegate, $timeout, $interval, $ionicModal, $state, $ionicHistory, $http, $timeout, $compile) {

  // GLOBALS
  $scope.titles = {
    page_1: "CSC428 Text UI Experiment", 
    page_2: "Questionnaire", 
    page_3: "Text Input", 
    page_4: "Experiment Completed"
  }

  // root to make REST API calls
  $scope.restUrlRoot = "http://csc428-ui-experiment.herokuapp.com/"; // production server
  $scope.testUrl = "http://localhost:3000/"; // local dev/test server

  // --------------------------------------------------
  // IMPORTANT
  // current URL to use
  $scope.CURRENT_URL = $scope.restUrlRoot;
  // --------------------------------------------------

  $scope.buttonClicked = false;

  // initial keyboard text
  $scope.initialKbText = "";

  // corpus text
  $scope.corpusText = "He is just like everyone else except better There is great disturbance in the force Love means never having to say you are sorry";
  $scope.corpusTextArray = $scope.corpusText.replace(/([a-z])\s*([A-Z])/g, "$1|$2").split("|"); // split string into array of sentences
  $scope.corpusTextArrayIndex = 0;
  var firstText = $scope.corpusTextArray[$scope.corpusTextArrayIndex];
  firstText = firstText.toLowerCase();
  $scope.sentence = {
    content: firstText
  }

  $scope.arrayProps = {
    size: $scope.corpusTextArray.length
  }

  // increment by one
  $scope.corpusTextArrayIndex = $scope.corpusTextArrayIndex + 1;

  $scope.timerStarted = false;

  // start time and end time
  $scope.startTime;
  $scope.endTime;

  // go back to home page
  $scope.goHome = false;

  // array to keep track of the sentences the user has typed so far
  $scope.userSentences = [];

  // ------------------------------------------------------------------------------------
  // PARTICIPANT DATA

  $scope.inputType = -1; // keyboard or voice value
  $scope.gender = -1; // participant self-identified gender
  $scope.age = -1; // age range
  $scope.wpm = -1; // words per minute
  $scope.correctEntryPct = -1; // correct entry percentage (accuracy)

  $scope.userId = -1; // user id

  // post-questionnaire data
  $scope.mentalDemand = -1;
  $scope.physicalDemand = -1;
  $scope.temporalDemand = -1;
  $scope.performance = -1;
  $scope.effort = -1;
  $scope.frustration = -1;

  // ------------------------------------------------------------------------------------

  // MODALS

  // create the modal "$scope.errorModal"
  $ionicModal.fromTemplateUrl('error-modal.html', function(modal) {
    $scope.errorModal = modal;
  }, {
    scope: $scope
  });

  // create the modal "$scope.ajaxErrorModal"
  $ionicModal.fromTemplateUrl('ajax-error-modal.html', function(modal) {
    $scope.ajaxErrorModal = modal;
  }, {
    scope: $scope
  });

  // ------------------------------------------------------------------------------------

  /*
  * AJAX call to get the next available id for use when creating users and return the response
  */
  $scope.getNextAvailableUserId = function() {

    return {
      getResult: function() {
        return $http({
          method: 'POST',
          url: $scope.CURRENT_URL + "users/get_next_available_id/",
        }).then(function mySucces(response) {
          console.log("Successful response");
          console.log("Response:");
          console.log(response);

          return response;
        }, function myError(response) {
          console.log("Failed response");
          console.log("Response:");
          console.log(response);

          return response;
        });
      }
    }
  }

  /*
  * AJAX call to get user information from a first and last name
  */
  $scope.getUserInfo = function(first_name, last_name) {

    return {
      getResult: function() {

        // make an AJAX call to the REST API
        return $http({
          method: 'POST',
          url: $scope.CURRENT_URL + "users/get_info_by_name/",
          data: {
            "first_name": first_name,
            "last_name": last_name
          }
        }).then(function mySucces(response) {
          console.log("Successful response");
          console.log("Response:");
          console.log(response);

          return response;
        }, function myError(response) {
          console.log("Failed response");
          console.log("Response:");
          console.log(response);

          return response;
        });
      }
    }    
  }

  /*
  * AJAX call to create a new user
  */
  $scope.createUser = function(first_name, last_name) {

    return {
      getResult: function() {

        console.log("url: " + $scope.CURRENT_URL + "users/");

        // make an AJAX call to the REST API
        return $http({
          method: 'POST',
          url: $scope.CURRENT_URL + "users/",
          data: {
            "first_name": first_name,
            "last_name": last_name
          }
        }).then(function mySucces(response) {
          console.log("Successful response");
          console.log("Response:");
          console.log(response);

          return response;
        }, function myError(response) {
          console.log("Failed response");
          console.log("Response:");
          console.log(response);

          return response;
        });
      }
    }
  }

  /*
  * AJAX call to send data to the REST API
  */
  $scope.sendExperimentData = function(input_type, user_id, gender, age, correct_entry_pct, w_p_m, mental_demand, physical_demand, temporal_demand, performance, effort, frustration) {

    // make an AJAX call to the REST API
    $http({
      method: 'POST',
      url: $scope.CURRENT_URL + "participants/",
      data: {
        "input_type": input_type, 
        "user_id": user_id,
        "gender": gender, 
        "age": age, 
        "correct_entry_pct": correct_entry_pct, 
        "wpm": w_p_m, 
        "mental_demand": mental_demand, 
        "physical_demand": physical_demand, 
        "temporal_demand": temporal_demand, 
        "performance": performance, 
        "effort": effort, 
        "frustration": frustration
      }
    }).then(function mySucces(response) {
      console.log("Successful response");
      console.log("Response:");
      console.log(response);
    }, function myError(response) {
      console.log("Failed response");
      console.log("Response:");
      console.log(response);
    });
  }

  /*
  * Close the error modal
  */
  $scope.closeErrorModal = function() {
    if ($scope.errorModal.isShown()) {
      $scope.errorModal.hide();
    }
  }

  /*
  * Close the AJAX error modal
  */
  $scope.closeAjaxErrorModal = function() {
    if ($scope.ajaxErrorModal.isShown()) {
      $scope.ajaxErrorModal.hide();
    }
  }

  /*
  * Event controller if "Search" button is clicked on the "User Select" page
  */
  $scope.searchButtonClicked = function() {

    // get first name input
    var firstNameInput = document.getElementById("first-name");
    var firstName = firstNameInput.value;

    // get last name input
    var lastNameInput = document.getElementById("last-name");
    var lastName = lastNameInput.value;

    if (firstName === "" && lastName === "") {

      // show error message
      $scope.errorModal.show();

    } else {

      $scope.getUserInfo(firstName, lastName).getResult().then(function(response) {

        // if both fields are empty
        // NOTE: We can accept one field empty in this case
        if (response.status == 200 && response.data.code == 1) {

          var validUsers = response.data.payload.valid_users;

          $scope.populateSearchResults(validUsers);

        } else {
          console.log("DAVID: AJAX response was not successful");

          $scope.ajaxErrorModal.show();
        }
      });
    }
  }

  /*
  * Event controller if user is selected from the "Search Results" list
  */
  $scope.userSelect = function(id) {

    $scope.userId = id;

    $scope.goToQuestionnaire();
  }

  /*
  * Populate the .results-list item with search results from the AJAX call in $scope.searchButtonClicked()
  */
  $scope.populateSearchResults = function(validUsersArray) {

    // remove existing search results
    $scope.clearSearchResults();

    // get the list
    var resultsList = document.getElementById("results-list");

    // get the divider
    var header = document.getElementById("search-results-divider");

    for (var i = 0; i < validUsersArray.length; i++) {
      
      var item = document.createElement("a");
      item.href = "#";
      item.id = "user_" + validUsersArray[i].id;
      item.className = "item ng-binding ng-scope";
      item.innerHTML = validUsersArray[i].first_name + " " + validUsersArray[i].last_name;
      item.setAttribute("ng-click", "userSelect(" + validUsersArray[i].id + ")");

      // // make "item" an angular component called "temp"
      var temp = $compile(item)($scope);

      // // append "temp" to the DOM
      angular.element(resultsList).append(temp);

      resultsList.appendChild(item);
    }
  }

  /*
  * Reset the search results to nothing
  */
  $scope.clearSearchResults = function() {

    // get the list
    var resultsList = document.getElementById("results-list");

    // remove all children from the list
    resultsList.innerHTML = "";

    // add the divider title
    var divider = document.createElement("div");
    divider.id = "search-results-divider";
    divider.className = "item item-divider";
    divider.innerHTML = "Search Results";

    // add the divider title
    resultsList.appendChild(divider);
  }
  

  /*
  * Event controller if "New User" button is clicked
  */
  $scope.newUserButtonClicked = function() {

    // get first name input
    var firstNameInput = document.getElementById("first-name");
    var firstName = firstNameInput.value;

    // get last name input
    var lastNameInput = document.getElementById("last-name");
    var lastName = lastNameInput.value;

    // if the user has not entered in both their first and last name
    if (firstName === "" || lastName === "") {

      // show error message
      $scope.errorModal.show();

    } else {

      // create the new user
      $scope.createUser(firstName, lastName).getResult().then(function(response) {

        // if we get the correct response when making the AJAX call
        if (response.status == 200 && response.data.code == 1) {

          // get the id for the new user
          $scope.userId = response.data.payload.id;

          // move to the questionnaire page
          $scope.goToQuestionnaire();

        } else {
          console.log("DAVID: AJAX response was not successful");

          $scope.ajaxErrorModal.show();
        }
      });
    }
  }

  /*
  * Check if the user is running a practice run or an experiment trial
  */
  $scope.checkIfPractice = function(practice) {

    // set the array of user completed sentences to the empty array
    $scope.userSentences = [];

    // set to the second sentence in the corpus because the first is automatically set in the view
    $scope.corpusTextArrayIndex = 1;

    // check if the user is running a practice run
    if (!practice) {
      $scope.startTimer();
    }
  }


$scope.resetText = function() {
  // clear the contents of the textarea
  $scope.clearTextArea("textarea-kb");

}
  /*
  * Change the sentence displayed to the user based on the contents of the $scope.corpusTextArray array
  */
  $scope.nextText = function() {

    // get the user's inputed text
    var textArea = $scope.getUserContent();
    textArea = textArea.toLowerCase();

    // add the user's completed sentence to the array
    $scope.userSentences.push(textArea);

    // clear the contents of the textarea
    $scope.clearTextArea("textarea-kb");

    // if there is at least one sentence left in the array
    if ($scope.corpusTextArrayIndex < $scope.corpusTextArray.length) {

      // move card out of view to the left
      var cardElement = document.getElementById("experiment-text-card");
      cardElement.className = "card animated fadeOutLeft";

      // create the new card content
      var newCardElement = document.createElement("div");
      newCardElement.id = "experiment-text-card";
      newCardElement.className = "card animated fadeInRight";

      // create the new progress div
      var newProgressElement = document.createElement("div");
      newProgressElement.id = "progress";
      newProgressElement.className = "item item-divider text-right";
      newProgressElement.innerHTML = "Progress: " + ($scope.corpusTextArrayIndex + 1) + "/" + $scope.arrayProps.size;

      // create the new experiment text
      var newExperimentText = document.createElement("div");
      newExperimentText.id = "experiment-text";
      newExperimentText.className = "item item-text-wrap";
      var nextText = $scope.corpusTextArray[$scope.corpusTextArrayIndex];
      nextText = nextText.toLowerCase();
      newExperimentText.innerHTML = nextText;

      // insert newExperimentText and newProgressElement into newCardElement
      newCardElement.appendChild(newProgressElement);
      newCardElement.appendChild(newExperimentText);

      // replace the old "experiment-text-card" id with the new element
      cardElement.parentNode.replaceChild(newCardElement, cardElement);

      $scope.corpusTextArrayIndex = $scope.corpusTextArrayIndex + 1;

    } else {

      // RESET EVERYTHING FOR NEXT TIME
      
      // reset the original sentence and index to zero for next time
      $scope.corpusTextArrayIndex = 0;
      var nextSentence = $scope.corpusTextArray[$scope.corpusTextArrayIndex];
      nextSentence = nextSentence.toLowerCase()
      $scope.sentence.content = nextSentence;

      // move card out of view to the left
      var cardElement = document.getElementById("experiment-text-card");
      cardElement.className = "card animated fadeOutLeft";

      // create the new progress div
      var newProgressElement = document.createElement("div");
      newProgressElement.id = "progress";
      newProgressElement.className = "item item-divider text-right";
      newProgressElement.innerHTML = "Progress: 1" + "/" + $scope.arrayProps.size;

      // create the new card content
      var newCardElement = document.createElement("div");
      newCardElement.id = "experiment-text-card";
      newCardElement.className = "card animated fadeInRight";

      // create the new experiment text
      var newExperimentText = document.createElement("div");
      newExperimentText.id = "experiment-text";
      newExperimentText.className = "item item-text-wrap";
      var nextSentence = $scope.corpusTextArray[$scope.corpusTextArrayIndex];
      nextSentence = nextSentence.toLowerCase()
      newExperimentText.innerHTML = nextSentence;

      // insert newExperimentText and newProgressElement into newCardElement
      newCardElement.appendChild(newProgressElement);
      newCardElement.appendChild(newExperimentText);

      // replace the old "experiment-text-card" id with the new element
      cardElement.parentNode.replaceChild(newCardElement, cardElement);

      // stop the timer
      $scope.stopTimer();
    }
  }

  /*
  * Get the keycode for the last character in a string using an Android-appropriate function
  */
  $scope.getKeyCode = function(str) {

    return str.charCodeAt(str.length - 1);
  }

  /*
  * Set the initial text for the textarea to an empty string if the user has not inputed any text
  */
  $scope.initializeTextArea = function(id) {

    // the text area element itself
    var textArea = document.getElementById(id);

    // only set to empty string if the user has not already started typing
    if (textArea.value.trim() == "") {
      textArea.value = "";
    }
  }

  /*
  * Reset the textarea to no content
  */
  $scope.clearTextArea = function(id) {

    // the text area element itself
    var textArea = document.getElementById(id);

    textArea.value = "";
  }

  /*
  * Start the timer for the experiment
  */
  $scope.startTimer = function() {

    // change the boolean value
    $scope.timerStarted = true;

    // log the start time
    $scope.startTime = new Date();

    // get the input type
    var inputDOM = document.getElementById("input-type-select");
    $scope.inputType = inputDOM.options[inputDOM.selectedIndex].value;

    // get the gender
    var genderDOM = document.getElementById("gender-select");
    $scope.gender = genderDOM.options[genderDOM.selectedIndex].value;

    // get the age category
    var ageDOM = document.getElementById("age-select");
    $scope.age = ageDOM.options[ageDOM.selectedIndex].value;

  }

  /*
  * Stop the timer for the experiment
  */
  $scope.stopTimer = function() {

    // if the user is actually conducting the experiment
    if ($scope.timerStarted) {

      // change the boolean value
      $scope.timerStarted = false;

      // log the end time
      $scope.endTime = new Date();

      // get the time difference (in minutes) between the start and end times
      var diff = $scope.getMinuteTimeDifference($scope.startTime, $scope.endTime);

      // get the user content from the array
      var content = $scope.mergeArrayContentsAsString($scope.userSentences);

      // set the global wpm value
      $scope.wpm = $scope.calculateWpm(content, diff);

      // set the global accuracy value
      $scope.correctEntryPct = $scope.calculateAccuracy(content);

      // go to the post-questionnaire
      // NOTE: The post-questionnaire has a button to take the user back to the home screen
      $scope.goToPostQuestionnaire();

    } else {

      // if the user is doing a practice session
      $scope.goToHome();

    }
    
  }

  /*
  * Function that is run when the user clicks the "Done" button on the post-questionnaire page
  */
  $scope.completeExperiment = function() {

    // get the mental demand
    var mentalDemandDOM = document.getElementById("mental-demand-select");
    $scope.mentalDemand = mentalDemandDOM.value;

    // get the physical demand
    var physicalDemandDOM = document.getElementById("physical-demand-select");
    $scope.physicalDemand = physicalDemandDOM.value;

    // get the temporal demand
    var temporalDemandDOM = document.getElementById("temporal-demand-select");
    $scope.temporalDemand = temporalDemandDOM.value;

    // get the performance
    var performanceDOM = document.getElementById("performance-select");
    $scope.performance = performanceDOM.value;

    // get the effort
    var effortDOM = document.getElementById("effort-select");
    $scope.effort = effortDOM.value;

    // get the frustration
    var frustrationDOM = document.getElementById("frustration-select");
    $scope.frustration = frustrationDOM.value;

    // if all variables have been set
    if ($scope.inputType != -1 && $scope.userId != -1 && $scope.gender != -1 && $scope.age != -1 && $scope.wpm != -1 && $scope.correctEntryPct != -1 && $scope.mentalDemand != -1 && $scope.physicalDemand != -1 && $scope.temporalDemand != -1 && $scope.performance != -1 && $scope.effort != -1 && $scope.frustration != -1) {

      console.log($scope.userId);

      // make the AJAX call
      $scope.sendExperimentData($scope.inputType, $scope.userId, $scope.gender, $scope.age, $scope.correctEntryPct, $scope.wpm, $scope.mentalDemand, $scope.physicalDemand, $scope.temporalDemand, $scope.performance, $scope.effort, $scope.frustration);
    }

    // go back to home screen
    $scope.goToHome();

  }

  /*
  * Concatenate all the string in the array as one string separated by spaces.
  */
  $scope.mergeArrayContentsAsString = function(arr) {

    return arr.join(" ");
  }

  /*
  * Get the content the user typed in the textarea
  */
  $scope.getUserContent = function() {

    // the textArea element in the DOM
    var textArea = document.getElementById("textarea-kb");

    // get the content the user typed
    var userContent = textArea.value.trim();

    return userContent;

  }

  /*
  * Given two "Date" objects, return the number of minutes in the interval
  */
  $scope.getMinuteTimeDifference = function(startTime, endTime) {

    var seconds = Math.abs((endTime.getTime() - startTime.getTime())/1000);

    return seconds / 60.0;
  }

  /*
  * Perform the arithmetic calculation of gross wpm
  *
  * Link: http://www.speedtypingonline.com/typing-equations
  */
  $scope.calculateWpm = function(userContent, minutes) {

    // ({number of characters} / 5) / number of minutes
    return (userContent.length / 5.0) / minutes;
  }

  /*
  * Perform the accuracy calculation
  *
  * Link: http://www.speedtypingonline.com/typing-equations
  */
  $scope.calculateAccuracy = function(userContent) {

    // array of all the words in the corpus text
    var lowerCorpus = $scope.corpusText;
    lowerCorpus = lowerCorpus.toLowerCase()
    var experimentTextArray = lowerCorpus.split(" ");
    // array of all the words the user entered
    var userContentArray = userContent.split(" ");

    var numWords = experimentTextArray.length;
    var correctWords = 0;

    // count the number of matching correct words between the user and the experiment text
    for (var i = 0; i < experimentTextArray.length; i++) {
      if (experimentTextArray[i] == userContentArray[i]) {
        correctWords = correctWords + 1;
      }
    }

    return correctWords / numWords * 100.0;

  }

  /*
  * Go to the quesitonnaire state
  */
  $scope.goToQuestionnaire = function() {

    console.log("$scope.userId: " + $scope.userId);

    // go to the questionnaire state
    $state.go("questionnaire");
  }

  /*
  * Go to the post-questionnaire state
  */
  $scope.goToPostQuestionnaire = function() {

    // disable the back button when going to questionnaire state
    $ionicHistory.nextViewOptions({
      disableBack: true
    });

    // go to the post-questionnaire state
    $state.go("post-questionnaire");
  }

  /*
  * Go back to initial home state
  */
  $scope.goToHome = function() {
    
    // disable the back button when going back to home state
    $ionicHistory.nextViewOptions({
      disableBack: true
    });

    // go to initial home state
    $state.go("home");
    
    // remove the cache and clear the history within ionic framework of visited pages
    $ionicHistory.clearCache();
    $ionicHistory.clearHistory();
  }

});

