<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {
	
	public function index() {
		$this->login();
	}
	
	public function login() {
		$this->load->view('login_view');
	}
	
	public function logout() {
		// delete the cookie
		$this->session->sess_destroy();
		
		// redirect to login page
		redirect (base_url());
	}
	
	public function validateLogin() {
		
		$this->load->helper('url');
		$this->load->helper('form');
		
		$this->load->library('form_validation');
		
		// validate the input
		
		$this->form_validation->set_rules('email','Email','required|trim|xss_clean|callback_validate_credientials');
		
		$this->form_validation->set_rules('password','Password','required|md5|trim');
		
		if ($this->form_validation->run()) {
			
			$this->load->model('main_model');
			
			// check if admin
			
			// get the username from the form
			$adminDataIn['username'] = $this->input->post('email');
			
			// run query to check if user is admin
			$adminData['username'] = $this->main_model->getAdmin($adminDataIn);
			
			// run query to get the user's uid
			$uid = $this->main_model->getUid($adminDataIn);
			
			// Create the session info
			if ($adminData['username']->num_rows() > 0) {
				// the user exists and is an admin
				$data=array(
				'email'=>$this->input->post('email'),
				'uid' => $uid,
				'is_logged_in'=>1,
				'admin'=>1
				);
			} else {
				
				// the user is not an admin
				$data=array(
				'email'=>$this->input->post('email'),
				'uid' => $uid,
				'is_logged_in'=>1
				);
			}
			
			// set the cookie
			$this->session->set_userdata($data);
			
			// redirect to home view
			redirect (base_url()."home");
		} else {
			$this->load->view('templates/pleaseLogin');
		}
	}
	
	public function validate_credientials() {
		$this->load->model('main_model');
		
		if ($this->main_model->canLogIn()) {
			// connect to the database to validate username/password
			return true;
		} else {
			$this->form_validation->set_message('validate_credientials','Incorect 
				username/password');
			return false;
		}
	}
	
	public function signup() {
		$this->load->view('signup_view');
	}
	
	public function signup_validation() {
		
		$this->load->library('form_validation');
		
		//input form 'email' must be an valid email address and cannot already exist in the database
		$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[User.email]');
		
		$this->form_validation->set_rules('password','Password','required|trim');
		
		$this->form_validation->set_rules('cpassword','Confirm Password','required|trim|matches[password]');
		
		// override the default meaningless error message
		$this->form_validation->set_message('is_unique','That email address already exists.');
		
		if ($this->form_validation->run()) {
			
			// load the model
			$this->load->model('main_model');
			
			//generate a random key
			$key=md5(uniqid());
			
			// add user to the database
			if ($this->main_model->add_user($key)) {
				//echo "User added to the database";
				$this->load->view('templates/signupSuccess');
			} else {
				echo "Problem adding user to the database";
			}
		} else {
			$this->load->view('signup_view');
		}
	}
}
