<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Idea extends CI_Controller {
	
	public function index() {
		// session check
		if ($this->session->userdata('is_logged_in')) {
			$this->loadView();
		} else {
			$this->load->view('templates/pleaseLogin');
		}
	}
	
	public function loadView() {
		// session check
		if ($this->session->userdata('is_logged_in')) {
			
			$this->load->model('idea_model');
			
			$data['industries'] = $this->idea_model->getIndustries();
			
			$this->load->view('submitIdea_view', $data);
		} else {
			$this->load->view('templates/pleaseLogin');
		}
	}
	
	public function submit() {
		// session check
		if ($this->session->userdata('is_logged_in')) {
						
			$this->load->library('form_validation');
			
			// set validation rules
			$this->form_validation->set_rules('title', 'Title', 'required|trim');
			$this->form_validation->set_rules('keyword1', 'Keyword1', 'trim');
			$this->form_validation->set_rules('keyword2', 'Keyword2', 'trim');
			$this->form_validation->set_rules('keyword3', 'Keyword3', 'trim');
			$this->form_validation->set_rules('keyword4', 'Keyword4', 'trim');
			$this->form_validation->set_rules('description', 'Description', 'required|trim');
			
			if ($this->form_validation->run()) {
				
				// if there are no errors with the validation
				
				// get all the values from the input fields
				// and sanitize the input
				
				$this->load->helper('string');
				
				$data['title'] = quotes_to_entities($this->input->post('title'));
				$data['industry'] = $this->input->post('industry');
				$data['keyword1'] = quotes_to_entities(ucfirst(strtolower($this->input->post('keyword1'))));
				$data['keyword2'] = quotes_to_entities(ucfirst(strtolower($this->input->post('keyword2'))));
				$data['keyword3'] = quotes_to_entities(ucfirst(strtolower($this->input->post('keyword3'))));
				$data['keyword4'] = quotes_to_entities(ucfirst(strtolower($this->input->post('keyword4'))));
				$data['description'] = quotes_to_entities($this->input->post('description'));
				
				// load the model
				$this->load->model('idea_model');
				
				// if the data does not exist in the database already
				// under a different iid
				if ($this->myIdeaSubmitCallback($data)) {
					
					// create the Idea in the database
					$this->idea_model->createIdea($data);
					
					// get the iid of the idea we just created
					$iid = $this->idea_model->getIid($data);
					
					// add the keywords for the iid into the database
					$data['iid'] = $iid;
					$this->idea_model->createKeywords($data);
					
					$this->load->view('templates/ideaSuccess');
					
				} else {
					echo "Data already exists in the database!";
					echo "<br>";
					echo "Need to convey error message better";
				}
			} else {
				// redirect to the same page with an error message
				$this->loadView();
			}
		} else {
			$this->load->view('templates/pleaseLogin');
		}
	}
	
	// check if the user has already created the exact same idea
	// under a different iid
	//
	// return true if check passes and the idea has not been created
	// under a different iid
	public function myIdeaSubmitCallback($dataIn) {
		// session check
		if ($this->session->userdata('is_logged_in')) {
			$this->load->model('idea_model');
			
			if (! $this->idea_model->ideaAlreadyExists($dataIn)) {
				return true;
			} else {
				return false;
			}
			
		} else {
			$this->load->view('templates/pleaseLogin');
		}
	}
	
	// need to put $iid = null so that this always enters
	// as a security measure, I check later whether it is actually
	// set to a variable
	public function render($iid = null) {
		// session check
		if ($this->session->userdata('is_logged_in')) {
			
			if (isset($iid)) {
				
				// $iid is a string and is not an array
				
				$this->load->model('idea_model');
				
				// check if this is a valid iid
				if ($this->idea_model->iidExists($iid)) {
				
					$data['results'] = $this->idea_model->getIidInfo($iid);
					$data['numLikes'] = $this->idea_model->getNumLikes($iid);
					$data['numDislikes'] = $this->idea_model->getNumDislikes($iid);
					
					$this->load->view('ideaPage_view', $data);
				} else {
					$this->load->view('templates/ideaNotExistError');
				}
				
			} else {
				$this->load->view('templates/ideaNotExistError');
			}
			
		} else {
			$this->load->view('templates/pleaseLogin');
		}
	}
	
	public function rate($iid = null) {
		
		// session check
		if ($this->session->userdata('is_logged_in')) {
			
			if (isset($iid)) {
				
				// get form data
				$data['iid'] = $iid;
				$data['uid'] = $this->session->userdata('uid');
				$data['like'] = $this->input->get('like');
				$data['dislike'] = $this->input->get('dislike');
				
				// check if the user is allowed to rate the idea
				// users cannot rate their own ideas
				// need to compare session uid with the user who posted the idea
				
				$this->load->model('idea_model');
				
				if (!$this->idea_model->posterIsCurrUser($data['iid'])) {
					// poster is not the current user
					// user can rate the idea
					
					// set values to the database
					if ($data['like'] == 1) {
						
						$this->idea_model->rateLikeIdea($data);
						
						$this->load->view('templates/ideaRateSuccess', $data);
						
					} else if ($data['dislike'] == 1) {
						
						$this->idea_model->rateDislikeIdea($data);
						
						$this->load->view('templates/ideaRateSuccess', $data);
						
					} else {
						echo "Error in setting rate value to the database";
					}
					
				} else {
					// poster is the current user
					// user cannot rate the idea
					$this->load->view('templates/ideaRateError');
				}	
			} else {
				// load an error message
				$this->load->view('templates/ideaNotExistError');
			}
		} else {
			$this->load->view('templates/pleaseLogin');
		}
		
	}
}
