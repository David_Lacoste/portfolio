<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Analytics extends CI_Controller {
	
	public function index() {
		// session check
		if ($this->session->userdata('is_logged_in')) {
			$this->loadView();
		} else {
			$this->load->view('templates/pleaseLogin');
		}
	}
	
	public function loadView() {
		// session check
		if ($this->session->userdata('is_logged_in')) {
			$this->load->view('analytics_view');
		} else {
			$this->load->view('templates/pleaseLogin');
		}
	}
	
	public function wrapper() {
		// session check
		if ($this->session->userdata('is_logged_in')) {
			
			// get form data
			$data['numIdeas'] = $this->input->get('numIdeas');
			
			// call the "top" REST function with the right input
			$this->top($data['numIdeas']);
			
		} else {
			$this->load->view('templates/pleaseLogin');
		}
	}
	
	public function top($num = null) {
		// session check
		if ($this->session->userdata('is_logged_in')) {
			
			if (isset($num) && ctype_digit($num)) {
				
				// get form data
				$data['numIdeas'] = $num;
				$data['startYear'] = $this->input->get('startYear');
				$data['startMonth'] = $this->input->get('startMonth');
				$data['startDate'] = $this->input->get('startDate');
				$data['endYear'] = $this->input->get('endYear');
				$data['endMonth'] = $this->input->get('endMonth');
				$data['endDate'] = $this->input->get('endDate');
				
				$this->load->model('analytics_model');
				
				$data['results'] = $this->analytics_model->topIdeas($data);
				
				// convert $data['results'] to json and echo contents
				$jsonString = json_encode($data['results']);;
				
				echo $jsonString;
				
				
			} else {
				$this->load->view('templates/analyticsTopError');
			}
			
		} else {
			$this->load->view('templates/pleaseLogin');
		}
	}
	
	public function graph() {
		// session check
		if ($this->session->userdata('is_logged_in')) {
			
			$this->load->model('analytics_model');
			
			$data['graph'] = $this->analytics_model->getDistGraphData();
			
			$this->load->view('analytics_graph_view', $data);
			
		} else {
			$this->load->view('templates/pleaseLogin');
		}
	}
	
}
