<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Browse extends CI_Controller {
	
	public function index() {
		// session check
		if ($this->session->userdata('is_logged_in')) {
			$this->loadView();
		} else {
			$this->load->view('templates/pleaseLogin');
		}
	}
	
	public function loadView() {
		// session check
		if ($this->session->userdata('is_logged_in')) {
			
			$this->load->model('browse_model');
			
			$data['industries'] = $this->browse_model->getIndustries();
			
			$this->load->view('browse_view', $data);
		} else {
			$this->load->view('templates/pleaseLogin');
		}
	}
	
	public function filter() {
		// session check
		if ($this->session->userdata('is_logged_in')) {
			
			// get form data
			
			$data['title'] = quotes_to_entities($this->input->post('title'));
			$data['industry'] = $this->input->post('industry');
			$data['keyword1'] = quotes_to_entities(ucfirst(strtolower($this->input->post('keyword1'))));
			$data['keyword2'] = quotes_to_entities(ucfirst(strtolower($this->input->post('keyword2'))));
			$data['keyword3'] = quotes_to_entities(ucfirst(strtolower($this->input->post('keyword3'))));
			$data['keyword4'] = quotes_to_entities(ucfirst(strtolower($this->input->post('keyword4'))));
			
			// check that there is exactly one input
			$this->load->model('browse_model');
			
			$oneInput = $this->browse_model->checkOneInput($data);
			
			if ($oneInput) {
				// case where there is only one input
				// need to find which one is the input
				$titleIsInput = $this->browse_model->inputIsTitle($data);
				$industryIsInput = $this->browse_model->inputIsIndustry($data);
				
				if ($titleIsInput) {
					// title is input
					
					$this->titleBrowse($data['title']);
					
				} else if ($industryIsInput) {
					// industry is input
					
					$this->industryBrowse($data['industry']);
				} else {
					// input is one of the keywords
					$tmp = array($data['keyword1'], $data['keyword2'], $data['keyword3'], $data['keyword4']);
					
					// remove all empty elements from array
					$tmp = $this->browse_model->getNonEmpty($tmp);
					
					$this->keywordBrowse($tmp);
				}
			} else {
				// throw error message
				$this->load->view('templates/browseInputError');
			}
		} else {
			$this->load->view('templates/pleaseLogin');
		}
	}
	
	public function titleBrowse($titleIn) {
		// session check
		if ($this->session->userdata('is_logged_in')) {
			
			// NOTE: $titleIn is a string
			
			$this->load->model('browse_model');
			
			$data['results'] = $this->browse_model->titleSearch($titleIn);
			
			$this->load->view('browseResults_view', $data);
		} else {
			$this->load->view('templates/pleaseLogin');
		}
	}
	
	public function industryBrowse($industryIn) {
		// session check
		if ($this->session->userdata('is_logged_in')) {
			// $industryIn is a string
			
			$this->load->model('browse_model');
			
			$data['results'] = $this->browse_model->industrySearch($industryIn);
			
			$this->load->view('browseResults_view', $data);
			
		} else {
			$this->load->view('templates/pleaseLogin');
		}
	}
	
	public function keywordBrowse($dataIn) {
		// session check
		if ($this->session->userdata('is_logged_in')) {
			// $dataIn is an array
			
			$this->load->model('browse_model');
			
			$data['results'] = $this->browse_model->keywordSearch($dataIn);
			
			$this->load->view('browseResults_view', $data);
			
		} else {
			$this->load->view('templates/pleaseLogin');
		}
	}	
}
