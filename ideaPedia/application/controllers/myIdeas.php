<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MyIdeas extends CI_Controller {
	
	public function index() {
		// session check
		if ($this->session->userdata('is_logged_in')) {
			$this->loadView();
		} else {
			$this->load->view('templates/pleaseLogin');
		}
	}
	
	public function loadView() {
		// session check
		if ($this->session->userdata('is_logged_in')) {
			
			$this->load->model('my_ideas_model');
			
			$data['results'] = $this->my_ideas_model->getMySubmittedIdeas();
			
			$this->load->view('myIdeas_view', $data);
		} else {
			$this->load->view('templates/pleaseLogin');
		}
	}
	
	public function delete($iid = null) {
		// session check
		if ($this->session->userdata('is_logged_in')) {
			
			if (isset($iid)) {
				
				$data['iid'] = $iid;
				
				$this->load->model('idea_model');
				
				if ($this->idea_model->posterIsCurrUser($data['iid'])) {
					// poster is current user
					// user can delete the idea
					
					$this->load->model('my_ideas_model');
					
					// delete the idea
					$result = $this->my_ideas_model->deleteMyIdea($data['iid']);
					
					if ($result) {
						$this->load->view('templates/myIdeaDeleteSuccess');
					} else {
						echo "Error deleting idea";
					}
					
				} else {
					$this->load->view('templates/myIdeaDeleteError');
				}
			} else {
				$this->load->view('templates/ideaNotExistError');
			}
		} else {
			$this->load->view('templates/pleaseLogin');
		}
	}
	
	public function edit($iid = null) {
		// session check
		if ($this->session->userdata('is_logged_in')) {
			
			if (isset($iid)) {
				
				$data['iid'] = $iid;
				
				$this->load->model('idea_model');
				
				if ($this->idea_model->posterIsCurrUser($data['iid'])) {
					// poster is current user
					// user can edit the idea
					
					// get info about current idea from DB
					$this->load->model('my_ideas_model');
					$data['results'] = $this->my_ideas_model->editMyIdea($data['iid']);
					
					// get industry info from DB
					$this->load->model('idea_model');
					$data['industries'] = $this->idea_model->getIndustries();
					
					// load the edit view
					$this->load->view('myIdeas_edit_view', $data);
					
				} else {
					$this->load->view('templates/myIdeaEditError');
				}
			} else {
				$this->load->view('templates/ideaNotExistError');
			}
		} else {
			$this->load->view('templates/pleaseLogin');
		}
	}
	
	public function updateIdea($iid = null) {
		// session check
		if ($this->session->userdata('is_logged_in')) {
			
			if (isset($iid)) {
				
				$data['iid'] = $iid;
				
				$this->load->model('idea_model');
				
				if ($this->idea_model->posterIsCurrUser($data['iid'])) {
					
					$this->load->library('form_validation');
			
					// set validation rules
					$this->form_validation->set_rules('title', 'Title', 'required|trim');
					$this->form_validation->set_rules('keyword1', 'Keyword1', 'trim');
					$this->form_validation->set_rules('keyword2', 'Keyword2', 'trim');
					$this->form_validation->set_rules('keyword3', 'Keyword3', 'trim');
					$this->form_validation->set_rules('keyword4', 'Keyword4', 'trim');
					$this->form_validation->set_rules('description', 'Description', 'required|trim');
					
					if ($this->form_validation->run()) {
						
						echo "Form validation has run";
						
						// if there are no errors with the validation
				
						// get all the values from the input fields
						// and sanitize the input
						
						$this->load->helper('string');
						
						$data['title'] = quotes_to_entities($this->input->post('title'));
						$data['industry'] = $this->input->post('industry');
						$data['keyword1'] = quotes_to_entities(ucfirst(strtolower($this->input->post('keyword1'))));
						$data['keyword2'] = quotes_to_entities(ucfirst(strtolower($this->input->post('keyword2'))));
						$data['keyword3'] = quotes_to_entities(ucfirst(strtolower($this->input->post('keyword3'))));
						$data['keyword4'] = quotes_to_entities(ucfirst(strtolower($this->input->post('keyword4'))));
						$data['description'] = quotes_to_entities($this->input->post('description'));
						
						// load the model
						$this->load->model('my_ideas_model');
						
						// update the values in the database to the new values
						$resultValue = $this->my_ideas_model->updateIdea($data);
						
						if ($resultValue) {
							$this->load->view('templates/myIdeaUpdateSuccess');
						} else {
							echo "Error changing data";
						}
						
					} else {
						// redirect to the same page with an error message
						$this->edit($iid);
					}
					
				} else {
					$this->load->view('templates/myIdeaEditError');
				}
			} else {
				$this->load->view('templates/ideaNotExistError');
			}
		} else {
			$this->load->view('templates/pleaseLogin');
		}
	}
}
