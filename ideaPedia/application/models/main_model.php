<?php

class Main_model extends CI_Model {
	
	public function getAdmin($data) {
		
		$username = $data['username'];
		
		$query = $this->db->query("SELECT IFNULL(email, 0) AS email FROM User WHERE email='" . $username . "' AND admin=1 AND active=1;");
		
		return $query;
	}
	
	public function getUid($data) {
		
		$email = $data['username'];
		
		$query = $this->db->query("SELECT uid FROM User WHERE email='".$email."';");
		
		$row = $query->row();
		$uid = $row->uid;
		
		return $uid;
		
	}
	
	public function canLogIn() {
		$this->db->where('email', $this->input->post('email'));
		$this->db->where('password', md5($this->input->post('password')));
		$query = $this->db->get('User');
		
		if ($query->num_rows() == 1) {
			return true;
		} else {
			return false;
		}
	}
	
	// run from the signup_validation() function
	// adds a user to the database
	public function add_user($key) {
		$data = array(
			'email' => $this->input->post('email'),
			'password' => md5($this->input->post('password')),
			'active' => 1,
			'admin' => 0
		);
		
		$query = $this->db->insert('User', $data);
		
		// return whether the query was successful or not
		if ($query) {
			return true;
		} else {
			return false;
		}
	}
	
}
