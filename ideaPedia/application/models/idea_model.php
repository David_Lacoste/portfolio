<?php

class Idea_model extends CI_Model {
	
	public function getIndustries() {
		$query = $this->db->query("SELECT * FROM Industry WHERE active=1 GROUP BY title ASC;");
		
		return $query->result();
	}
	
	public function createIdea($data) {
		
		$title = $data['title'];
		$description = $data['description'];
		$submitter = $this->session->userdata('uid');
		$industry = $data['industry'];
		
		$queryString = "INSERT INTO Idea (title, description, postDate, submitter, industry, active) VALUES ('".$title."', '".$description."', now(), ".$submitter.", ".$industry.", 1)";
		
		$query = $this->db->query($queryString);
	}
	
	public function getIid($data) {
		
		// get the values from $data array to build query string
		$title = $data['title'];
		$description = $data['description'];
		$industry = $data['industry'];
		$uid = $this->session->userdata('uid');
		
		$queryString = "SELECT iid FROM Idea WHERE title='".$title."' AND description='".$description."' AND submitter=".$uid." AND industry=".$industry." AND active=1;";
		
		$query = $this->db->query($queryString);
		
		$row = $query->row();
		$iid = $row->iid;
		
		return $iid;
	}
	
	public function ideaAlreadyExists($dataIn) {
		
		$title = $dataIn['title'];
		$description = $dataIn['description'];
		$industry = $dataIn['industry'];
		$submitter = $this->session->userdata('uid');
		
		$queryString = "SELECT * FROM Idea WHERE title='".$title."' AND description='".$description."' AND submitter=".$submitter." AND industry=".$industry.";";
		
		$query = $this->db->query($queryString);
		
		if ($query->num_rows() > 0) {
			// the user has already created an identical idea
			return true;
		} else {
			// the user has not created an identical idea under another
			// iid and it is safe to create
			return false;
		}
	}
	
	public function createKeywords($data) {
		
		// sanitize keyword input
		if (strlen($data['keyword1']) == 0) {
			$data['keyword1'] = "''";
		}
		
		if (strlen($data['keyword2']) == 0) {
			$data['keyword2'] = "''";
		}
		
		if (strlen($data['keyword3']) == 0) {
			$data['keyword3'] = "''";
		}
		
		if (strlen($data['keyword4']) == 0) {
			$data['keyword4'] = "''";
		}
		
		// get data from $data array
		$iid = $data['iid'];
		
		$queryData = array(
			'keyword1' => $data['keyword1'],
			'keyword2' => $data['keyword2'],
			'keyword3' => $data['keyword3'],
			'keyword4' => $data['keyword4']
		);
		
		$this->db->where('iid', $iid);
		$this->db->update('Idea', $queryData);
	}
	
	public function getIidInfo($iid) {
		
		$queryString = "SELECT ID.iid, ID.submitter, ID.title AS title, IND.title AS industryTitle, ID.description, ID.keyword1, ID.keyword2, ID.keyword3, ID.keyword4 FROM Idea ID JOIN Industry IND ON ID.industry=IND.indId WHERE iid=".$iid.";";
		
		$query = $this->db->query($queryString);
		
		return $query->result();
	}
	
	public function iidExists($iid) {
		
		$queryString = "SELECT * FROM Idea WHERE iid=".$iid.";";
		
		$query = $this->db->query($queryString);
		
		if ($query->num_rows() == 1) {
			return true;
		} else {
			return false;
		}
	}
	
	/*
	 * Given the iid of an Idea, find whether the user who posted
	 * the idea is the current user. Return true if it is. If not,
	 * return false.
	 *  
	 * */
	public function posterIsCurrUser($iid) {
		
		// find the poser of the Idea
		$queryString = "SELECT submitter FROM Idea WHERE iid=".$iid.";";
		
		$query = $this->db->query($queryString);
		
		$row = $query->row();
		$submitter = $row->submitter;
		
		if ($submitter == $this->session->userdata('uid')) {
			// if the poster of the Idea is the current user
			return true;
		} else {
			return false;
		}
		
	}
	
	public function rateLikeIdea($data) {
		$uid = $data['uid'];
		$iid = $data['iid'];
		
		$queryData = array(
			'uid' => $uid,
			'iid' => $iid,
			'liked' => 1,
			'disliked' => 0,
			'active' => 1
		);
		
		$query = $this->db->insert('RateIdea', $queryData);
		
	}
	
	public function rateDislikeIdea($data) {
		$uid = $data['uid'];
		$iid = $data['iid'];
		
		$queryData = array(
			'uid' => $uid,
			'iid' => $iid,
			'liked' => 0,
			'disliked' => 1,
			'active' => 1
		);
		
		$query = $this->db->insert('RateIdea', $queryData);
	}
	
	public function getNumLikes($iid) {
		
		$queryString = "SELECT SUM(liked) AS likes FROM RateIdea WHERE iid=".$iid.";";
		
		$query = $this->db->query($queryString);
		
		$row = $query->row();
		$likes = $row->likes;
		
		return $likes;
		
	}
	
	public function getNumDislikes($iid) {
		
		$queryString = "SELECT SUM(disliked) AS dislikes FROM RateIdea WHERE iid=".$iid.";";
		
		$query = $this->db->query($queryString);
		
		$row = $query->row();
		$dislikes = $row->dislikes;
		
		return $dislikes;
		
	}
	
}