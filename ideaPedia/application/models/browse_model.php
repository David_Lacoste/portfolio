<?php

class Browse_model extends CI_Model {
	
	public function getIndustries() {
		$query = $this->db->query("SELECT * FROM Industry WHERE active=1 ORDER BY title ASC;");
		
		return $query->result();
	}
	
	public function checkOneInput($data) {
		
		// get all the data from $data
		$title = $data['title'];
		$industry = $data['industry'];
		$keyword1 = $data['keyword1'];
		$keyword2 = $data['keyword2'];
		$keyword3 = $data['keyword3'];
		$keyword4 = $data['keyword4'];
		
		$num = 0;
		
		// count the number of fields that have an input
		if ($industry != "--") {
			$num = $num + 1;
		} else if (! empty($title)) {
			$num = $num + 1;
		} else if (!empty($keyword1) || !empty($keyword2) || !empty($keyword3) || !empty($keyword4)) {
			$num = $num + 1;
		}
		
		return $num;
		
	}
	
	public function inputIsTitle($data) {
		// get all the data from $data
		$title = $data['title'];
		
		if (! empty($title)) {
			return true;
		}
		
	}
	
	public function inputIsIndustry($data) {
		// get all the data from $data
		$industry = $data['industry'];
		
		if ($industry != "--") {
			return true;
		}
	}
	
	public function countInputs($data) {
		// get all the data from $data
		$title = $data['title'];
		$industry = $data['industry'];
		$keyword1 = $data['keyword1'];
		$keyword2 = $data['keyword2'];
		$keyword3 = $data['keyword3'];
		$keyword4 = $data['keyword4'];
		
		$num = 0;
		
		// count the number of fields that have an input
		if ($industry != "--") {
			$num = $num + 1;
		}
		
		if (! empty($title)) {
			$num = $num + 1;
		}
		
		$tmp = array($keyword1, $keyword2, $keyword3, $keyword4);
		
		foreach ($tmp as $item) {
			if (!empty($item)) {
				$num = $num + 1;
			}
		}
		
		return $num;
	}
	
	public function titleSearch($titleIn) {
		
		// query the keywords in a separate query
		
		$queryString = "SELECT ID.iid, ID.title AS title, ID.description, ID.keyword1, ID.keyword2, ID.keyword3, ID.keyword4, ID.industry, IND.title AS indTitle FROM Idea ID JOIN Industry IND ON ID.industry=IND.indId WHERE ID.title='".$titleIn."' AND ID.active=1 AND IND.active=1;";
		
		$query = $this->db->query($queryString);
		
		return $query->result();
	}
	
	public function industrySearch($industryIn) {
		$queryString = "SELECT ID.iid, ID.title AS title, ID.description, ID.keyword1, ID.keyword2, ID.keyword3, ID.keyword4, ID.industry, IND.title AS indTitle FROM Idea ID JOIN Industry IND ON ID.industry=IND.indId WHERE ID.industry='".$industryIn."' AND ID.active=1 AND IND.active=1;";
		
		$query = $this->db->query($queryString);
		
		return $query->result();
	}
	
	public function getNonEmpty($dataIn) {
		// $dataIn is an array
		
		$outArray = [];
		
		foreach($dataIn as $item) {
			if (! empty($item)) {
				$outArray[] = $item;
			}
		}
		
		return $outArray;
	}
	
	public function keywordSearch($dataIn) {
		$dataInSize = count($dataIn);
		
		// if we get to this function, there is at least one item in
		// $dataIn and we know that it is non-empty
		
		$viewString = "CREATE VIEW tmpView AS ((SELECT DISTINCT iid, title, description, keyword1, keyword2, keyword3, keyword4, industry FROM Idea WHERE keyword1='".$dataIn[0]."' OR keyword2='".$dataIn[0]."' OR keyword3='".$dataIn[0]."' OR keyword4='".$dataIn[0]."')";
		
		for ($i = 1; $i < $dataInSize; $i++) {
			$viewString = $viewString . " UNION ";
			$viewString = $viewString . "(SELECT DISTINCT iid, title, description, keyword1, keyword2, keyword3, keyword4, industry FROM Idea WHERE keyword1='".$dataIn[$i]."' OR keyword2='".$dataIn[$i]."' OR keyword3='".$dataIn[$i]."' OR keyword4='".$dataIn[$i]."')";
		}
		
		//$viewString = $viewString . " ORDER BY title ASC);";
		$viewString = $viewString . ");";
		
		$viewQuery = $this->db->query($viewString);
		
		$queryString = "SELECT TP.iid, TP.title AS title, TP.description, TP.keyword1, TP.keyword2, TP.keyword3, TP.keyword4, TP.industry, IND.title AS indTitle FROM tmpView TP JOIN Industry IND ON TP.iid=IND.indId WHERE IND.active=1;";
		
		$query = $this->db->query($queryString);
		
		$this->dropView();
		
		return $query->result();
		
	}
	
	public function dropView() {
		
		$dropString = "DROP VIEW tmpView;";
		
		$dropQuery = $this->db->query($dropString);
		
		if ($dropQuery) {
			return true;
		} else {
			return false;
		}
	}
}
