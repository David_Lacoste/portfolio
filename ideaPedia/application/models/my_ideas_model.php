<?php

class My_ideas_model extends CI_Model {
	
	public function getMySubmittedIdeas() {
		
		$submitter = $this->session->userdata('uid');
		
		$queryString = "SELECT ID.iid, ID.title AS title, ID.industry, IND.title AS indTitle FROM Idea ID JOIN Industry IND ON ID.industry=IND.indId WHERE ID.submitter=".$submitter." AND ID.active=1 AND IND.active=1;";
		
		$query = $this->db->query($queryString);
		
		return $query->result();
		
	}
	
	public function editMyIdea($iid) {
		$queryString = "SELECT iid, title, description, keyword1, keyword2, keyword3, keyword4, industry FROM Idea WHERE iid=".$iid." AND active=1;";
		
		$query = $this->db->query($queryString);
		
		if ($query) {
			return $query->result();
		} else {
			$query[0] = "Error";
			return $query;
		}
	}
	
	public function deleteMyIdea($iid) {
		$queryString = "UPDATE Idea SET active=0 WHERE iid=".$iid.";";
		
		$query = $this->db->query($queryString);
		
		// return the query status result
		if ($query) {
			return true;
		} else {
			return false;
		}
	}
	
	public function updateIdea($dataIn) {
		
		$iid = $dataIn['iid'];
		$title = $dataIn['title'];
		$industry = $dataIn['industry'];
		$keyword1 = $dataIn['keyword1'];
		$keyword2 = $dataIn['keyword2'];
		$keyword3 = $dataIn['keyword3'];
		$keyword4 = $dataIn['keyword4'];
		$description = $dataIn['description'];
		
		$queryString = "UPDATE Idea SET title='".$title."', industry=".$industry.", keyword1='".$keyword1."', keyword2='".$keyword2."', keyword3='".$keyword3."', keyword4='".$keyword4."', description='".$description."' WHERE active=1 AND iid=".$iid.";";
		
		$query = $this->db->query($queryString);
		
		if ($query) {
			return true;
		} else {
			return false;
		}
	}
}
