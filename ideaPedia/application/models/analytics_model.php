<?php

class Analytics_model extends CI_Model {
	
	public function topIdeas($data) {
		
		// build start date and end date
		$numIdeas = $data['numIdeas'];
		$startYear = $data['startYear'];
		$startMonth = $data['startMonth'];
		$startDate = $data['startDate'];
		$endYear = $data['endYear'];
		$endMonth = $data['endMonth'];
		$endDate = $data['endDate'];
		
		$startPost = $startYear."-".$startMonth."-".$startDate." 00:00:00";
		$endPost = $endYear."-".$endMonth."-".$endDate." 00:00:00";
		
		$firstQueryString = "CREATE VIEW tmpView AS (SELECT iid, (IFNULL(SUM(liked),0) - IFNULL(SUM(disliked),0)) AS ratings FROM RateIdea WHERE active=1 GROUP BY iid);";
		
		// create the view
		$viewQuery = $this->db->query($firstQueryString);
		
		$queryString = "SELECT ID.iid, ID.title, ID.postDate, TP.ratings FROM Idea ID JOIN tmpView TP ON TP.iid=ID.iid WHERE (ID.postDate between '".$startPost."' and '".$endPost."') AND ID.active=1 GROUP BY ID.iid ORDER BY TP.ratings DESC LIMIT ".$numIdeas.";";
		
		$query = $this->db->query($queryString);
		
		$this->dropTemp();
		
		return $query->result();
		
	}
	
	public function dropTemp() {
		$queryString = "DROP VIEW tmpView;";
		
		$query = $this->db->query($queryString);
	}
	
	public function getDistGraphData() {
		$queryString = "SELECT ID.industry AS industry, IND.title AS title, COUNT(ID.industry) AS total FROM Idea ID JOIN Industry IND ON ID.industry=IND.indId WHERE ID.active=1 AND IND.active=1 GROUP BY industry ASC;";
		
		$query = $this->db->query($queryString);
		
		return $query->result();
	}
	
}
