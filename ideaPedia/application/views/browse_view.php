<!DOCTYPE html>
<html>
	
	<head>
		<title>Browse</title>
		<link type='text/css' rel='stylesheet' href='<?php echo base_url()?>assets/stylesheets/header.css' />
		<link type='text/css' rel='stylesheet' href='<?php echo base_url()?>assets/stylesheets/browse.css' />
		<script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery-2.1.3.js"></script>
		<script type="text/javascript" src="<?php echo base_url()?>assets/js/submitIdea.js"></script>
	</head>
	
	<header>
		<!-- Include the header -->
		<?php $this->load->view('templates/header');?>
	</header>
	
	<body>
		
		<h2>Filter Results By:</h2>
		
		<?php echo validation_errors();	?>
		
		<form method="post" action="<?php echo base_url()?>browse/filter">
			Idea Title: <input type="text" name="title" /> 
			<h3>OR</h3>
			Industry: <select name="industry" size="1">
				<option value="--">--</option>
				
				<!-- Create drop-down contents from DB query -->
				<?php
					foreach ($industries as $row) {
						echo "<option value='".$row->indId."'>".$row->title;
						echo "</option>";
					}
				
				 ?>
			</select>
			<h3>OR</h3>
			<table>
				<tr>
					<td>Keywords:</td>
					<td><input class="noSpace" type="text" name="keyword1" /></td>
					<td><input class="noSpace" type="text" name="keyword2" /></td>
				</tr>
				<tr>
					<td></td>
					<td><input class="noSpace" type="text" name="keyword3" /></td>
					<td><input class="noSpace" type="text" name="keyword4" /></td>
				</tr>
			</table>
			<br>
			<button type="button" onClick='window.location.href = "<?php echo base_url();?>home"'>Cancel</button>
			<button type="submit">GO!</button>
		</form>
	</body>
	
</html>