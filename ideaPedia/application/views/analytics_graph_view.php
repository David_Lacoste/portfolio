<!DOCTYPE html>
<html>
	
	<head>
		<title>Idea Distribution</title>
		<link type='text/css' rel='stylesheet' href='<?php echo base_url()?>assets/stylesheets/header.css' />
		<link type='text/css' rel='stylesheet' href='<?php echo base_url()?>assets/stylesheets/browse.css' />
		<script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery-2.1.3.js"></script>
		<script type="text/javascript" src="<?php echo base_url()?>assets/js/analytics.js"></script>
		
		<!-- WEBSITE CODE -->
		<script type="text/javascript" src="https://www.google.com/jsapi"></script>
	    <script type="text/javascript">
			
		  /*
		  * This code was taken and modified from the following
		  * Google Developers Charts API website tutorial.
		  *
		  * Link: https://google-developers.appspot.com/chart/interactive/docs/quick_start
		  *
		  **/
	
	      // Load the Visualization API and the piechart package.
	      google.load('visualization', '1.0', {'packages':['corechart']});
	
	      // Set a callback to run when the Google Visualization API is loaded.
	      google.setOnLoadCallback(drawChart);
	
	      // Callback that creates and populates a data table,
	      // instantiates the pie chart, passes in the data and
	      // draws it.
	      function drawChart() {
	
	        // Create the data table.
	        var data = new google.visualization.DataTable();
	        
	        data.addColumn('string', 'Industry');
	        data.addColumn('number', 'Total');
	        data.addRows([
				<?php
					// only get the first item
					for ($i = 0; $i < 1; $i++) {
						echo "['".$graph[0]->title."', ".$graph[0]->total."]";
					}
					
					$count = 0;
					
					foreach ($graph as $row) {
						if (!($count == 0)) {
							// if it is not the first row
							// do nothing for the first row as it was
							// already handled above
							
							echo ", ";
							echo "['".$row->title."', ".$row->total."]";
						}
						$count = $count + 1;
					}
				 ?>
	        ]);
	
	        // Set chart options
	        var options = {'title':'Category Distribution of Ideas',
							'width':800,
							'height':300};
	        
	        // Instantiate and draw our chart, passing in some options.
	        var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
	        chart.draw(data, options);
	      }
	    </script>

	</head>
	
	<header>
		<!-- Include the header -->
		<?php $this->load->view('templates/header');?>
	</header>
	
	<body>
		 <!--Div that will hold the pie chart-->
	    <div id="chart_div"></div>
	</body>
	
</html>