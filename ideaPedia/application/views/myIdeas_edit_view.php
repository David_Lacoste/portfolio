<!DOCTYPE html>
<html>

	<head>
		<title>Edit Idea</title>
		<link type='text/css' rel='stylesheet' href='<?php echo base_url()?>assets/stylesheets/header.css' />
		<link type='text/css' rel='stylesheet' href='<?php echo base_url()?>assets/stylesheets/submitIdea.css' />
		<script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery-2.1.3.js"></script>
		<script type="text/javascript" src="<?php echo base_url()?>assets/js/submitIdea.js"></script>
	</head>
	
	<header>
		<!-- Include the header -->
		<?php $this->load->view('templates/header');?>
	</header>
	
	<body>
		<h1>Edit Idea</h1>
		
		<?php echo validation_errors(); ?>
		 
		 <form method="post" action="<?php echo base_url();?>myIdeas/updateIdea/<?php
			foreach ($results as $row) {
				echo $row->iid;
			}
		 ?>">
			
			Title: <input type="text" name="title" 
			<?php
				foreach($results as $row) {
					echo "value='".$row->title."' />";
				}
			 ?>
			 
			 <br>
			 
			 Industry: <select name="industry" size="1">
			
			<!-- Create drop-down contents from DB query -->
			<?php
				foreach ($industries as $row) {
					echo "<option value='".$row->indId."'>".$row->title;
					echo "</option>";
				}
			
			 ?>
			 </select>
			 
			<br>
			
			<table>
				<tr>
					<td>Keywords:</td>
					<td><input class="noSpace" type="text" name="keyword1" <?php
					
						foreach ($results as $row) {
							echo "value='".$row->keyword1."'";
						}
					
					 ?>/></td>
					<td><input class="noSpace" type="text" name="keyword2" <?php
					
						foreach ($results as $row) {
							echo "value='".$row->keyword2."'";
						}
					
					 ?>/></td>
				</tr>
				<tr>
					<td></td>
					<td><input class="noSpace" type="text" name="keyword3" <?php
					
						foreach ($results as $row) {
							echo "value='".$row->keyword3."'";
						}
					
					 ?>/></td>
					<td><input class="noSpace" type="text" name="keyword4" <?php
					
						foreach ($results as $row) {
							echo "value='".$row->keyword4."'";
						}
					
					 ?>/></td>
				</tr>
			</table>
			Description:
			<br>
			<div class="container">
				<textarea name="description" rows="10" cols="90"><?php echo $results[0]->description;?></textarea>
				<br>
				<br>
				<button type="button" onClick='window.location.href = "<?php echo base_url();?>myIdeas"'>Cancel</button>
				<button type="submit">Submit Idea</button>
			</div>
			
		 </form>
		
		
		
	</body>


</html>