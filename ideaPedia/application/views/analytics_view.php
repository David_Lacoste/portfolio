<!DOCTYPE html>
<html>

	<head>
		<title>Analytics</title>
		<link type='text/css' rel='stylesheet' href='<?php echo base_url()?>assets/stylesheets/header.css' />
		<link type='text/css' rel='stylesheet' href='<?php echo base_url()?>assets/stylesheets/browse.css' />
		<script type="text/javascript" src="<?php echo base_url()?>assets/js/jquery-2.1.3.js"></script>
		<script type="text/javascript" src="<?php echo base_url()?>assets/js/analytics.js"></script>
	</head>
	
	<header>
		<!-- Include the header -->
		<?php $this->load->view('templates/header');?>
	</header>
	
	<body>
		
			<h2>Analytics</h2>
				
				<button type="button" onClick='window.location.href = "<?php echo base_url();?>analytics/graph"'>Idea Distribution Graph</button>
				
				<br>
				<br>
				
				<hr>
				
				<?php echo validation_errors(); ?>
				
				<form type="get" action="<?php echo base_url();?>analytics/wrapper">
				
				<h3>Show Most Popular Ideas</h3>
				
				Show top <input type="text" name="numIdeas" size="2" /> ideas (i.e. "3")
				
				<br>
				<br>
				
				Start Date: Y:
				<!-- start year selection -->
				<select name="startYear" size="1">
					<?php
						// create an <option> tag for years between
						// 1850 and 2050
						for ($i = 1850; $i < 2051; $i++) {
							echo "<option value='" . $i . "'>" . $i;
							echo "</option>";
						}
					
					 ?>
				</select>
				
				<!-- start month selection -->
				M: 
				<select name="startMonth" size="1">
					<?php
					
						for ($i = 1; $i < 13; $i++) {
							echo "<option value='" . $i . "'>" . $i;
							echo "</option>";
						}
					
					 ?>
				</select>
				
				<!-- start date selection -->
				D: <select name="startDate" size="1">
					<?php
						for ($i = 1; $i < 32; $i++) {
							echo "<option value='" . $i . "'>" . $i;
							echo "</option>";
						}
					 ?>
				</select>
				
				<br>
				<br>
				
				End Date: Y:
				<!-- end year selection -->
				<select name="endYear" size="1">
					<?php
						// create an <option> tag for years between
						// 1850 and 2050
						for ($i = 1850; $i < 2051; $i++) {
							echo "<option value='" . $i . "'>" . $i;
							echo "</option>";
						}
					
					 ?>
				</select>
				
				<!-- end month selection -->
				M: 
				<select name="endMonth" size="1">
					<?php
					
						for ($i = 1; $i < 13; $i++) {
							echo "<option value='" . $i . "'>" . $i;
							echo "</option>";
						}
					
					 ?>
				</select>
				
				<!-- end date selection -->
				D: <select name="endDate" size="1">
					<?php
						for ($i = 1; $i < 32; $i++) {
							echo "<option value='" . $i . "'>" . $i;
							echo "</option>";
						}
					 ?>
				</select>
				
				<br>
				<br>
				
				<button type="submit">Display</button>
				
			</form>
		
	</body>


</html>