<!DOCTYPE html>
<html>
	
	<head>
		<title>Login</title>
		<link type='text/css' rel='stylesheet' href='<?php echo base_url()?>assets/stylesheets/login.css' />
	</head>
	
	<body>
		
		<div class="container">
		
		<h1>Welcome to ideaPedia!</h1>
		
		<hr>
		
		<form action="<?php echo base_url()?>main/validateLogin" method="post">
			<br>
			<input type="text" name="email" placeholder="Login" />
			<br>
			<br>
			<input type="password" name="password" placeholder="Password" />
			<br>
			<br>
			<button type="button" onClick='window.location.href = "<?php echo base_url(); ?>main/signup"'>Signup</button>
			<button type="submit">Login</button>
			<br>
			<br>
		</form>
		
		<hr>
		
		<a href="<?php echo base_url();?>about">About</a>
		<br>
		<br>
		</div>
	</body>
	
</html>