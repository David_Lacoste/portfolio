<!DOCTYPE html>
<html>
	
	<head>
		<title>My Ideas</title>
		<link type='text/css' rel='stylesheet' href='<?php echo base_url()?>assets/stylesheets/header.css' />
		<link type='text/css' rel='stylesheet' href='<?php echo base_url()?>assets/stylesheets/browse.css' />
		<link type='text/css' rel='stylesheet' href='<?php echo base_url()?>assets/stylesheets/browseResults.css' />
		<script type="text/javascript" src="<?php echo base_url()?>assets/js/myIdeas.js"></script>
	</head>
	
	<header>
		<!-- Include the header -->
		<?php $this->load->view('templates/header');?>
	</header>
	
	<body>
		<h1>My Submitted Ideas</h1>
		
		<table>
			<th>Idea Reference #</th>
			<th>Title</th>
			<th>Industry</th>
			<th>Modify</th>
			
			<?php
			
				foreach ($results as $row) {
					echo "<tr>";
						echo "<td>";
						echo $row->iid;
						echo "</td>";
						echo "<td>";
						echo "<a href='";
						echo base_url();
						echo "idea/render/".$row->iid."'>";
						echo $row->title;
						echo "</a>";
						echo "</td>";
						echo "<td>";
						echo $row->indTitle;
						echo "</td>";
						echo "<td>";
						
						// edit button
						echo "<button type='button' onClick='editClick(\"".base_url()."\", \"".$row->iid."\")'>Edit</button>";
						
						// delete button
						echo "<button type='button' onClick='deleteClick(\"".base_url()."\", \"".$row->iid."\")'>Delete</button>";
						
						echo "</td>";
					echo "</tr>";
				}
			
			 ?>
			
		</table>
	</body>
	
	
</html>