<!DOCTYPE html>
<html>
	
	<head>
		<!-- Render the title -->
		<title>Idea - <?php
			foreach($results as $row) {
				echo $row->title;
			}
		 ?></title>
		<link type='text/css' rel='stylesheet' href='<?php echo base_url()?>assets/stylesheets/header.css' />
		<link type='text/css' rel='stylesheet' href='<?php echo base_url()?>assets/stylesheets/ideaRender.css' />
	</head>
	
	<header>
		<!-- Include the header -->
		<?php $this->load->view('templates/header');?>
	</header>
	
	<body>
		<h2>Title: <?php 
			
			// there will only ever be 
			foreach($results as $row) {
				echo $row->title;
			}
		
		?></h2>
		
		<!-- Display rate status -->
		<!-- TODO: New feature: display message about whether
		the user has already liked/disliked the idea
		
		NOTE: Could also directly change the button rendering through
		the DOM/javascript
		 -->
		
		Description: <?php
			
			foreach($results as $row) {
				echo $row->description;
			}
		
		 ?>
		<br>
		<br>
		Industry: <?php
		
			foreach($results as $row) {
					echo $row->industryTitle;
			}
		
		 ?>
		<br>
		Keywords: <?php
		
			$tmp = array($row->keyword1, $row->keyword2, $row->keyword3, $row->keyword4);
					
			$outArray = [];
			
			foreach($tmp as $item) {
				if (! empty($item)) {
					$outArray[] = $item;
				}
			}
			
			// changed all "tmp" to "outArray"
			if (!empty($outArray[0]) && $outArray[0] != "''") {
				echo $outArray[0];
			}
			
			for ($i = 1; $i < count($outArray); $i++) {
				if (!empty($outArray[$i]) && $outArray[$i] != "''") {
					echo ", ";
					echo $outArray[$i];
				}
			}
		
		 ?>
		 <br>
		 Rating: <?php echo $numLikes;?> likes, <?php echo $numDislikes;?> dislikes
		
		<?php 
			
			// there will only be one result
			foreach ($results as $row) {
				$queryUID = $row->submitter;
				$iid = $row->iid;
			}
			
			if ($queryUID != $this->session->userdata('uid')) {
				// if the current user is not the user that posted
				// the idea
				echo "<br>";
				echo "<form method='get' action='".base_url()."idea/rate/".$iid."'>";
				
				// do not need hidden iid input field because the rate
				// function needs it to be declared explicitly when using the REST call
				echo "Rate: ";
				echo "<button type='submit' name='like' value='1'>Like</button>";
				echo "<button type='submit' name='dislike' value='1'>Dislike</button>";
				echo "</form>";
				
			}
		
		?>
		
	</body>
	
</html>