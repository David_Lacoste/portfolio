<!DOCTYPE html>
<html>
	<head>
		<title>Sign Up</title>
		<link type='text/css' rel='stylesheet' href='<?php echo base_url()?>assets/stylesheets/header.css' />
		<link type='text/css' rel='stylesheet' href='<?php echo base_url()?>assets/stylesheets/login.css' />
	</head>
	
	<body>
		
		<div class="container">
		
		<h1>Sign Up</h1>
		
		<hr>
		
		<?php echo validation_errors(); ?>
		
		<br>
		
		<form method="post" action="<?php echo base_url();?>main/signup_validation">
			<input type="text" name="email" placeholder="Email" />
			<br>
			<br>
			<input type="password" name="password" placeholder="Password" />
			<br>
			<br>
			<input type="password" name="cpassword" placeholder="Confirm Password" />
			<br>
			<br>
			<button type="button" onClick='window.location.href = "<?php echo base_url(); ?>"'>Cancel</button>
			<button type="submit">Sign Up</button>
			<br>
			<br>
		</form>
		
		</div>
	</body>
</html>