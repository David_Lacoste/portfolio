<!DOCTYPE html>
<html>
	<head>
		<title>Welcome</title>
		<link type='text/css' rel='stylesheet' href='<?php echo base_url()?>assets/stylesheets/header.css' />
		<link type='text/css' rel='stylesheet' href='<?php echo base_url()?>assets/stylesheets/welcome.css' />
	</head>
	
	<header>
		<!-- Include the header -->
		<?php $this->load->view('templates/header');?>
	</header>
	
	<body>
		<div class="container">
			<h1>Welcome to ideaPedia!</h1>
			<h2>An innovative space for people to browse and submit start-up ideas!</h2>
			<button id="button1" type="button" onClick='window.location.href = "<?php echo base_url()?>idea"'>Start Today</button>
			<button id="button2" type="button" onClick='window.location.href = "<?php echo base_url()?>browse"'>Browse Ideas</button>
		</div>
	</body>
</html>