<!DOCTYPE html>
<html>
	
	<head>
		<title>Browse Results</title>
		<link type='text/css' rel='stylesheet' href='<?php echo base_url()?>assets/stylesheets/header.css' />
		<link type='text/css' rel='stylesheet' href='<?php echo base_url()?>assets/stylesheets/welcome.css' />
		<link type='text/css' rel='stylesheet' href='<?php echo base_url()?>assets/stylesheets/browseResults.css' />
	</head>
	
	<header>
		<!-- Include the header -->
		<?php $this->load->view('templates/header');?>
	</header>
	
	<body>
		<h1>Results</h1>
		
		<table>
			<th>Idea Title</th>
			<th>Industry</th>
			<th>Keywords</th>
			<?php
			
			foreach ($results as $row) {
				echo "<tr>";
					echo "<td>";
					echo "<a href='";
					echo base_url();
					echo "idea/render/".$row->iid."'>";
					echo $row->title."</a></td>";
					echo "<td>".$row->indTitle."</td>";
					echo "<td>";
					$tmp = array($row->keyword1, $row->keyword2, $row->keyword3, $row->keyword4);
					
					$outArray = [];
					
					foreach($tmp as $item) {
						if (! empty($item)) {
							$outArray[] = $item;
						}
					}
					
					// changed all "tmp" to "outArray"
					if (!empty($outArray[0]) && $outArray[0] != "''") {
						echo $outArray[0];
					}
					
					for ($i = 1; $i < count($outArray); $i++) {
						if (!empty($outArray[$i]) && $outArray[$i] != "''") {
							echo ", ";
							echo $outArray[$i];
						}
					}
					
					echo "</td>";
				echo "</tr>";
				
			}
			
			 ?>
		</table>
		
		
	</body>
	
</html>