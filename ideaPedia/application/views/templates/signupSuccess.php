<!DOCTYPE html>
<html>
	
	<head>
		<title>Success</title>
		<meta http-equiv="refresh" content="5; <?php echo base_url();?>main">
	</head>
	
	<body>
		<p>User has been added to the database.</p>
		<p>You will be automatically redirected to the login page in 5 seconds.</p>
	</body>
	
</html>