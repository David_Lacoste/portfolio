<!DOCTYPE html>
<html>
	
	<head>
		<title>Error</title>
	</head>
	
	<body>
		<h3>Login Error</h3>
		
		<p>You are seeing this page if you have either not logged in 
		or have tried logging in with an incorrect username/password. 
		Please go to this <a href="<?php echo base_url()?>">link</a> and try logging in again.</p>
	</body>
</html>