<!DOCTYPE html>
<html>
	<head>
		<title>About</title>
		<link type='text/css' rel='stylesheet' href='<?php echo base_url()?>assets/stylesheets/login.css' />
	</head>
	
	
	
	<body>
		
		<div class="aboutContainer">
		
		<h1 id="aboutTitle">About</h1>
		
		<p id="par1">
		<b>Author: </b> David Lacoste
		<br>
		<b>CDF ID: </b> g3lacost
		<br>
		<b>Email: </b> david.lacoste@mail.utoronto.ca
		<br>
		<br>
		Created for the course CSC309H at the University of Toronto in Winter 2015
		</p>
		
		<div class="buttonContainer">
			<button type="button" onClick='window.location.href = "<?php echo base_url();?>"'>Login</button>
		</div>
		
		<br>
		
		</div>
		
	</body>
	
</html>