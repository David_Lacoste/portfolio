// disable using the spacebar for all elements in the "noSpace" class
$(document).ready(function() {
	$(".noSpace").keydown(function (event) {
		if (event.keyCode == 32) {
			event.preventDefault();
		}
	});
});