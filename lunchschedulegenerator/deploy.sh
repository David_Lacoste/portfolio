#! /bin/bash

# clean existing WAR
mvn clean

# build the production WAR
mvn package -Denv=prod -Ddefault

# deploy to heroku
heroku deploy:war target/LunchSchedulerWeb.war --app lunchschedulerweb