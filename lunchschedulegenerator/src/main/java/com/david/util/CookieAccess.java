/*
 * Static class to provide the current contents in the browser's relevant cookies
 */
package com.david.util;

public class CookieAccess {
	
	private static String username;
	private static String role;
	
	public CookieAccess(String username, String role) {
		CookieAccess.username = username;
		CookieAccess.role = role;
	}
	
	public static String getUsername() {
		return username;
	}
	
	public static String getRole() {
		return role;
	}
}
