package com.david.util;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class CalendarUtil {
	
	/*
	 * Given a date, return true if the day of the week is a Saturday. Else, return false.
	 */
	public boolean isSaturday(Date day) {
		
		Calendar c = Calendar.getInstance();
		c.setTime(day);
		int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
		
		if (dayOfWeek == 7) {
			return true;
		}
		
		return false;
	}
	
	/*
	 * Given a date, return the int value of the day of the week.
	 * 
	 * Java Calendar Days of Week
	 * 	Sunday: 1
	 * 	Monday: 2
	 * 	Tuesday: 3
	 * 	Wednesday: 4
	 * 	Thursday: 5
	 * 	Friday: 6
	 * 	Saturday: 7
	 */
	public int getWeekDay(Date day) {
		
		Calendar c = Calendar.getInstance();
		c.setTime(day);
		return c.get(Calendar.DAY_OF_WEEK);

	}
	
	/*
	 * Return the date that is "days" days away from "date".
	 * 
	 * If we are the 23rd of a given month:
	 *  
	 * 		getDateFromSetDate({23rd}, 3) ==> 26th
	 * 		
	 * 		getDateFromSetDate({23rd}, -3) ==> 20th
	 */
	public Date getDateFromSetDate(Date date, int days) {
		
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		
		// Add "days" days to the calendar
		c.add(Calendar.DAY_OF_MONTH, days);
		
		// get the corresponding date of adding "days" days
		Date calcDate = c.getTime();
		return calcDate;
	}
	
	/*
	 * Return a HashMap containing <key, value> for <dayOfWeek, daysFromWeekStart>.
	 * 
	 * Example: Since the schedule week starts Saturday, Tuesday would be three days later (three days from) Saturday.
	 */
	public HashMap<Integer, Integer> getDaysFromWeekStartMap() {
		
		// initialize HashMap
		HashMap<Integer, Integer> daysFromStart = new HashMap<Integer, Integer>();
		
		daysFromStart.put(7, 0); // Saturday
		daysFromStart.put(1, 1); // Sunday
		daysFromStart.put(2, 2); // Monday
		daysFromStart.put(3, 3); // Tuesday
		daysFromStart.put(4, 4); // Wednesday
		daysFromStart.put(5, 5); // Thursday
		daysFromStart.put(6, 6); // Friday
		
		return daysFromStart;
	}
	
	/*
	 * Return a HashMap containing <key, value> for <dayOfWeek, daysFromWeekEnd>.
	 * 
	 * Example: Since the schedule week ends Friday, Wednesday would be three days earlier (three days before) Friday.
	 */
	public HashMap<Integer, Integer> getDaysFromWeekEndMap() {
		
		// initialize HashMap
		HashMap<Integer, Integer> daysFromEnd = new HashMap<Integer, Integer>();
		
		daysFromEnd.put(7, 6); // Saturday
		daysFromEnd.put(1, 5); // Sunday
		daysFromEnd.put(2, 4); // Monday
		daysFromEnd.put(3, 3); // Tuesday
		daysFromEnd.put(4, 2); // Wednesday
		daysFromEnd.put(5, 1); // Thursday
		daysFromEnd.put(6, 0); // Friday
		
		return daysFromEnd;
	}
	
}
