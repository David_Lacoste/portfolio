package com.david.util;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class Messages {
	
	private static String BUNDLE_NAME;
	private static ResourceBundle RESOURCE_BUNDLE;
	
	public Messages(String fileName) {
		this.BUNDLE_NAME = fileName;
		this.RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);
	}
	
	public static String getString(String key) {
		try {
			return RESOURCE_BUNDLE.getString(key);
		} catch (MissingResourceException e) {
			return '!' + key + '!';
		}
	}
}
