package com.david.util;

public class MessagesFactory {
	
	public static Messages getMessages(String fileName) {
		
		if (fileName == null) {
			return null;
		}
		
		if (fileName.equalsIgnoreCase("email")) {
			return new Messages("email");
		}
		
		// file is "messages/email.properties"
		if (fileName.equalsIgnoreCase("messages.email")) {
			return new Messages("messages.email");
		}
		
		return null;
	}
}
