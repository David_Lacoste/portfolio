package com.david.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import com.david.validation.category.IsValidCategory;

public class TypeModel {
	
	@NotNull
	@NotEmpty
	@Size(min = 2, max = 150)
	private String value;
	
	@NotNull
	@IsValidCategory
	private int category;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public int getCategory() {
		return category;
	}

	public void setCategory(int category) {
		this.category = category;
	}
}
