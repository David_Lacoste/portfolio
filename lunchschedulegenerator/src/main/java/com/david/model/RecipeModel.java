package com.david.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import com.david.validation.location.IsValidLocation;
import com.david.validation.meal.IsValidMeal;
import com.david.validation.season.IsValidSeason;
import com.david.validation.type.IsValidType;

public class RecipeModel {
	
	@NotNull
	@NotEmpty
	@Size(min = 2, max = 150)
	private String name;
	
	@NotNull
	@IsValidType
	private int type;
	
	@NotNull
	@IsValidLocation
	private int location;
	
	@NotNull
	@IsValidSeason
	private int season;
	
	@NotNull
	@IsValidMeal
	private int meal;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getLocation() {
		return location;
	}

	public void setLocation(int location) {
		this.location = location;
	}

	public int getSeason() {
		return season;
	}

	public void setSeason(int season) {
		this.season = season;
	}

	public int getMeal() {
		return meal;
	}

	public void setMeal(int meal) {
		this.meal = meal;
	}
}
