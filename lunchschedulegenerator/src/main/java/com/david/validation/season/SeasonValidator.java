package com.david.validation.season;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.david.entity.SeasonDAO;
import com.david.entity.SeasonEntity;
import com.david.entity.TypeDAO;
import com.david.entity.TypeEntity;

public class SeasonValidator implements ConstraintValidator<IsValidSeason, Integer> {
	
	@Override
	public void initialize(IsValidSeason isValidSeason) {}
	
	@Override
	public boolean isValid(Integer seasonId, ConstraintValidatorContext ctx) {
		
		// make sure the seasonId matches an existing type
		SeasonDAO seasonDAO = new SeasonDAO();
		SeasonEntity season = seasonDAO.findById(seasonId);
		seasonDAO.closeSession();
		
		if (season == null) {
			return false;
		}
		
		return true;
	}
}
