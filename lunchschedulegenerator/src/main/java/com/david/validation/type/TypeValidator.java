package com.david.validation.type;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.david.entity.TypeDAO;
import com.david.entity.TypeEntity;

public class TypeValidator implements ConstraintValidator<IsValidType, Integer> {
	
	@Override
	public void initialize(IsValidType isValidType) {}
	
	@Override
	public boolean isValid(Integer typeId, ConstraintValidatorContext ctx) {
		
		// make sure the typeId matches an existing type
		TypeDAO typeDAO = new TypeDAO();
		TypeEntity type = typeDAO.findById(typeId);
		typeDAO.closeSession();
		
		if (type == null) {
			return false;
		}
		
		return true;
	}
}
