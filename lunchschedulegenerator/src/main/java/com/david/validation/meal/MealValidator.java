package com.david.validation.meal;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.david.entity.MealDAO;
import com.david.entity.MealEntity;
import com.david.entity.TypeDAO;
import com.david.entity.TypeEntity;

public class MealValidator implements ConstraintValidator<IsValidMeal, Integer> {
	
	@Override
	public void initialize(IsValidMeal isValidMeal) {}
	
	@Override
	public boolean isValid(Integer mealId, ConstraintValidatorContext ctx) {
		
		// make sure the mealId matches an existing type
		MealDAO mealDAO = new MealDAO();
		MealEntity meal = mealDAO.findById(mealId);
		mealDAO.closeSession();
		
		if (meal == null) {
			return false;
		}
		
		return true;
	}
}
