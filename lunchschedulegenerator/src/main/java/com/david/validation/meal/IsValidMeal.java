package com.david.validation.meal;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.david.validation.type.TypeValidator;

@Documented
@Constraint(validatedBy = MealValidator.class)
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface IsValidMeal {

	String message() default "The provided meal is not valid";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
