package com.david.validation;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.david.model.UserModel;

@Component
public class LoginFormValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return UserModel.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		
		UserModel user = (UserModel) target;
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "NotEmpty.loginform.username");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty.loginform.password");
		
		if (user.getUsername().equalsIgnoreCase("none")) {
			errors.rejectValue("username", "NotEmpty.loginform.username");
		}
		
		if (user.getUsername().length() < 2 || user.getUsername().length() > 45) {
			errors.rejectValue("username", "Size.loginform.username");
		}
		
		if (user.getPassword().length() < 2 || user.getPassword().length() > 20) {
			errors.rejectValue("password", "Size.loginform.password");
		}
	}
	
}
