package com.david.validation.category;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import org.springframework.context.i18n.LocaleContextHolder;

@Documented
@Constraint(validatedBy = CategoryValidator.class)
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface IsValidCategory {
	
	String message() default "The provided category is not valid";
	
	Class<?>[] groups() default {};
	
	Class<? extends Payload>[] payload() default {};
}
