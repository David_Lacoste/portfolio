package com.david.validation.category;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.david.entity.CategoryDAO;
import com.david.entity.CategoryEntity;

public class CategoryValidator implements ConstraintValidator<IsValidCategory, Integer> {

	@Override
	public void initialize(IsValidCategory isValidCategory) {}

	@Override
	public boolean isValid(Integer categoryId, ConstraintValidatorContext ctx) {
		
		// make sure the categoryId matches an existing category
		CategoryDAO categoryDAO = new CategoryDAO();
		CategoryEntity category = categoryDAO.findById(categoryId);
		categoryDAO.closeSession();
		
		if (category == null) {
			return false;
		}
		
		return true;
	}

}
