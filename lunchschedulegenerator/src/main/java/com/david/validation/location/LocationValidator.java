package com.david.validation.location;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.david.entity.LocationDAO;
import com.david.entity.LocationEntity;

public class LocationValidator implements ConstraintValidator<IsValidLocation, Integer> {
	
	@Override
	public void initialize(IsValidLocation isValidLocation) {}
	
	@Override
	public boolean isValid(Integer locationId, ConstraintValidatorContext ctx) {
		
		// make sure the typeId matches an existing type
		LocationDAO locationDAO = new LocationDAO();
		LocationEntity location = locationDAO.findById(locationId);
		locationDAO.closeSession();
		
		if (location == null) {
			return false;
		}
		
		return true;
	}
	
}
