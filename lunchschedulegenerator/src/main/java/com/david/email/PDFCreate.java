package com.david.email;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import com.david.schedule.ScheduleModel;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

public class PDFCreate {
	
	private static File temp;
	private static String file;
	private static Font titleFont = FontFactory.getFont("Times-Roman", 16, Font.BOLD);
	private static Font tableHeaderFont = FontFactory.getFont("Times-Roman", 12, Font.BOLD);
	private static Font textFont = FontFactory.getFont("Times-Roman", 12);
	private Date START_DATE = new Date();
	private static HashMap<String, Date> weeklyDaysMap = new HashMap<String, Date>();
	
	/*
	 * Constructor
	 */
	public PDFCreate() {
		
		try {
			// create a temp file
			this.temp = File.createTempFile("sample", ".pdf");
			
			// get its absolute location
			this.file = this.temp.getAbsolutePath();
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// populate the hashmap with Date objects in the current week
		this.generateWeeklyDaysMap(this.START_DATE);
	}
	
	public static File getTemp() {
		return temp;
	}

	public static void setTemp(File temp) {
		PDFCreate.temp = temp;
	}

	public static String getFile() {
		return file;
	}

	public static void setFile(String file) {
		PDFCreate.file = file;
	}

	public static Font getTitleFont() {
		return titleFont;
	}

	public static void setTitleFont(Font titleFont) {
		PDFCreate.titleFont = titleFont;
	}

	public static Font getTableHeaderFont() {
		return tableHeaderFont;
	}

	public static void setTableHeaderFont(Font tableHeaderFont) {
		PDFCreate.tableHeaderFont = tableHeaderFont;
	}

	public static Font getTextFont() {
		return textFont;
	}

	public static void setTextFont(Font textFont) {
		PDFCreate.textFont = textFont;
	}
	
	public Date getSTART_DATE() {
		return START_DATE;
	}

	public void setSTART_DATE(Date sTART_DATE) {
		START_DATE = sTART_DATE;
	}

	public static HashMap<String, Date> getWeeklyDaysMap() {
		return weeklyDaysMap;
	}

	public void setWeeklyDaysMap(HashMap<String, Date> weeklyDaysMap) {
		this.weeklyDaysMap = weeklyDaysMap;
	}

	private void generateWeeklyDaysMap(Date startDate) {
		
		// get the weekly start date
		Calendar cal = Calendar.getInstance();
		
		// add each date to the map
		String key = "day";
		
		for (int i = 1; i < 8; i++) {
			
			// reset the calendar to the startDate each time
			cal.setTime(startDate);
			
			// i.e. "day1", "day2", etc...
			key = key + i;
			
			// add a date to the calendar
			cal.add(Calendar.DATE, i - 1);
			Date currDate = cal.getTime();
			
			// add the current weekly date to the hashmap
			// i.e. weeklyDaysMap.put("day1", {DATE})
			this.weeklyDaysMap.put(key, currDate);
			
			// reset the key
			key = "day";
		}
	}
	
	/*
	 * Add the provided metadata parameters to document
	 */
	@SuppressWarnings("unused")
	private static void addMetaData(Document document, String title) {
		document.addTitle(title);
	}
	
	/*
	 * Add the provided metadata parameters to document
	 */
	@SuppressWarnings("unused")
	private static void addMetaData(Document document, String title, String subject) {
		document.addTitle(title);
		document.addSubject(subject);
	}
	
	/*
	 * Add the provided metadata parameters to document
	 */
	@SuppressWarnings("unused")
	private static void addMetaData(Document document, String title, String subject, String author) {
		document.addTitle(title);
		document.addSubject(subject);
		document.addAuthor(author);
	}
	
	
	/*
	 * Add the provided metadata parameters to document
	 */
	@SuppressWarnings("unused")
	private static void addMetaData(Document document, String title, String subject, String author, String creator) {
		document.addTitle(title);
		document.addSubject(subject);
		document.addAuthor(author);
		document.addCreator(creator);
	}
	
	/*
	 * Given a paragraph and a number, adds the number of empty lines into the paragraph
	 */
	private static void addEmptyLine(Paragraph paragraph, int number) {
		for (int i = 0; i < number; i++) {
			paragraph.add(new Paragraph(" "));
		}
	}
	
	/*
	 * Given a Date, return a String with format i.e. "Saturday July 16 2016"
	 */
	private static String formatDate(Date date) {
		
		String pattern = "EEEE MMMMM dd yyyy";
		SimpleDateFormat simpDate = new SimpleDateFormat(pattern);
		
		return simpDate.format(date);
		
	}
	
	/*
	 * Add generated title to a paragraph
	 */
	private static void addGeneratedTitle(Paragraph paragraph) {
		
		HashMap<String, Date> map = getWeeklyDaysMap();
		
		// get the week startDate
		Date startDate = map.get("day1");

		// get the week endDate
		Date endDate = map.get("day7");

		// add the title
		String formattedStartDate = formatDate(startDate);
		String formattedEndDate = formatDate(endDate);
		paragraph.add(new Paragraph("Menu for the Week of " + formattedStartDate + " - " + formattedEndDate));
		
	}
	
	private static void addHeaderCell(PdfPTable table, String headerTitle) {
		
		PdfPCell cell = new PdfPCell(new Phrase(headerTitle));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(cell);
	}
	
	private static void addRegularCell(PdfPTable table, String text) {
		
		table.addCell(text);
	}
	
	private static void addGeneratedTable(Paragraph paragraph, ScheduleModel schedule) {
		
		// create a table with 8 cols
		PdfPTable table = new PdfPTable(8);

		// add all header title cells
		addHeaderCell(table, "Jour");
		addHeaderCell(table, "Samedi");
		addHeaderCell(table, "Dimanche");
		addHeaderCell(table, "Lundi");
		addHeaderCell(table, "Mardi");
		addHeaderCell(table, "Mercredi");
		addHeaderCell(table, "Jeudi");
		addHeaderCell(table, "Vendredi");

		// add first row
		addHeaderCell(table, "Date");
		addRegularCell(table, "18");
		addRegularCell(table, "19");
		addRegularCell(table, "20");
		addRegularCell(table, "21");
		addRegularCell(table, "22");
		addRegularCell(table, "23");
		addRegularCell(table, "24");
		

		// add second row
		addHeaderCell(table, "Info");
		addRegularCell(table, schedule.getDay1Info());
		addRegularCell(table, schedule.getDay2Info());
		addRegularCell(table, schedule.getDay3Info());
		addRegularCell(table, schedule.getDay4Info());
		addRegularCell(table, schedule.getDay5Info());
		addRegularCell(table, schedule.getDay6Info());
		addRegularCell(table, schedule.getDay7Info());

		// add third row
		addHeaderCell(table, "Diner");
		addRegularCell(table, schedule.getDay1Diner());
		addRegularCell(table, schedule.getDay2Diner());
		addRegularCell(table, schedule.getDay3Diner());
		addRegularCell(table, schedule.getDay4Diner());
		addRegularCell(table, schedule.getDay5Diner());
		addRegularCell(table, schedule.getDay6Diner());
		addRegularCell(table, schedule.getDay7Diner());

		// add fourth row
		addHeaderCell(table, "Souper");
		addRegularCell(table, schedule.getDay1Souper());
		addRegularCell(table, schedule.getDay2Souper());
		addRegularCell(table, schedule.getDay3Souper());
		addRegularCell(table, schedule.getDay4Souper());
		addRegularCell(table, schedule.getDay5Souper());
		addRegularCell(table, schedule.getDay6Souper());
		addRegularCell(table, schedule.getDay7Souper());

		paragraph.add(table);
		
	}
	
	private static void addGeneratedTable(Paragraph paragraph) {
		
		HashMap<String, Date> map = getWeeklyDaysMap();
		
		// create a table with 8 cols
		PdfPTable table = new PdfPTable(8);
		
		// add all header title cells
		addHeaderCell(table, "Jour");
		addHeaderCell(table, "Samedi");
		addHeaderCell(table, "Dimanche");
		addHeaderCell(table, "Lundi");
		addHeaderCell(table, "Mardi");
		addHeaderCell(table, "Mercredi");
		addHeaderCell(table, "Jeudi");
		addHeaderCell(table, "Vendredi");
		
		// add first row
		addHeaderCell(table, "Date");
		addRegularCell(table, formatDate(map.get("day1")));
		addRegularCell(table, formatDate(map.get("day2")));
		addRegularCell(table, formatDate(map.get("day3")));
		addRegularCell(table, formatDate(map.get("day4")));
		addRegularCell(table, formatDate(map.get("day5")));
		addRegularCell(table, formatDate(map.get("day6")));
		addRegularCell(table, formatDate(map.get("day7")));
		
		// add second row
		addHeaderCell(table, "Info");
		addRegularCell(table, "-");
		addRegularCell(table, "-");
		addRegularCell(table, "-");
		addRegularCell(table, "-");
		addRegularCell(table, "-");
		addRegularCell(table, "-");
		addRegularCell(table, "-");
		
		// add third row
		addHeaderCell(table, "Diner");
		addRegularCell(table, "-");
		addRegularCell(table, "-");
		addRegularCell(table, "-");
		addRegularCell(table, "-");
		addRegularCell(table, "-");
		addRegularCell(table, "-");
		addRegularCell(table, "-");
		
		// add fourth row
		addHeaderCell(table, "Souper");
		addRegularCell(table, "-");
		addRegularCell(table, "-");
		addRegularCell(table, "-");
		addRegularCell(table, "-");
		addRegularCell(table, "-");
		addRegularCell(table, "-");
		addRegularCell(table, "-");
		
		paragraph.add(table);
		
	}
	
	/*
	 * Create a page with a title and a table
	 */
	private static void generate(Document document) throws DocumentException {
		
		Paragraph preface = new Paragraph();
		
		// add one empty line
		addEmptyLine(preface, 1);
		
		// add the generated title
		addGeneratedTitle(preface);
		
		// add 2 empty lines
		addEmptyLine(preface, 2);
		
		// add the preface section to the document
		document.add(preface);
		
		// add the table
		Paragraph tableParagraph = new Paragraph();
		addGeneratedTable(tableParagraph);
		
		// add 2 empty lines
		addEmptyLine(tableParagraph, 2);
		
		// add the table section to the document
		document.add(tableParagraph);
		
	}
	
	/*
	 * Create a page with a title and a table
	 */
	private static void generate(Document document, ScheduleModel schedule) throws DocumentException {
		
		Paragraph preface = new Paragraph();
		
		// add one empty line
		addEmptyLine(preface, 1);
		
		// add the generated title
		addGeneratedTitle(preface);
		
		// add 2 empty lines
		addEmptyLine(preface, 2);
		
		// add the preface section to the document
		document.add(preface);
		
		// add the table
		Paragraph tableParagraph = new Paragraph();
		addGeneratedTable(tableParagraph, schedule);
		
		// add 2 empty lines
		addEmptyLine(tableParagraph, 2);
		
		// add the table section to the document
		document.add(tableParagraph);
		
	}
	
	/*
	 * Create the PDF document in a temp location and return the file path to the document
	 */
	public static String createPDF() throws DocumentException, IOException {
		
		PDFCreate create = new PDFCreate();
		Document document = new Document();
		
		// create a file output stream for the temp file
		FileOutputStream fos = new FileOutputStream(PDFCreate.getTemp());
		
		PdfWriter.getInstance(document, fos);
		document.open();
		
		PDFCreate.addMetaData(document, "Test PDF", "Test", "David", "David");
		PDFCreate.generate(document);
		
		document.close();
		fos.close();
		
		return PDFCreate.getFile();
	}
	
	public static String createDemoPDF() throws DocumentException, IOException {
		
		PDFCreate create = new PDFCreate();
		Document document = new Document();
		
		// create a file output stream for the temp file
		FileOutputStream fos = new FileOutputStream(PDFCreate.getTemp());
		
		PdfWriter.getInstance(document, fos);
		document.open();
		
		// Create Demo ScheduleModel
		ScheduleModel schedule = new ScheduleModel();
		schedule.generateDemoSchedule();
		
		PDFCreate.addMetaData(document, "Demo PDF", "Test", "LunchScheduleGenerator", "LunchScheduleGenerator");
		PDFCreate.generate(document, schedule);
		
		document.close();
		fos.close();
		
		return PDFCreate.getFile();
	}
}
