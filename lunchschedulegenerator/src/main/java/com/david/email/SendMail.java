package com.david.email;

import java.io.IOException;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.log4j.Logger;

import com.david.controller.LoginController;
import com.david.util.Messages;
import com.david.util.MessagesFactory;
import com.lowagie.text.DocumentException;

public class SendMail {
	
	// logger
	private static final Logger logger = Logger.getLogger(LoginController.class);
	
	public SendMail() {}
	
	@SuppressWarnings("static-access")
	public boolean sendMail(String recipient, String subject, String body, boolean demo) {
		
		// get email credentials from email.properties file
		Messages emailInfo = MessagesFactory.getMessages("email");
		
		final String username = emailInfo.getString("email");
		final String password = emailInfo.getString("password");
		final String smtpAuth = emailInfo.getString("smtp-auth");
		final String startTls = emailInfo.getString("starttls-enable");
		final String smtpHost = emailInfo.getString("smtp-host");
		final String smtpPort = emailInfo.getString("smtp-port");
		
		Properties props = System.getProperties();
		props.put("mail.smtp.auth", smtpAuth);
		props.put("mail.smtp.starttls.enable", startTls);
		props.put("mail.smtp.host", smtpHost);
		props.put("mail.smtp.port", smtpPort);
		
		// authenticate password
		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});
		
		try {
			
			// build the message
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(username));
			message.setRecipients(Message.RecipientType.TO, 
					InternetAddress.parse(recipient));
			message.setSubject(subject);
			
			BodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setText(body);
			
			// add the body
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);
			
			// add the attachment
			messageBodyPart = new MimeBodyPart(); // create new object
			
			// create the PDF and get the file path of the temp file
			PDFCreate pdfCreate = new PDFCreate();
			String filePath;
			
			if (demo) {
				filePath = pdfCreate.createDemoPDF();
			} else {
				filePath = pdfCreate.createPDF();
			}
			
			logger.info("PDF Filepath: " + filePath);
			logger.info("PDF object filepath: " + pdfCreate.getFile());
			logger.info("Tmp dir: " + System.getProperty("java.io.tmpdir"));
			
			// attach to email
			DataSource source = new FileDataSource(filePath);
			messageBodyPart.setDataHandler(new DataHandler(source));
			messageBodyPart.setFileName("WeeklySchedule.pdf");
			multipart.addBodyPart(messageBodyPart);
			message.setContent(multipart);
			
			// send message
			Transport.send(message);
			
		} catch (MessagingException | DocumentException | IOException e) {
			e.printStackTrace();
			return false;
		} finally {
			PDFCreate.getTemp().delete();
		}
		
		return true;
	}
	
}
