package com.david.entity;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

import com.david.security.PasswordEncoder;

public class UserDAO extends AbstractDAO {

	public UserDAO() {
		super();
	}

	public List<UserEntity> findAll() {

		Query query = this.getSession().createQuery("FROM UserEntity WHERE is_valid = :is_valid");
		query.setBoolean("is_valid", true);
		List<UserEntity> users = query.list();
		return users;
	}
	
	public UserEntity findById(int id) {
		
		Query query = this.getSession().createQuery("FROM UserEntity WHERE id = :id AND is_valid = :is_valid");
		query.setInteger("id", id);
		query.setBoolean("is_valid", true);
		List<UserEntity> userEntities = query.list();
		
		// there should only be one entity in the list
		if (userEntities.size() < 1) {
			return null;
		} else if (userEntities.size() > 1) {
			return null;
		}
		
		return userEntities.get(0);
	}
	
	public UserEntity findByUsername(String username) {
		
		Query query = this.getSession().createQuery("FROM UserEntity WHERE username = :username AND is_valid = :is_valid");
		query.setString("username", username);
		query.setBoolean("is_valid", true);
		List<UserEntity> userEntities = query.list();
		
		// there should only be one entity in the list
		if (userEntities.size() < 1) {
			return null;
		} else if (userEntities.size() > 1) {
			return null;
		}
		
		return userEntities.get(0);
	}
	
	public Integer findIdByUsername(String username) {
		
		Query query = this.getSession().createQuery("FROM UserEntity WHERE username = :username AND is_valid = :is_valid");
		query.setString("username", username);
		query.setBoolean("is_valid", true);
		List<UserEntity> userEntities = query.list();
		
		// there should only be one entity in the list
		if (userEntities.size() < 1) {
			return null;
		} else if (userEntities.size() > 1) {
			return null;
		}
		
		return userEntities.get(0).getId();
	}
	
	public String findUsernameById(Integer id) {
		
		Query query = this.getSession().createQuery("FROM UserEntity WHERE id = :id AND is_valid = :is_valid");
		query.setInteger("id", id);
		query.setBoolean("is_valid", true);
		List<UserEntity> userEntities = query.list();
		
		// there should only be one entity in the list
		if (userEntities.size() < 1) {
			return null;
		} else if (userEntities.size() > 1) {
			return null;
		}
		
		return userEntities.get(0).getUsername();
	}
	
	public boolean createNewUser(String username, String password, UserRoleEntity role, boolean is_valid) {
		
		try {
			
			// Encrypt password
			PasswordEncoder encoder = new PasswordEncoder();
			String hashedPassword = encoder.encode(password);
			
			// begin transaction
			this.getSession().beginTransaction();
			
			// create the entity
			Date now = new Date();
			UserEntity userEntity = new UserEntity();
			userEntity.setUsername(username);
			userEntity.setPassword(hashedPassword);
			userEntity.setRole(role);
			userEntity.setCreated_at(now);
			userEntity.setUpdated_at(now);
			userEntity.setIs_valid(true);
			
			// persist
			this.getSession().save(userEntity);
			this.getSession().getTransaction().commit();
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
}
