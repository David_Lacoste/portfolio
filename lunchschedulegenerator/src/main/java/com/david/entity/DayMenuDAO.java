package com.david.entity;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

public class DayMenuDAO extends AbstractDAO {

	public DayMenuDAO() {
		super();
	}

	public List<DayMenuEntity> findAll() {

		Query query = this.getSession().createQuery("FROM DayMenuEntity WHERE is_valid = :is_valid");
		query.setBoolean("is_valid", true);
		List<DayMenuEntity> dayMenus = query.list();
		return dayMenus;
	}
}
