package com.david.entity;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;

public class LocationDAO extends AbstractDAO {

	public LocationDAO() {
		super();
	}

	public List<LocationEntity> findAll() {

		Query query = this.getSession().createQuery("FROM LocationEntity WHERE is_valid = :is_valid");
		query.setBoolean("is_valid", true);
		List<LocationEntity> locations = query.list();
		return locations;
	}
	
	public List<LocationEntity> findAllByUser(int user) {
		
		// get user
		UserDAO userDAO = new UserDAO();
		UserEntity userEntity = userDAO.findById(user);
		userDAO.closeSession();
		
		Query query = this.getSession().createQuery("FROM LocationEntity WHERE is_valid = :is_valid AND user = :user");
		query.setBoolean("is_valid", true);
		query.setInteger("user", userEntity.getId());
		List<LocationEntity> locations = query.list();
		return locations;
	}
	
	public LocationEntity findById(int id) {
		
		Query query = this.getSession().createQuery("FROM LocationEntity WHERE is_valid = :is_valid AND id = :id");
		query.setInteger("id", id);
		query.setBoolean("is_valid", true);
		List<LocationEntity> locations = query.list();
		
		// there should only be one entity in the list
		if (locations.size() < 1) {
			return null;
		} else if (locations.size() > 1) {
			return null;
		}
		
		return locations.get(0);
	}
	
	public LocationEntity findById(int id, int user) {
		
		Query query = this.getSession().createQuery("FROM LocationEntity WHERE is_valid = :is_valid AND id = :id AND user = :user");
		query.setBoolean("is_valid", true);
		query.setInteger("id", id);
		query.setInteger("user", user);
		List<LocationEntity> locations = query.list();
		
		// there should only be one entity in the list
		if (locations.size() < 1) {
			return null;
		} else if (locations.size() > 1) {
			return null;
		}
		
		return locations.get(0);
	}
	
	public boolean deleteById(int id) {
		
		try {
			
			LocationEntity location = (LocationEntity) this.getSession().get(LocationEntity.class, id);
			
			this.getSession().beginTransaction();
			this.getSession().delete(location);
			this.getSession().getTransaction().commit();
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public boolean updateById(int id, boolean is_valid, String value, int user) {
		
		try {
			
			// get user
			UserDAO userDAO = new UserDAO();
			UserEntity userEntity = userDAO.findById(user);
			userDAO.closeSession();
			
			LocationEntity location = (LocationEntity) this.getSession().get(LocationEntity.class, id);
			
			Date now = new Date();
			
			// modify the entity
			location.setIs_valid(is_valid);
			location.setValue(value);
			location.setUpdated_at(now);
			location.setUser(userEntity);
			
			this.getSession().beginTransaction();
			this.getSession().update(location);
			this.getSession().getTransaction().commit();
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public boolean createNewLocation(String value, boolean is_valid, int user) {
		
		try {
			
			Date now = new Date();
			
			// get user
			UserDAO userDAO = new UserDAO();
			UserEntity userEntity = userDAO.findById(user);
			userDAO.closeSession();
			
			// create the object
			LocationEntity locationEntity = new LocationEntity();
			locationEntity.setValue(value);
			locationEntity.setCreated_at(now);
			locationEntity.setUpdated_at(now);
			locationEntity.setIs_valid(is_valid);
			locationEntity.setUser(userEntity);
			
			// persist
			this.getSession().beginTransaction();
			this.getSession().save(locationEntity);
			this.getSession().getTransaction().commit();
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
		
	}
}
