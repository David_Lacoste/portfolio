package com.david.entity;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

public class MealDAO extends AbstractDAO {

	public MealDAO() {
		super();
	}

	public List<MealEntity> findAll() {

		Query query = this.getSession().createQuery("FROM MealEntity WHERE is_valid = :is_valid");
		query.setBoolean("is_valid", true);
		List<MealEntity> meals = query.list();
		return meals;
	}

	public MealEntity findById(int id) {

		try {

			MealEntity meal = (MealEntity) this.getSession().get(MealEntity.class, id);
			return meal;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
