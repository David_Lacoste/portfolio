package com.david.entity;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

import com.david.util.CalendarUtil;

public class WeekMenuDAO extends AbstractDAO {

	public WeekMenuDAO() {
		super();
	}

	public List<WeekMenuEntity> findAll() {

		Query query = this.getSession().createQuery("FROM WeekMenuEntity WHERE is_valid = :is_valid");
		query.setBoolean("is_valid", true);
		List<WeekMenuEntity> weekMenus = query.list();
		return weekMenus;
	}
	
	public WeekMenuEntity findById(int id) {
		
		Query query = this.getSession().createQuery("FROM WeekMenuEntity WHERE is_valid = :is_valid AND id = :id");
		query.setBoolean("is_valid", true);
		query.setInteger("id", id);
		List<WeekMenuEntity> weekMenus = query.list();
		
		// there should only be one entity in the list
		if (weekMenus.size() < 1) {
			return null;
		} else if (weekMenus.size() > 1) {
			return null;
		}
		
		return weekMenus.get(0);
	}
	
	/*
	 * Given a current date, return true if a schedule exists for the user containing curDate. Else, return false.
	 */
	public boolean weekMenuExists(int user, Date curDate) {
		
//		Query query = this.getSession().createQuery("FROM WeekMenuEntity WHERE is_valid = 1 AND ")
		
		CalendarUtil calUtil = new CalendarUtil();
		boolean saturday = calUtil.isSaturday(curDate);
		
		if (saturday) {
			// get the endDate of the weekly schedule (the upcoming Friday)
			Date endDate = calUtil.getDateFromSetDate(curDate, 6);
			
			return this.weekMenuExists(user, curDate, endDate);
			
		} else {
			
			// get HashMap
			HashMap<Integer, Integer> map = calUtil.getDaysFromWeekStartMap();
			
			// get int value for week day
			int weekDay = calUtil.getWeekDay(curDate);
			
			// get number of days from start of week (Saturday)
			int numDaysFromStart = map.get(weekDay);
			
			// get week start date
			Date startDate = calUtil.getDateFromSetDate(curDate, (numDaysFromStart * -1)); // negative number because we need to look backwards
			
			// get different HashMap
			map = calUtil.getDaysFromWeekEndMap();
			
			// get number of days until end of week (Friday)
			int numDaysUntilEnd = map.get(weekDay);
			
			// get week end date
			Date endDate = calUtil.getDateFromSetDate(curDate, numDaysUntilEnd);
			
		}
		
		return true;
	}
	
	/*
	 * Given a startDate and an endDate, return true if a schedule exists for the user starting on startDate and ending
	 * on endDate. Else, return false.
	 */
	public boolean weekMenuExists(int user, Date startDate, Date endDate) {
		
		try {
			
			Query query = this.getSession().createQuery("FROM WeekMenuEntity WHERE is_valid = :is_valid AND date_from = :date_from AND date_to = :date_to AND user = :user");
			query.setBoolean("is_valid", true);
			query.setDate("date_from", startDate);
			query.setDate("date_to", endDate);
			query.setInteger("user", user);
			List<WeekMenuEntity> weekMenus = query.list();
			
			if (weekMenus.size() > 0) {
				return true;
			} else {
				return false;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}
}
