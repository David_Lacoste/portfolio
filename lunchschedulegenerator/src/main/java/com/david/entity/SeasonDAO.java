package com.david.entity;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

public class SeasonDAO extends AbstractDAO {

	public SeasonDAO() {
		super();
	}

	public List<SeasonEntity> findAll() {
		
		try {
			
			Query query = this.getSession().createQuery("FROM SeasonEntity WHERE is_valid = :is_valid");
			query.setBoolean("is_valid", true);
			List<SeasonEntity> seasons = query.list();
			return seasons;
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}
	
	public SeasonEntity findById(int id) {
		
		try {
			
			SeasonEntity season = (SeasonEntity) this.getSession().get(SeasonEntity.class, id);
			return season;
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}
}
