package com.david.entity;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;

public class TypeDAO extends AbstractDAO {

	public TypeDAO() {
		super();
	}

	public List<TypeEntity> findAll() {

		Query query = this.getSession().createQuery("FROM TypeEntity WHERE is_valid = :is_valid");
		query.setBoolean("is_valid", true);
		List<TypeEntity> types = query.list();
		return types;
	}
	
	public List<TypeEntity> findAllByUser(int user) {
		
		// get user
		UserDAO userDAO = new UserDAO();
		UserEntity userEntity = userDAO.findById(user);
		userDAO.closeSession();
		
		Query query = this.getSession().createQuery("FROM TypeEntity WHERE is_valid = :is_valid AND user = :user");
		query.setBoolean("is_valid", true);
		query.setInteger("user", user);
		List<TypeEntity> types = query.list();
		return types;
	}
	
	public TypeEntity findById(int id) {
		
		Query query = this.getSession().createQuery("FROM TypeEntity WHERE is_valid = :is_valid AND id = :id");
		query.setBoolean("is_valid", true);
		query.setInteger("id", id);
		List<TypeEntity> types = query.list();
		
		// there should only be one entity in the list
		if (types.size() < 1) {
			return null;
		} else if (types.size() > 1) {
			return null;
		}
		
		return types.get(0);
		
	}
	
	public TypeEntity findById(int id, int user) {
		
		Query query = this.getSession().createQuery("FROM TypeEntity WHERE is_valid = :is_valid AND id = :id AND user = :user");
		query.setBoolean("is_valid", true);
		query.setInteger("id", id);
		query.setInteger("user", user);
		List<TypeEntity> types = query.list();

		// there should only be one entity in the list
		if (types.size() < 1) {
			return null;
		} else if (types.size() > 1) {
			return null;
		}

		return types.get(0);
	}
	
	public boolean deleteById(int id) {
		
		try {
			
			TypeEntity type = (TypeEntity) this.getSession().get(TypeEntity.class, id);
			
			this.getSession().beginTransaction();
			this.getSession().delete(type);
			this.getSession().getTransaction().commit();
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public boolean updateById(int id, int category, boolean is_valid, String value, int user) {
		
		try {
			
			// get category
			CategoryDAO categoryDAO = new CategoryDAO();
			CategoryEntity categoryEntity = categoryDAO.findById(category);
			categoryDAO.closeSession();
			
			// get user
			UserDAO userDAO = new UserDAO();
			UserEntity userEntity = userDAO.findById(user);
			userDAO.closeSession();
			
			TypeEntity type = (TypeEntity) this.getSession().get(TypeEntity.class, id);
			
			Date now = new Date();
			
			// modify the entity
			type.setCategory(categoryEntity);
			type.setIs_valid(is_valid);
			type.setValue(value);
			type.setUpdated_at(now);
			type.setUser(userEntity);
			
			this.getSession().beginTransaction();
			this.getSession().update(type);
			this.getSession().getTransaction().commit();
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
		
	}
	
	public boolean createNewType(String value, int category, boolean is_valid, int user) {
		
		try {
			
			// get category
			CategoryDAO categoryDAO = new CategoryDAO();
			CategoryEntity categoryEntity = categoryDAO.findById(category);
			categoryDAO.closeSession();
			
			// get user
			UserDAO userDAO = new UserDAO();
			UserEntity userEntity = userDAO.findById(user);
			userDAO.closeSession();
			
			Date now = new Date();
			
			// create the object
			TypeEntity typeEntity = new TypeEntity();
			typeEntity.setValue(value);
			typeEntity.setCategory(categoryEntity);
			typeEntity.setCreated_at(now);
			typeEntity.setUpdated_at(now);
			typeEntity.setIs_valid(is_valid);
			typeEntity.setUser(userEntity);
			
			// persist
			this.getSession().beginTransaction();
			this.getSession().save(typeEntity);
			this.getSession().getTransaction().commit();
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
		
	}
}
