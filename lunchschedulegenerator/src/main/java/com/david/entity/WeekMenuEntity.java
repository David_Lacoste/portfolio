package com.david.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "week_menu")
public class WeekMenuEntity {
	
	@Id
	@SequenceGenerator(name = "week_menu_id_seq", sequenceName = "week_menu_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "week_menu_id_seq")
	@Column(name = "id", updatable = false)
	private int id;
	
	@Column(name = "date_from", columnDefinition = "DATETIME", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date date_from;
	
	@Column(name = "date_to", columnDefinition = "DATETIME", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date date_to;
	
	@OneToOne
	@JoinColumn(name = "day1", nullable = false)
	private DayMenuEntity day1;
	
	@OneToOne
	@JoinColumn(name = "day2", nullable = false)
	private DayMenuEntity day2;
	
	@OneToOne
	@JoinColumn(name = "day3", nullable = false)
	private DayMenuEntity day3;
	
	@OneToOne
	@JoinColumn(name = "day4", nullable = false)
	private DayMenuEntity day4;
	
	@OneToOne
	@JoinColumn(name = "day5", nullable = false)
	private DayMenuEntity day5;
	
	@OneToOne
	@JoinColumn(name = "day6", nullable = false)
	private DayMenuEntity day6;
	
	@OneToOne
	@JoinColumn(name = "day7", nullable = false)
	private DayMenuEntity day7;
	
	@Column(name = "created_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date created_at;
	
	@Column(name = "updated_at", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date updated_at;
	
	@Column(name = "is_valid", nullable = false)
	private boolean is_valid;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDate_from() {
		return date_from;
	}

	public void setDate_from(Date date_from) {
		this.date_from = date_from;
	}

	public Date getDate_to() {
		return date_to;
	}

	public void setDate_to(Date date_to) {
		this.date_to = date_to;
	}

	public DayMenuEntity getDay1() {
		return day1;
	}

	public void setDay1(DayMenuEntity day1) {
		this.day1 = day1;
	}

	public DayMenuEntity getDay2() {
		return day2;
	}

	public void setDay2(DayMenuEntity day2) {
		this.day2 = day2;
	}

	public DayMenuEntity getDay3() {
		return day3;
	}

	public void setDay3(DayMenuEntity day3) {
		this.day3 = day3;
	}

	public DayMenuEntity getDay4() {
		return day4;
	}

	public void setDay4(DayMenuEntity day4) {
		this.day4 = day4;
	}

	public DayMenuEntity getDay5() {
		return day5;
	}

	public void setDay5(DayMenuEntity day5) {
		this.day5 = day5;
	}

	public DayMenuEntity getDay6() {
		return day6;
	}

	public void setDay6(DayMenuEntity day6) {
		this.day6 = day6;
	}

	public DayMenuEntity getDay7() {
		return day7;
	}

	public void setDay7(DayMenuEntity day7) {
		this.day7 = day7;
	}

	public Date getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}

	public Date getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(Date updated_at) {
		this.updated_at = updated_at;
	}

	public boolean isIs_valid() {
		return is_valid;
	}

	public void setIs_valid(boolean is_valid) {
		this.is_valid = is_valid;
	}
}
