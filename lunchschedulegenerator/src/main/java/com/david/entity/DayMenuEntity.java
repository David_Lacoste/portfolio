package com.david.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "day_menu")
public class DayMenuEntity {
	
	@Id
	@SequenceGenerator(name = "day_menu_id_seq", sequenceName = "day_menu_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "day_menu_id_seq")
	@Column(name = "id", updatable = false)
	private int id;
	
	@Column(name = "day", columnDefinition = "DATETIME", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date day;
	
	@Column(name = "info")
	private String info;
	
	@OneToOne
	@JoinColumn(name = "diner", nullable = false)
	private RecipeEntity diner;
	
	@OneToOne
	@JoinColumn(name = "souper", nullable = false)
	private RecipeEntity souper;
	
	@Column(name = "created_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date created_at;
	
	@Column(name = "updated_at", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date updated_at;
	
	@Column(name = "is_valid", nullable = false)
	private boolean is_valid;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDay() {
		return day;
	}

	public void setDay(Date day) {
		this.day = day;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public RecipeEntity getDiner() {
		return diner;
	}

	public void setDiner(RecipeEntity diner) {
		this.diner = diner;
	}

	public RecipeEntity getSouper() {
		return souper;
	}

	public void setSouper(RecipeEntity souper) {
		this.souper = souper;
	}

	public Date getCreated_at() {
		return created_at;
	}

	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}

	public Date getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(Date updated_at) {
		this.updated_at = updated_at;
	}

	public boolean isIs_valid() {
		return is_valid;
	}

	public void setIs_valid(boolean is_valid) {
		this.is_valid = is_valid;
	}
}
