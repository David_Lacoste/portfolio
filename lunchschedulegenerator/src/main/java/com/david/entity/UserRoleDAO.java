package com.david.entity;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

public class UserRoleDAO extends AbstractDAO {

	public UserRoleDAO() {
		super();
	}

	public List<UserRoleEntity> findAll() {

		Query query = this.getSession().createQuery("FROM UserRoleEntity WHERE is_valid = :is_valid");
		query.setBoolean("is_valid", true);
		List<UserRoleEntity> userRoles = query.list();
		return userRoles;
	}
	
	public Integer findIdByRole(String role) {
		
		Query query = this.getSession().createQuery("FROM UserRoleEntity WHERE role = :role");
		query.setString("role", role);
		List<UserRoleEntity> userRoles = query.list();
		
		// there should only be one in the list
		if (userRoles.size() < 1) {
			return null;
		} else if (userRoles.size() > 1) {
			return null;
		}
		
		return userRoles.get(0).getId();
	}
	
	public UserRoleEntity findByRole(String role) {
		
		Query query = this.getSession().createQuery("FROM UserRoleEntity WHERE role = :role");
		query.setString("role", role);
		List<UserRoleEntity> userRoles = query.list();
		
		// there should only be one in the list
		if (userRoles.size() < 1) {
			return null;
		} else if (userRoles.size() > 1) {
			return null;
		}
		
		return userRoles.get(0);
		
	}
}
