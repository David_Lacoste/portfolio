package com.david.entity;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

public abstract class AbstractDAO {
	
	Session session;
	SessionFactory sessionFactory;
	
	public AbstractDAO() {
		this.session = this.openSession();
	}
	
	public Session openSession() {

		this.sessionFactory = new AnnotationConfiguration().configure().buildSessionFactory();
		Session session = sessionFactory.openSession();
		return session;

	}

	public void closeSession() {
		
		this.session.disconnect();
		this.session.close();
		this.sessionFactory.close();

	}
	
	public Session getSession() {
		return this.session;
	}
	
	public SessionFactory getSessionFactory() {
		return this.sessionFactory;
	}
}
