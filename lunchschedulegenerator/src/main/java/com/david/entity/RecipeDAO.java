package com.david.entity;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;

public class RecipeDAO extends AbstractDAO {
	
	public RecipeDAO() {
		super();
	}
	
	public List<RecipeEntity> findAll() {

		Query query = this.getSession().createQuery("FROM RecipeEntity WHERE is_valid = :is_valid");
		query.setBoolean("is_valid", true);
		List<RecipeEntity> recipes = query.list();
		return recipes;
	}
	
	public List<RecipeEntity> findAllByUser(int user) {
		
		// get user
		UserDAO userDAO = new UserDAO();
		UserEntity userEntity = userDAO.findById(user);
		userDAO.closeSession();
		
		Query query = this.getSession().createQuery("FROM RecipeEntity WHERE is_valid = :is_valid AND user = :user");
		query.setBoolean("is_valid", true);
		query.setInteger("user", userEntity.getId());
		List<RecipeEntity> recipes = query.list();
		return recipes;
	}
	
	public List<RecipeEntity> findAllByCategory(int user, int category) {
		
		// get user
		UserDAO userDAO = new UserDAO();
		UserEntity userEntity = userDAO.findById(user);
		userDAO.closeSession();
		
		Query query = this.getSession().createQuery("FROM RecipeEntity WHERE is_valid = :is_valid AND user = :user AND type.category = :category");
		query.setBoolean("is_valid", true);
		query.setInteger("user", userEntity.getId());
		query.setInteger("category", category);
		List<RecipeEntity> recipes = query.list();
		return recipes;
	}
	
	public RecipeEntity findById(int id) {
		
		Query query = this.getSession().createQuery("FROM RecipeEntity WHERE id = :id AND is_valid = :is_valid");
		query.setParameter("id", id);
		query.setBoolean("is_valid", true);
		List<RecipeEntity> recipes = query.list();
		
		// there should only be one entity in the list
		if (recipes.size() < 1) {
			return null;
		} else if (recipes.size() > 1) {
			return null;
		}
		
		return recipes.get(0);
		
	}
	
	public RecipeEntity findById(int id, int user) {
		
		Query query = this.getSession().createQuery("FROM RecipeEntity WHERE is_valid = :is_valid AND id = :id AND user = :user");
		query.setBoolean("is_valid", true);
		query.setInteger("id", id);
		query.setInteger("user", user);
		List<RecipeEntity> recipes = query.list();
		
		// there should only be one entity in the list
		if (recipes.size() < 1) {
			return null;
		} else if (recipes.size() > 1) {
			return null;
		}

		return recipes.get(0);
	}
	
	public boolean deleteById(int id) {
		
		try {
			
			RecipeEntity recipe = (RecipeEntity) this.getSession().get(RecipeEntity.class, id);
			
			this.getSession().beginTransaction();
			this.getSession().delete(recipe);
			this.getSession().getTransaction().commit();
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public boolean updateById(int id, String name, int type, int location, int season, int meal, boolean is_valid, int user) {
		
		try {
			
			// get type
			TypeDAO typeDAO = new TypeDAO();
			TypeEntity typeEntity = typeDAO.findById(type);
			typeDAO.closeSession();
			
			// get location
			LocationDAO locationDAO = new LocationDAO();
			LocationEntity locationEntity = locationDAO.findById(location);
			locationDAO.closeSession();
			
			// get season
			SeasonDAO seasonDAO = new SeasonDAO();
			SeasonEntity seasonEntity = seasonDAO.findById(season);
			seasonDAO.closeSession();
			
			// get meal
			MealDAO mealDAO = new MealDAO();
			MealEntity mealEntity = mealDAO.findById(meal);
			mealDAO.closeSession();
			
			// get user
			UserDAO userDAO = new UserDAO();
			UserEntity userEntity = userDAO.findById(user);
			userDAO.closeSession();
			
			// get current recipe
			RecipeEntity recipe = (RecipeEntity) this.getSession().get(RecipeEntity.class, id);
			
			Date now = new Date();
			
			// modify the entity
			recipe.setName(name);
			recipe.setType(typeEntity);
			recipe.setLocation(locationEntity);
			recipe.setSeason(seasonEntity);
			recipe.setMeal(mealEntity);
			recipe.setIs_valid(is_valid);
			recipe.setUpdated_at(now);
			recipe.setUser(userEntity);
			
			this.getSession().beginTransaction();
			this.getSession().update(recipe);
			this.getSession().getTransaction().commit();
			
		} catch (Exception e){
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public boolean createNewRecipe(String name, int type, int location, int season, int meal, boolean is_valid, int user) {
		
		try {
			
			// get entities
			TypeDAO typeDAO = new TypeDAO();
			TypeEntity typeEntity = typeDAO.findById(type);
			typeDAO.closeSession();
			
			LocationDAO locationDAO = new LocationDAO();
			LocationEntity locationEntity = locationDAO.findById(location);
			locationDAO.closeSession();
			
			SeasonDAO seasonDAO = new SeasonDAO();
			SeasonEntity seasonEntity = seasonDAO.findById(season);
			seasonDAO.closeSession();
			
			MealDAO mealDAO = new MealDAO();
			MealEntity mealEntity = mealDAO.findById(meal);
			mealDAO.closeSession();
			
			UserDAO userDAO = new UserDAO();
			UserEntity userEntity = userDAO.findById(user);
			userDAO.closeSession();
			
			Date now = new Date();
			
			// create the object
			RecipeEntity recipeEntity = new RecipeEntity();
			recipeEntity.setName(name);
			recipeEntity.setType(typeEntity);
			recipeEntity.setLocation(locationEntity);
			recipeEntity.setSeason(seasonEntity);
			recipeEntity.setMeal(mealEntity);
			recipeEntity.setCreated_at(now);
			recipeEntity.setUpdated_at(now);
			recipeEntity.setIs_valid(is_valid);
			recipeEntity.setUser(userEntity);
			
			// persist
			this.getSession().beginTransaction();
			this.getSession().save(recipeEntity);
			this.getSession().getTransaction().commit();
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
}
