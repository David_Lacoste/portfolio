package com.david.entity;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

public class CategoryDAO extends AbstractDAO {
	
	public CategoryDAO() {
		super();
	}
	
	public List<CategoryEntity> findAll() {
		
		Query query = this.getSession().createQuery("FROM CategoryEntity WHERE is_valid = :is_valid");
		query.setBoolean("is_valid", true);
		List<CategoryEntity> categories = query.list();
		return categories;
	}
	
	public CategoryEntity findById(int id) {
		
		try {
			CategoryEntity category = (CategoryEntity) this.getSession().get(CategoryEntity.class, id);
			return category;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
}
