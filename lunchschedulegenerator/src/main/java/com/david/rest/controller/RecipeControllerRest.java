package com.david.rest.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.david.entity.RecipeDAO;
import com.david.entity.UserDAO;
import com.david.entity.UserEntity;
import com.david.rest.resource.DeleteRecipeResponse;
import com.david.rest.resource.UpdateRecipeRequest;
import com.david.rest.resource.UpdateRecipeResponse;
import com.david.util.CookieAccess;

@RestController
@RequestMapping(value = "/REST")
public class RecipeControllerRest {
	
	@RequestMapping(value = "/recipe/{id}/delete", method = RequestMethod.DELETE)
	public @ResponseBody DeleteRecipeResponse delete(@PathVariable("id") int id) {
		
		// query
		RecipeDAO recipeDAO = new RecipeDAO();
		boolean success = recipeDAO.deleteById(id);
		recipeDAO.closeSession();
		
		if (success) {
			return new DeleteRecipeResponse(1, "Recipe delete successful");
		} else {
			return new DeleteRecipeResponse(-1, "Recipe delete was not successful");
		}
	}
	
	@RequestMapping(value = "/recipe/{id}/update", method = RequestMethod.PUT, consumes = {"application/json"}, produces = "application/json")
	public @ResponseBody UpdateRecipeResponse update(@RequestBody UpdateRecipeRequest recipe) {
		
		// get JSON request values
		int id = recipe.getId();
		String name = recipe.getName();
		int type = recipe.getType();
		int location = recipe.getLocation();
		int season = recipe.getSeason();
		int meal = recipe.getMeal();
		boolean is_valid = recipe.isIs_valid();
		
		// get user info from cookie
		String username = CookieAccess.getUsername();
		String role = CookieAccess.getRole();

		// get user from username
		UserDAO userDAO = new UserDAO();
		UserEntity userEntity = userDAO.findByUsername(username);
		userDAO.closeSession();
		
		// update recipe
		RecipeDAO recipeDAO = new RecipeDAO();
		boolean success = recipeDAO.updateById(id, name, type, location, season, meal, is_valid, userEntity.getId());
		recipeDAO.closeSession();
		
		if (success) {
			return new UpdateRecipeResponse(1, "Update recipe successful", id, name, type, location, season, meal, is_valid);
		} else {
			return new UpdateRecipeResponse(-1, "Update recipe not successful");
		}
	}
}
