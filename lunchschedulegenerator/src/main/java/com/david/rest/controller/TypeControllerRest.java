package com.david.rest.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.david.entity.CategoryDAO;
import com.david.entity.CategoryEntity;
import com.david.entity.TypeDAO;
import com.david.entity.UserDAO;
import com.david.entity.UserEntity;
import com.david.rest.resource.DeleteTypeResponse;
import com.david.rest.resource.UpdateTypeRequest;
import com.david.rest.resource.UpdateTypeResponse;
import com.david.util.CookieAccess;

@RestController
@RequestMapping(value = "/REST")
public class TypeControllerRest {
	
	@RequestMapping(value = "/type/{id}/delete", method = RequestMethod.DELETE)
	public @ResponseBody DeleteTypeResponse delete(@PathVariable("id") int id) {
		
		// query
		TypeDAO typeDAO = new TypeDAO();
		boolean success = typeDAO.deleteById(id);
		typeDAO.closeSession();
		
		if (success) {
			return new DeleteTypeResponse(1, "Type delete successful");
		} else {
			return new DeleteTypeResponse(-1, "Type delete was not successful");
		}
	}
	
	@RequestMapping(value = "/type/{id}/update", method = RequestMethod.PUT, consumes = {"application/json"}, produces = "application/json")
	public @ResponseBody UpdateTypeResponse update(@RequestBody UpdateTypeRequest type) {
		
		// get JSON request values
		int id = type.getId();
		int categoryId = type.getCategory();
		boolean is_valid = type.isIs_valid();
		String value = type.getValue();
		
		// get user info from cookie
		String username = CookieAccess.getUsername();
		String role = CookieAccess.getRole();

		// get user from username
		UserDAO userDAO = new UserDAO();
		UserEntity userEntity = userDAO.findByUsername(username);
		userDAO.closeSession();
		
		// update type
		TypeDAO typeDAO = new TypeDAO();
		boolean success = typeDAO.updateById(id, categoryId, is_valid, value, userEntity.getId());
		typeDAO.closeSession();
		
		if (success) {
			return new UpdateTypeResponse(1, "Update type successful", id, categoryId, is_valid, value);
		} else {
			return new UpdateTypeResponse(-1, "Update type not successful");
		}
	}
}
