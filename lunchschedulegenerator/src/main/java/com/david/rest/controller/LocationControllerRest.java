package com.david.rest.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.david.entity.LocationDAO;
import com.david.entity.LocationEntity;
import com.david.entity.UserDAO;
import com.david.entity.UserEntity;
import com.david.rest.resource.DeleteLocationResponse;
import com.david.rest.resource.UpdateLocationRequest;
import com.david.rest.resource.UpdateLocationResponse;
import com.david.util.CookieAccess;

@RestController
@RequestMapping(value = "/REST")
public class LocationControllerRest {
	
	@RequestMapping(value = "/location/{id}/delete", method = RequestMethod.DELETE)
	public @ResponseBody DeleteLocationResponse delete(@PathVariable("id") int id) {
		
		// query
		LocationDAO locationDAO = new LocationDAO();
		boolean success = locationDAO.deleteById(id);
		locationDAO.closeSession();
		
		if (success) {
			return new DeleteLocationResponse(1, "Location delete successful");
		} else {
			return new DeleteLocationResponse(-1, "Location delete was not successful");
		}
	}
	
	@RequestMapping(value = "/location/{id}/update", method = RequestMethod.PUT, consumes = {"application/json"}, produces = "application/json")
	public @ResponseBody UpdateLocationResponse update(@RequestBody UpdateLocationRequest location) {
		
		// get JSON request values
		int id = location.getId();
		boolean is_valid = location.isIs_valid();
		String value = location.getValue();
		
		// get user info from cookie
		String username = CookieAccess.getUsername();
		String role = CookieAccess.getRole();

		// get user from username
		UserDAO userDAO = new UserDAO();
		UserEntity userEntity = userDAO.findByUsername(username);
		userDAO.closeSession();
		
		// update location
		LocationDAO locationDAO = new LocationDAO();
		boolean success = locationDAO.updateById(id, is_valid, value, userEntity.getId());
		locationDAO.closeSession();
		
		if (success) {
			return new UpdateLocationResponse(1, "Update location successful", id, is_valid, value);
		} else {
			return new UpdateLocationResponse(-1, "Update location not successful");
		}
	}
	
}
