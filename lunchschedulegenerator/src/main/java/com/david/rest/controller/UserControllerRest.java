package com.david.rest.controller;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.david.entity.UserDAO;
import com.david.entity.UserEntity;
import com.david.entity.UserRoleDAO;
import com.david.entity.UserRoleEntity;
import com.david.rest.resource.CreateNewUserRequest;
import com.david.rest.resource.CreateNewUserResponse;
import com.david.rest.resource.UserCheckRequest;
import com.david.rest.resource.UserCheckResponse;

@RestController
@RequestMapping(value = "/REST/user")
public class UserControllerRest {
	
	// logger
	private static final Logger logger = Logger.getLogger(UserControllerRest.class);
	
	@RequestMapping(value = "/user_check", method = RequestMethod.POST, consumes = {"application/json"}, produces = "application/json")
	public @ResponseBody UserCheckResponse userCheck(@RequestBody UserCheckRequest user) {
		
		String username = user.getUsername();

		try {
			
			// query
			UserDAO userDAO = new UserDAO();
			UserEntity userEntity = userDAO.findByUsername(username);
			userDAO.closeSession();
			
			if (userEntity != null) {
				return new UserCheckResponse(1, "User already exists");
			} else {
				return new UserCheckResponse(1, "User does not exist");
			}
			
		} catch (Exception e) {
			
			// if there is an issue
			return new UserCheckResponse(-1, "Error: Was not able to complete user check");
		}
		
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.POST, consumes = {"application/json"}, produces = "application/json")
	public @ResponseBody CreateNewUserResponse create(@RequestBody CreateNewUserRequest user) {
		
		// get JSON data
		String username = user.getUsername();
		String password = user.getPassword();
		
		try {
			
			// query UserRole
			UserRoleDAO userRoleDAO = new UserRoleDAO();
			UserRoleEntity userRole = userRoleDAO.findByRole("ROLE_USER");
			userRoleDAO.closeSession();
			
			// create User
			UserDAO userDAO = new UserDAO();
			boolean success = userDAO.createNewUser(username, password, userRole, true);
			userDAO.closeSession();
			
			if (success) {
				
				// return JSON
				return new CreateNewUserResponse(1, "New user created successfully");
				
			} else {
				
				// issue
				return new CreateNewUserResponse(-1, "Error: Was not able to complete user creation");
			}

		} catch (Exception e) {
			
			// if there is an issue
			return new CreateNewUserResponse(-1, "Error: Was not able to complete user creation");
		}
		
	}
}
