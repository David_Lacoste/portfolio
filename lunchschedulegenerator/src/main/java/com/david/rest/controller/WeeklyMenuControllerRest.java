package com.david.rest.controller;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.david.email.SendMail;
import com.david.rest.resource.EmailDemoScheduleRequest;
import com.david.rest.resource.EmailDemoScheduleResponse;

@RestController
@RequestMapping(value = "/REST")
public class WeeklyMenuControllerRest {
	
	@RequestMapping(value = "/schedule/emaildemo", method = RequestMethod.POST, consumes = {"application/json"}, produces = "application/json")
	public @ResponseBody EmailDemoScheduleResponse emailDemoSchedule(@RequestBody EmailDemoScheduleRequest emailDemo) {
		
		String recipient = emailDemo.getRecipient();
		
		SendMail sender = new SendMail();
		boolean success = sender.sendMail(recipient, "Test", "Test", true);
		
		if (success) {
			return new EmailDemoScheduleResponse(1, "Email sent successfully");
		} else {
			return new EmailDemoScheduleResponse(-1, "Email was not sent successfully");
		}
	}
}
