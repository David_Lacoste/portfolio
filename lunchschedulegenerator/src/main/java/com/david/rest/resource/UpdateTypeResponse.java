package com.david.rest.resource;

public class UpdateTypeResponse extends AbstractResponse {
	
	private int id;
	private int category;
	private boolean is_valid;
	private String value;
	
	public UpdateTypeResponse(long code, String message) {
		super(code, message);
	}

	public UpdateTypeResponse(long code, 
			String message, 
			int id, 
			int category, 
			boolean is_valid, 
			String value) {
		
		super(code, message);
		
		this.id = id;
		this.category = category;
		this.is_valid = is_valid;
		this.value = value;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCategory() {
		return category;
	}

	public void setCategory(int category) {
		this.category = category;
	}

	public boolean isIs_valid() {
		return is_valid;
	}

	public void setIs_valid(boolean is_valid) {
		this.is_valid = is_valid;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
