package com.david.rest.resource;

public abstract class AbstractResponse {
	
	private final long code;
	private final String message;
	
	public AbstractResponse(long code, String message) {
		this.code = code;
		this.message = message;
	}
	
	public long getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}
}
