/*
 * Example JSON
 * 
 * {
 * 		"code": 1,
 * 		"message": "User already exists"
 * }
 * 
 */
package com.david.rest.resource;

public class UserCheckResponse extends AbstractResponse {
	
	public UserCheckResponse(long code, String message) {
		super(code, message);
	}
	
}
