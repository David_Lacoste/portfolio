package com.david.rest.resource;

public class DeleteLocationResponse extends AbstractResponse {
	
	public DeleteLocationResponse(long code, String message) {
		super(code, message);
	}
}
