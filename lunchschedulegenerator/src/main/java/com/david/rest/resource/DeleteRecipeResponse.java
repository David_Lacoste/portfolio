package com.david.rest.resource;

public class DeleteRecipeResponse extends AbstractResponse {
	
	public DeleteRecipeResponse(long code, String message) {
		super(code, message);
	}
	
}
