package com.david.rest.resource;

public class UpdateLocationResponse extends AbstractResponse {
	
	private int id;
	private boolean is_valid;
	private String value;
	
	public UpdateLocationResponse(long code, String message) {
		super(code, message);
	}

	public UpdateLocationResponse(long code, 
			String message, 
			int id, 
			boolean is_valid, 
			String value) {
		
		super(code, message);
		
		this.id = id;
		this.is_valid = is_valid;
		this.value = value;
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isIs_valid() {
		return is_valid;
	}

	public void setIs_valid(boolean is_valid) {
		this.is_valid = is_valid;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
