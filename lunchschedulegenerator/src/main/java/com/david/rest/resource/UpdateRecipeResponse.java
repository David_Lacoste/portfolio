package com.david.rest.resource;

public class UpdateRecipeResponse extends AbstractResponse {
	
	private int id;
	private String name;
	private int type;
	private int location;
	private int season;
	private int meal;
	private boolean is_valid;
	
	public UpdateRecipeResponse(long code, String message) {
		super(code, message);
	}

	public UpdateRecipeResponse(long code, 
			String message, 
			int id, 
			String name, 
			int type, 
			int location, 
			int season, 
			int meal, 
			boolean is_valid) {
		
		super(code, message);
		
		this.id = id;
		this.name = name;
		this.type = type;
		this.location = location;
		this.season = season;
		this.meal = meal;
		this.is_valid = is_valid;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getLocation() {
		return location;
	}

	public void setLocation(int location) {
		this.location = location;
	}

	public int getSeason() {
		return season;
	}

	public void setSeason(int season) {
		this.season = season;
	}

	public int getMeal() {
		return meal;
	}

	public void setMeal(int meal) {
		this.meal = meal;
	}

	public boolean isIs_valid() {
		return is_valid;
	}

	public void setIs_valid(boolean is_valid) {
		this.is_valid = is_valid;
	}
	
}
