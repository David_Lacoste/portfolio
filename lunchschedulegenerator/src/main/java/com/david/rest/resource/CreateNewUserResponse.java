package com.david.rest.resource;

public class CreateNewUserResponse extends AbstractResponse {
	
	public CreateNewUserResponse(long code, String message) {
		super(code, message);
	}
}
