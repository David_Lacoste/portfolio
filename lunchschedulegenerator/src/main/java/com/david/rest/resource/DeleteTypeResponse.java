package com.david.rest.resource;

public class DeleteTypeResponse extends AbstractResponse {
	
	public DeleteTypeResponse(long code, String message) {
		super(code, message);
	}
	
}
