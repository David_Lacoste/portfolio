package com.david.rest.resource;

public class EmailDemoScheduleRequest {
	
	private String recipient;
	
	public EmailDemoScheduleRequest() {}

	public String getRecipient() {
		return recipient;
	}

	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}
}
