package com.david.rest.resource;

public class EmailDemoScheduleResponse extends AbstractResponse {
	
	public EmailDemoScheduleResponse(long code, String message) {
		super(code, message);
	}
}
