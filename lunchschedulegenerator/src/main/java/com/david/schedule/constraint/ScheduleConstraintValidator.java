package com.david.schedule.constraint;

import java.util.ArrayList;

import com.david.schedule.ScheduleModel;

public class ScheduleConstraintValidator {
	
	private ArrayList<AbstractScheduleConstraint> scheduleConstraints = new ArrayList<AbstractScheduleConstraint>();
	private ScheduleModel schedule;
	
	public ScheduleConstraintValidator(ScheduleModel schedule) {
		this.schedule = schedule;
	}
	
	public void addConstraint(AbstractScheduleConstraint constraint) {
		this.scheduleConstraints.add(constraint);
	}
	
	public boolean validateConstraints() {
		
		boolean result = true;
		
		for (int i = 0; i < this.scheduleConstraints.size(); i++ ) {
			result = result && this.scheduleConstraints.get(i).evaluate(this.schedule);
		}
		
		return result;
	}
}
