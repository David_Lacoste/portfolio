package com.david.schedule.constraint;

import com.david.schedule.ScheduleModel;

public class PastaTwiceConstraint extends AbstractScheduleConstraint {
	
	PastaTwiceConstraint() {
		this.setTitle("Pasta Twice Constraint");
		this.setExplanation("Return true if the schedule contains two meals in the 'Pates' category between Monday-Thursday");
	}

	@Override
	public boolean evaluate(ScheduleModel schedule) {
		return false;
	}

}
