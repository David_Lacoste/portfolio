package com.david.schedule.constraint;

import com.david.schedule.ScheduleModel;

public abstract class AbstractScheduleConstraint {
	
	private String title;
	private String explanation;
	
	public AbstractScheduleConstraint() {}
	
	public AbstractScheduleConstraint(String title, String explanation) {
		this.title = title;
		this.explanation = explanation;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getExplanation() {
		return explanation;
	}

	public void setExplanation(String explanation) {
		this.explanation = explanation;
	}
	
	/*
	 * Given a schedule, return true if the schedule satisfies the constraint. If not, return false.
	 */
	public abstract boolean evaluate(ScheduleModel schedule);
}
