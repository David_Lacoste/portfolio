/*
 * A model for easy access to the String 
 */
package com.david.schedule;

import java.util.Date;

import com.david.entity.WeekMenuEntity;

public class ScheduleModel {
	
	private WeekMenuEntity weekMenu;
	private Date day1Date;
	private String day1Info;
	private String day1Diner;
	private String day1Souper;
	private Date day2Date;
	private String day2Info;
	private String day2Diner;
	private String day2Souper;
	private Date day3Date;
	private String day3Info;
	private String day3Diner;
	private String day3Souper;
	private Date day4Date;
	private String day4Info;
	private String day4Diner;
	private String day4Souper;
	private Date day5Date;
	private String day5Info;
	private String day5Diner;
	private String day5Souper;
	private Date day6Date;
	private String day6Info;
	private String day6Diner;
	private String day6Souper;
	private Date day7Date;
	private String day7Info;
	private String day7Diner;
	private String day7Souper;
	
	public ScheduleModel() {}
	
	public ScheduleModel(WeekMenuEntity weekMenu) {
		this.setWeekMenu(weekMenu);
	}

	public WeekMenuEntity getWeekMenu() {
		return weekMenu;
	}

	public void setWeekMenu(WeekMenuEntity weekMenu) {
		this.weekMenu = weekMenu;
	}

	public Date getDay1Date() {
		return day1Date;
	}

	public void setDay1Date(Date day1Date) {
		this.day1Date = day1Date;
	}

	public String getDay1Info() {
		return day1Info;
	}

	public void setDay1Info(String day1Info) {
		this.day1Info = day1Info;
	}

	public String getDay1Diner() {
		return day1Diner;
	}

	public void setDay1Diner(String day1Diner) {
		this.day1Diner = day1Diner;
	}

	public String getDay1Souper() {
		return day1Souper;
	}

	public void setDay1Souper(String day1Souper) {
		this.day1Souper = day1Souper;
	}

	public Date getDay2Date() {
		return day2Date;
	}

	public void setDay2Date(Date day2Date) {
		this.day2Date = day2Date;
	}

	public String getDay2Info() {
		return day2Info;
	}

	public void setDay2Info(String day2Info) {
		this.day2Info = day2Info;
	}

	public String getDay2Diner() {
		return day2Diner;
	}

	public void setDay2Diner(String day2Diner) {
		this.day2Diner = day2Diner;
	}

	public String getDay2Souper() {
		return day2Souper;
	}

	public void setDay2Souper(String day2Souper) {
		this.day2Souper = day2Souper;
	}

	public Date getDay3Date() {
		return day3Date;
	}

	public void setDay3Date(Date day3Date) {
		this.day3Date = day3Date;
	}

	public String getDay3Info() {
		return day3Info;
	}

	public void setDay3Info(String day3Info) {
		this.day3Info = day3Info;
	}

	public String getDay3Diner() {
		return day3Diner;
	}

	public void setDay3Diner(String day3Diner) {
		this.day3Diner = day3Diner;
	}

	public String getDay3Souper() {
		return day3Souper;
	}

	public void setDay3Souper(String day3Souper) {
		this.day3Souper = day3Souper;
	}

	public Date getDay4Date() {
		return day4Date;
	}

	public void setDay4Date(Date day4Date) {
		this.day4Date = day4Date;
	}

	public String getDay4Info() {
		return day4Info;
	}

	public void setDay4Info(String day4Info) {
		this.day4Info = day4Info;
	}

	public String getDay4Diner() {
		return day4Diner;
	}

	public void setDay4Diner(String day4Diner) {
		this.day4Diner = day4Diner;
	}

	public String getDay4Souper() {
		return day4Souper;
	}

	public void setDay4Souper(String day4Souper) {
		this.day4Souper = day4Souper;
	}

	public Date getDay5Date() {
		return day5Date;
	}

	public void setDay5Date(Date day5Date) {
		this.day5Date = day5Date;
	}

	public String getDay5Info() {
		return day5Info;
	}

	public void setDay5Info(String day5Info) {
		this.day5Info = day5Info;
	}

	public String getDay5Diner() {
		return day5Diner;
	}

	public void setDay5Diner(String day5Diner) {
		this.day5Diner = day5Diner;
	}

	public String getDay5Souper() {
		return day5Souper;
	}

	public void setDay5Souper(String day5Souper) {
		this.day5Souper = day5Souper;
	}

	public Date getDay6Date() {
		return day6Date;
	}

	public void setDay6Date(Date day6Date) {
		this.day6Date = day6Date;
	}

	public String getDay6Info() {
		return day6Info;
	}

	public void setDay6Info(String day6Info) {
		this.day6Info = day6Info;
	}

	public String getDay6Diner() {
		return day6Diner;
	}

	public void setDay6Diner(String day6Diner) {
		this.day6Diner = day6Diner;
	}

	public String getDay6Souper() {
		return day6Souper;
	}

	public void setDay6Souper(String day6Souper) {
		this.day6Souper = day6Souper;
	}

	public Date getDay7Date() {
		return day7Date;
	}

	public void setDay7Date(Date day7Date) {
		this.day7Date = day7Date;
	}

	public String getDay7Info() {
		return day7Info;
	}

	public void setDay7Info(String day7Info) {
		this.day7Info = day7Info;
	}

	public String getDay7Diner() {
		return day7Diner;
	}

	public void setDay7Diner(String day7Diner) {
		this.day7Diner = day7Diner;
	}

	public String getDay7Souper() {
		return day7Souper;
	}

	public void setDay7Souper(String day7Souper) {
		this.day7Souper = day7Souper;
	}
	
	public void generateDemoSchedule() {
		
		// NOTE: The dayXDate will always be the current date...
		// TODO: Fix the dayXDate
		
		// day 1
		this.day1Date = new Date();
		this.day1Info = "-";
		this.day1Diner = "Hot Dog (Recettes)";
		this.day1Souper = "Sweet BBQ Chicken Kebobs (Recettes)";

		// day 2
		this.day2Date = new Date();
		this.day2Info = "-";
		this.day2Diner = "Kraft Dinner (Recettes)";
		this.day2Souper = "Jambon poêle (Recettes)";

		// day 3
		this.day3Date = new Date();
		this.day3Info = "-";
		this.day3Diner = "-";
		this.day3Souper = "Pâtes pesto (Classic Pasta)";

		// day 4
		this.day4Date = new Date();
		this.day4Info = "-";
		this.day4Diner = "-";
		this.day4Souper = "Family Classic Meatloaf (Recettes)";

		// day 5
		this.day5Date = new Date();
		this.day5Info = "-";
		this.day5Diner = "-";
		this.day5Souper = "Pâtes bolognese (Recettes)";

		// day 6
		this.day6Date = new Date();
		this.day6Info = "-";
		this.day6Diner = "-";
		this.day6Souper = "Shakshouka (Recettes)";

		// day 7
		this.day7Date = new Date();
		this.day7Info = "-";
		this.day7Diner = "-";
		this.day7Souper = "Gaby et Yoyo";
		
	}
	
	public void generateScheduleFromWeekMenu() {
		
		// day 1
		this.day1Date = this.getWeekMenu().getDay1().getDay();
		this.day1Info = this.getWeekMenu().getDay1().getInfo();
		this.day1Diner = this.getWeekMenu().getDay1().getDiner().getName() + " (" + this.getWeekMenu().getDay1().getDiner().getLocation() + ")";
		this.day1Souper = this.getWeekMenu().getDay1().getSouper().getName() + " (" + this.getWeekMenu().getDay1().getSouper().getLocation() + ")";
		
		// day 2
		this.day2Date = this.getWeekMenu().getDay2().getDay();
		this.day2Info = this.getWeekMenu().getDay2().getInfo();
		this.day2Diner = this.getWeekMenu().getDay2().getDiner().getName() + " (" + this.getWeekMenu().getDay2().getDiner().getLocation() + ")";
		this.day2Souper = this.getWeekMenu().getDay2().getSouper().getName() + " (" + this.getWeekMenu().getDay2().getSouper().getLocation() + ")";

		// day 3
		this.day3Date = this.getWeekMenu().getDay3().getDay();
		this.day3Info = this.getWeekMenu().getDay3().getInfo();
		this.day3Diner = this.getWeekMenu().getDay3().getDiner().getName() + " (" + this.getWeekMenu().getDay3().getDiner().getLocation() + ")";
		this.day3Souper = this.getWeekMenu().getDay3().getSouper().getName() + " (" + this.getWeekMenu().getDay3().getSouper().getLocation() + ")";

		// day 4
		this.day4Date = this.getWeekMenu().getDay4().getDay();
		this.day4Info = this.getWeekMenu().getDay4().getInfo();
		this.day4Diner = this.getWeekMenu().getDay4().getDiner().getName() + " (" + this.getWeekMenu().getDay4().getDiner().getLocation() + ")";
		this.day4Souper = this.getWeekMenu().getDay4().getSouper().getName() + " (" + this.getWeekMenu().getDay4().getSouper().getLocation() + ")";

		// day 5
		this.day5Date = this.getWeekMenu().getDay5().getDay();
		this.day5Info = this.getWeekMenu().getDay5().getInfo();
		this.day5Diner = this.getWeekMenu().getDay5().getDiner().getName() + " (" + this.getWeekMenu().getDay5().getDiner().getLocation() + ")";
		this.day5Souper = this.getWeekMenu().getDay5().getSouper().getName() + " (" + this.getWeekMenu().getDay5().getSouper().getLocation() + ")";

		// day 6
		this.day6Date = this.getWeekMenu().getDay6().getDay();
		this.day6Info = this.getWeekMenu().getDay6().getInfo();
		this.day6Diner = this.getWeekMenu().getDay6().getDiner().getName() + " (" + this.getWeekMenu().getDay6().getDiner().getLocation() + ")";
		this.day6Souper = this.getWeekMenu().getDay6().getSouper().getName() + " (" + this.getWeekMenu().getDay6().getSouper().getLocation() + ")";

		// day 7
		this.day7Date = this.getWeekMenu().getDay7().getDay();
		this.day7Info = this.getWeekMenu().getDay7().getInfo();
		this.day7Diner = this.getWeekMenu().getDay7().getDiner().getName() + " (" + this.getWeekMenu().getDay7().getDiner().getLocation() + ")";
		this.day7Souper = this.getWeekMenu().getDay7().getSouper().getName() + " (" + this.getWeekMenu().getDay7().getSouper().getLocation() + ")";
	}
	
}
