package com.david.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.david.util.CookieAccess;

@Controller
public class WeeklyMenuController {
	
	@RequestMapping(value = "/weeklymenu")
	public ModelAndView getWeeklyMenu() {
		
		// get user info from cookie
		String username = CookieAccess.getUsername();
		
		// get welcome.jsp
		ModelAndView model = new ModelAndView("weeklyMenu");
		model.addObject("username", username);

		return model;
	}
}
