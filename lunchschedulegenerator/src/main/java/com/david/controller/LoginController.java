package com.david.controller;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.david.model.UserModel;
import com.david.validation.LoginFormValidator;

@Controller
@RequestMapping(value = "/login")
public class LoginController {
	
	// logger
	private static final Logger logger = Logger.getLogger(LoginController.class);
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(new LoginFormValidator());
	}
	
	@RequestMapping(value = "", method = RequestMethod.GET)
	public ModelAndView getLogin() throws ClassNotFoundException {
		
		// get login.jsp
		ModelAndView model = new ModelAndView("login");
		
		return model;
	}
	
	@RequestMapping(value = "/process", method = RequestMethod.POST)
	public ModelAndView processLogin(@Validated @ModelAttribute("user") UserModel user, BindingResult result) {
		
		if (result.hasErrors()) {
			ModelAndView model = new ModelAndView("login");
			return model;
		}
		
		// run user authentication here
		
		// if user passes authentication
		ModelAndView model = new ModelAndView("welcome");
		return model;
		
	}
	
	@RequestMapping(value = "/error", method = RequestMethod.GET)
	public ModelAndView error() {
		
		ModelAndView model = new ModelAndView("login");
		model.addObject("error", "Invalid username and password");
		return model;
		
	}
	
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public ModelAndView logout(HttpServletRequest req, HttpServletResponse resp) {
		
		// remove the currently logged in cookie
		Cookie[] cookies = req.getCookies();
		
		if (cookies != null) {
			for (int i = 0; i < cookies.length; i++) {
				
				if (cookies[i].getName().equalsIgnoreCase("LSG-username") || 
						cookies[i].getName().equalsIgnoreCase("LSG-role")) {
					
					// remove the cookie
					
					cookies[i].setValue(null);
					cookies[i].setMaxAge(0);
					resp.addCookie(cookies[i]);
					
				}
			}
		}
		
		ModelAndView model = new ModelAndView("login");
		model.addObject("logout", "You've been logged out successfully");
		return model;
		
	}
	
}
