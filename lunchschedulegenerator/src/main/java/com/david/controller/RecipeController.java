package com.david.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.david.entity.LocationDAO;
import com.david.entity.LocationEntity;
import com.david.entity.MealDAO;
import com.david.entity.MealEntity;
import com.david.entity.RecipeDAO;
import com.david.entity.RecipeEntity;
import com.david.entity.SeasonDAO;
import com.david.entity.SeasonEntity;
import com.david.entity.TypeDAO;
import com.david.entity.TypeEntity;
import com.david.entity.UserDAO;
import com.david.entity.UserEntity;
import com.david.model.RecipeModel;
import com.david.util.CookieAccess;

@Controller
public class RecipeController {
	
	/*
	 * Redirect to show all recipes
	 * 
	 * GET /recipe/
	 */
	@RequestMapping(value = "/recipe/", method = RequestMethod.GET)
	public String catchAll() {
		return "redirect:/recipe/list";
	}
	
	/*
	 * Show all recipes
	 * 
	 * GET /recipe/list
	 */
	@RequestMapping(value="/recipe/list", method = RequestMethod.GET)
	public ModelAndView showAllRecipes() {
		
		// get user info from cookie
		String username = CookieAccess.getUsername();

		// get user from username
		UserDAO userDAO = new UserDAO();
		UserEntity userEntity = userDAO.findByUsername(username);
		userDAO.closeSession();
		
		// query
		RecipeDAO recipeDAO = new RecipeDAO();
		List<RecipeEntity> recipes = recipeDAO.findAllByUser(userEntity.getId());
		recipeDAO.closeSession();
		
		// show recipeList.jsp
		ModelAndView model = new ModelAndView("recipeList");
		
		// provide the arrayList to the view
		model.addObject("recipes", recipes);
		
		return model;
	}
	
	/*
	 * View recipe
	 * 
	 * GET /recipe/{id}
	 */
	@RequestMapping(value = "/recipe/{id}", method = RequestMethod.GET)
	public ModelAndView display(@PathVariable("id") int id) {
		
		// get user info from cookie
		String username = CookieAccess.getUsername();

		// get user from username
		UserDAO userDAO = new UserDAO();
		UserEntity userEntity = userDAO.findByUsername(username);
		userDAO.closeSession();
		
		// queries
		RecipeDAO recipeDAO = new RecipeDAO();
		RecipeEntity recipe = recipeDAO.findById(id, userEntity.getId());
		recipeDAO.closeSession();
		
		if (recipe != null) {
			
			// recipe exists
			
			TypeDAO typeDAO = new TypeDAO();
			List<TypeEntity> types = typeDAO.findAll();
			typeDAO.closeSession();
			
			LocationDAO locationDAO = new LocationDAO();
			List<LocationEntity> locations = locationDAO.findAll();
			locationDAO.closeSession();
			
			SeasonDAO seasonDAO = new SeasonDAO();
			List<SeasonEntity> seasons = seasonDAO.findAll();
			seasonDAO.closeSession();
			
			MealDAO mealDAO = new MealDAO();
			List<MealEntity> meals = mealDAO.findAll();
			mealDAO.closeSession();
			
			// show recipeShow.jsp
			ModelAndView model = new ModelAndView("recipeShow");
			
			// provide the objects to the view
			model.addObject("recipe", recipe);
			model.addObject("types", types);
			model.addObject("locations", locations);
			model.addObject("seasons", seasons);
			model.addObject("meals", meals);
			
			return model;
			
		} else {
			
			// this page does not exist
			ModelAndView model = new ModelAndView("error404");
			return model;
		}
	}
	
	@RequestMapping(value="/recipe/add", method = RequestMethod.GET)
	public ModelAndView recipeAdd() {
		
		// get types
		TypeDAO typeDAO = new TypeDAO();
		List<TypeEntity> types = typeDAO.findAll();
		typeDAO.closeSession();
		
		// get locations
		LocationDAO locationDAO = new LocationDAO();
		List<LocationEntity> locations = locationDAO.findAll();
		locationDAO.closeSession();
		
		// get seasons
		SeasonDAO seasonDAO = new SeasonDAO();
		List<SeasonEntity> seasons = seasonDAO.findAll();
		seasonDAO.closeSession();
		
		// get meals
		MealDAO mealDAO = new MealDAO();
		List<MealEntity> meals = mealDAO.findAll();
		mealDAO.closeSession();
		
		ModelAndView model = new ModelAndView("recipeAdd");
		
		// provide null error
		String error = null;
		model.addObject("error", error);
		
		// add objects to model
		model.addObject("types", types);
		model.addObject("locations", locations);
		model.addObject("seasons", seasons);
		model.addObject("meals", meals);
		
		return model;
	}
	
	@RequestMapping(value = "/recipe", method = RequestMethod.POST)
	public ModelAndView create(@Validated @ModelAttribute("recipe") RecipeModel recipe, BindingResult result) {
		
		if (result.hasErrors()) {
			
			// get the model from running recipeAdd()
			ModelAndView model = this.recipeAdd();
			String error = "Recipe creation not successful";
			model.addObject("error", error);
			return model;
			
		}
		
		// get user info from cookie
		String username = CookieAccess.getUsername();
		String role = CookieAccess.getRole();
		
		// get user from username
		UserDAO userDAO = new UserDAO();
		UserEntity userEntity = userDAO.findByUsername(username);
		userDAO.closeSession();
		
		// create object
		RecipeDAO recipeDAO = new RecipeDAO();
		boolean success = recipeDAO.createNewRecipe(recipe.getName(), recipe.getType(), recipe.getLocation(), recipe.getSeason(), recipe.getMeal(), true, userEntity.getId());
		recipeDAO.closeSession();
		
		if (success) {
			
			// get the model from running showAllRecipes()
			ModelAndView model = this.showAllRecipes();
			String message = "Recipe creation success";
			model.addObject("message", message);
			return model;
			
		} else {
			
			// get the model from running recipeAdd()
			ModelAndView model = this.recipeAdd();
			String error = "Recipe creation not successful";
			model.addObject("error", error);
			return model;
			
		}
	}
}
