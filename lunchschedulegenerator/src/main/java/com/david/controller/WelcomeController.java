package com.david.controller;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.david.util.CookieAccess;

@Controller
public class WelcomeController {
	
	@RequestMapping(value = "/welcome")
	public ModelAndView getWelcome(HttpServletResponse response) {
		
		// get spring session cookie info
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String name = auth.getName();
		
		// get user authority
		String role = "";
		for (GrantedAuthority authority : auth.getAuthorities()) {
			if (authority.getAuthority().equals("ROLE_ADMIN")) {
				role = "ROLE_ADMIN";
			} else if (authority.getAuthority().equals("ROLE_USER")) {
				role = "ROLE_USER";
			}
		}
		
		// create cookie
		response.addCookie(new Cookie("LSG-username", name));
		response.addCookie(new Cookie("LSG-role", role));
		
		// create CookieAccess object
		final CookieAccess cookieAccess = new CookieAccess(name, role);
		
		// get welcome.jsp
		ModelAndView model = new ModelAndView("welcome");
		
		// add the model objects
		model.addObject("user", name);
		model.addObject("role", role);
		
		// send test email
		/*
		SendMail sender = new SendMail();
		sender.sendMail("davidlacoste3@gmail.com", "test email", "Hello, this is a test email");
		*/
		
		return model;
	}
}
