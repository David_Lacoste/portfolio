package com.david.controller;

import java.io.IOException;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class WakeDynoController {
	
	@RequestMapping(value = "/wakemydyno.txt", method = RequestMethod.GET)
	public void export(HttpServletResponse response) throws IOException {
		
		response.setContentType("text/plain");
		response.setHeader("Content-Disposition", "attachment;filename=wakemydyno.txt");
		ServletOutputStream out = response.getOutputStream();
		out.flush();
		out.close();
	}
}
