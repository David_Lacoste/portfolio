package com.david.controller;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.david.entity.UserDAO;
import com.david.entity.UserEntity;
import com.david.model.UserModel;

@Controller
public class UserController {
	
	@RequestMapping(value = "/user", method = RequestMethod.POST)
	public ModelAndView create(@Validated @ModelAttribute("user") UserModel user, BindingResult result) {
		
		if (result.hasErrors()) {
			ModelAndView model = new ModelAndView("login");
			return model;
		}
		
		ModelAndView model = new ModelAndView("welcome");
		return model;
		
	}
}
