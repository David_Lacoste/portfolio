package com.david.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.david.entity.CategoryDAO;
import com.david.entity.CategoryEntity;
import com.david.entity.TypeDAO;
import com.david.entity.TypeEntity;
import com.david.entity.UserDAO;
import com.david.entity.UserEntity;
import com.david.model.TypeModel;
import com.david.util.CookieAccess;

@Controller
public class TypeController {
	
	@RequestMapping(value = "/type/", method = RequestMethod.GET)
	public String catchAll() {
		return "redirect:/type/list";
	}
	
	@RequestMapping(value="/type/list", method = RequestMethod.GET)
	public ModelAndView showAllTypes() {
		
		// get user info from cookie
		String username = CookieAccess.getUsername();
		String role = CookieAccess.getRole();

		// get user from username
		UserDAO userDAO = new UserDAO();
		UserEntity userEntity = userDAO.findByUsername(username);
		userDAO.closeSession();
		
		// query
		TypeDAO typeDAO = new TypeDAO();
		List<TypeEntity> types = typeDAO.findAllByUser(userEntity.getId());
		typeDAO.closeSession();
		
		// show typeList.jsp
		ModelAndView model = new ModelAndView("typeList");
		
		// provide null message
		String message = null;
		model.addObject("message", message);
		
		// provide the arrayList to the view
		model.addObject("types", types);
		model.addObject("role", role);
		
		return model;
	}
	
	/*
	 * View type
	 * 
	 * GET /type/{id}
	 */
	@RequestMapping(value = "/type/{id}", method = RequestMethod.GET)
	public ModelAndView display(@PathVariable("id") int id) {
		
		// get user info from cookie
		String username = CookieAccess.getUsername();
		String role = CookieAccess.getRole();

		// get user from username
		UserDAO userDAO = new UserDAO();
		UserEntity userEntity = userDAO.findByUsername(username);
		userDAO.closeSession();
		
		// queries
		TypeDAO typeDAO = new TypeDAO();
		TypeEntity type = typeDAO.findById(id, userEntity.getId());
		typeDAO.closeSession();
		
		if (type != null) {
			
			if (!type.isUpdate() && !role.equalsIgnoreCase("ROLE_ADMIN")) {
				// if the user is not allowed to modify and is not an admin
				
				// this page does not exist
				ModelAndView model = new ModelAndView("error404");
				return model;
			}
			
			CategoryDAO categoryDAO = new CategoryDAO();
			List<CategoryEntity> categories = categoryDAO.findAll();
			categoryDAO.closeSession();
			
			// show typeShow.jsp
			ModelAndView model = new ModelAndView("typeShow");
			
			model.addObject("type", type);
			model.addObject("categories", categories);
			
			return model;
			
		} else {
			
			// this page does not exist
			ModelAndView model = new ModelAndView("error404");
			return model;
			
		}
	}
	
	@RequestMapping(value="/type/add", method = RequestMethod.GET)
	public ModelAndView typeAdd() {
		
		// get categories
		CategoryDAO categoryDAO = new CategoryDAO();
		List<CategoryEntity> categories = categoryDAO.findAll();
		categoryDAO.closeSession();
		
		ModelAndView model = new ModelAndView("typeAdd");
		
		// provide null error
		String error = null;
		model.addObject("error", error);
		
		model.addObject("categories", categories);
		
		return model;
	}
	
	@RequestMapping(value = "/type", method = RequestMethod.POST)
	public ModelAndView create(@Validated @ModelAttribute("type") TypeModel type, BindingResult result) {
		
		if (result.hasErrors()) {
			
			// get the model from running typeAdd() method
			ModelAndView model = this.typeAdd();
			String error = "Type creation not successful";
			model.addObject("error", error);
			return model;
		}
		
		// get user info from cookie
		String username = CookieAccess.getUsername();
		String role = CookieAccess.getRole();

		// get user from username
		UserDAO userDAO = new UserDAO();
		UserEntity userEntity = userDAO.findByUsername(username);
		userDAO.closeSession();
		
		// create object
		TypeDAO typeDAO = new TypeDAO();
		boolean success = typeDAO.createNewType(type.getValue(), type.getCategory(), true, userEntity.getId());
		typeDAO.closeSession();
		
		if (success) {
			
			// get the model from running showAllTypes()
			ModelAndView model = this.showAllTypes();
			String message = "Type creation success";
			model.addObject("message", message);
			model.addObject("role", role);
			return model;
			
		} else {
			
			// get the model from running typeAdd()
			ModelAndView model = this.typeAdd();
			String error = "Type creation not successful";
			model.addObject("error", error);
			return model;
			
		}
	}
}
