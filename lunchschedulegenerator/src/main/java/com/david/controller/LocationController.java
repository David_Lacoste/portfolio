package com.david.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.david.entity.LocationDAO;
import com.david.entity.LocationEntity;
import com.david.entity.UserDAO;
import com.david.entity.UserEntity;
import com.david.model.LocationModel;
import com.david.util.CookieAccess;

@Controller
public class LocationController {
	
	@RequestMapping(value = "/location/", method = RequestMethod.GET)
	public String catchAll() {
		return "redirect:/location/list";
	}

	@RequestMapping(value="/location/list", method = RequestMethod.GET)
	public ModelAndView showAllLocations() {
		
		// get user info from cookie
		String username = CookieAccess.getUsername();
		String role = CookieAccess.getRole();

		// get user from username
		UserDAO userDAO = new UserDAO();
		UserEntity userEntity = userDAO.findByUsername(username);
		userDAO.closeSession();
		
		// query
		LocationDAO locationDAO = new LocationDAO();
		List<LocationEntity> locations = locationDAO.findAllByUser(userEntity.getId());
		locationDAO.closeSession();

		// show locationList.jsp
		ModelAndView model = new ModelAndView("locationList");
		
		// provide null message
		String message = null;
		model.addObject("message", message);

		// provide the arrayList to the view
		model.addObject("locations", locations);
		model.addObject("role", role);

		return model;
	}
	
	/*
	 * View location
	 * 
	 * GET /location/{id}
	 */
	@RequestMapping(value = "/location/{id}", method = RequestMethod.GET)
	public ModelAndView display(@PathVariable("id") int id) {
		
		// get user info from cookie
		String username = CookieAccess.getUsername();
		String role = CookieAccess.getRole();

		// get user from username
		UserDAO userDAO = new UserDAO();
		UserEntity userEntity = userDAO.findByUsername(username);
		userDAO.closeSession();
		
		// queries
		LocationDAO locationDAO = new LocationDAO();
		LocationEntity location = locationDAO.findById(id, userEntity.getId());
		locationDAO.closeSession();
		
		if (location != null) {
			
			if (!location.isUpdate() && !role.equalsIgnoreCase("ROLE_ADMIN")) {
				// if the user is not allowed to modify and is not an admin
				
				// this page does not exist
				ModelAndView model = new ModelAndView("error404");
				return model;
			}
			
			ModelAndView model = new ModelAndView("locationShow");
			model.addObject("location", location);
			return model;
			
		} else {
			
			// this page does not exist
			ModelAndView model = new ModelAndView("error404");
			return model;
		}
	}
	
	@RequestMapping(value="/location/add", method = RequestMethod.GET)
	public ModelAndView locationAdd() {
		
		ModelAndView model = new ModelAndView("locationAdd");
		
		// provide null error
		String error = null;
		model.addObject("error", error);
		
		return model;
	}
	
	@RequestMapping(value = "/location", method = RequestMethod.POST)
	public ModelAndView create(@Validated @ModelAttribute("location") LocationModel location, BindingResult result) {
		
		if (result.hasErrors()) {
			
			// get the model from running locationAdd()
			ModelAndView model = this.locationAdd();
			String error = "Location creation not successful";
			model.addObject("error", error);
			return model;
		}
		
		// get user info from cookie
		String username = CookieAccess.getUsername();
		String role = CookieAccess.getRole();

		// get user from username
		UserDAO userDAO = new UserDAO();
		UserEntity userEntity = userDAO.findByUsername(username);
		userDAO.closeSession();
		
		// create object
		LocationDAO locationDAO = new LocationDAO();
		boolean success = locationDAO.createNewLocation(location.getValue(), true, userEntity.getId());
		locationDAO.closeSession();
		
		if (success) {
			
			// get the model from running showAllLocations()
			ModelAndView model = this.showAllLocations();
			String message = "Location creation success";
			model.addObject("message", message);
			model.addObject("role", role);
			return model;
			
		} else {
			
			// get the model from running locationAdd()
			ModelAndView model = this.locationAdd();
			String error = "Location creation not successful";
			model.addObject("error", error);
			return model;
			
		}
		
	}
}
