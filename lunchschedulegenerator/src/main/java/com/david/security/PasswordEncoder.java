package com.david.security;

import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class PasswordEncoder {
	
	public String encode(String password) {
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String hashedPassword = passwordEncoder.encode(password);
		
		return hashedPassword;
	}
	
	public boolean compare(String password, String storedHash) {
		
		if (BCrypt.checkpw(password, storedHash)) {
			return true;
		} else {
			return false;
		}
	}
	
	public static void main(String[] args) {
		
		// spring security
		PasswordEncoder encoder = new PasswordEncoder();
		int i = 0;
		boolean result;
		while (i < 10) {
			String password = "123456";
			String hashedPassword = encoder.encode(password);
			
			System.out.println("Hashed password: " + hashedPassword);
			i++;
		}
		
		System.out.println("##############################");
		
		// check whether the passwords match
		String test1 = "1234";
		String test2 = "123";
		String test3 = "123456";
		String[] tests = {test1, test2, test3};
		
		for (i = 0; i < tests.length; i++) {
			
			// the hash is for "123456"
			result = encoder.compare(tests[i], "$2a$10$Cc6vgWYuob3lotWFxnq12ewzQhTN9jbrh9Eoa3ueXVtiz9SWJopoe");
			System.out.println("Test: " + tests[i] + ", Matches: " + String.valueOf(result));
		}
		
		
	}
}
