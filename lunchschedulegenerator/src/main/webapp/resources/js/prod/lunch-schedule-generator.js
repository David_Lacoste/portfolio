jQuery(document).ready(function() {
	
	var LSGModule = function() {
		
		// PREPROCESSING
		
						
		var TEST_URL = "http://localhost:8080/REST";
		var PROD_URL = "http://lunchschedulerweb.herokuapp.com/REST";
		
				
		var URL = PROD_URL;
		
				
		// END PREPROCESSING
		
		var userExists = -1;
		var userCreateSuccess = -1;
		var recipeDeleteId = -1;
		var recipeDeleteSuccess = -1;
		var locationDeleteId = -1;
		var locationDeleteSuccess = -1;
		var typeDeleteId = -1;
		var typeDeleteSuccess = -1;
		
		var CurrentLocation = -1;
		var locationUpdateSuccess = -1;
		var CurrentType = -1;
		var typeUpdateSuccess = -1;
		var CurrentRecipe = -1;
		var recipeUpdateSuccess = -1;
		
		var CurrentUser = -1;
		var emailDemoScheduleSuccess = -1;
		
		
		// ------------------------------------------------------
		// MODELS
		// ------------------------------------------------------
		
		/*
		 * A model containing Recipe data
		 */
		var RecipeModel = function(id, name, type, location, season, meal, created_at, updated_at, is_valid) {
			this.id = id;
			this.name = name;
			this.type = type;
			this.location = location;
			this.season = season;
			this.meal = meal;
			this.created_at = created_at;
			this.updated_at = updated_at;
			this.is_valid = is_valid;
		}
		
		/*
		 * A model containing Location data
		 */
		var LocationModel = function(id, value, created_at, updated_at, is_valid) {
			this.id = id;
			this.value = value;
			this.created_at = created_at;
			this.updated_at = updated_at;
			this.is_valid = is_valid;
		}
		
		/*
		 * A model containing Type data
		 */
		var TypeModel = function(id, value, category, created_at, updated_at, is_valid) {
			this.id = id;
			this.value = value;
			this.category = category;
			this.created_at = created_at;
			this.updated_at = updated_at;
			this.is_valid = is_valid;
		}
		
		/*
		 * A model containing current user data
		 */
		var UserModel = function(username, role) {
			this.username = username;
			this.role = role;
		}
		
		// ------------------------------------------------------
		// END MODELS
		// ------------------------------------------------------
		
		/*
		 * Given a string with a cookie name, return the value of the cookie.
		 */
		getCookie = function(cname) {
			var name = cname + "=";
			var ca = document.cookie.split(';');
			
			// loop through cookies
			for (var i = 0; i < ca.length; i++) {
				var c = ca[i];
				
				while (c.charAt(0) == ' ') {
					c = c.substring(1);
				}
				
				if (c.indexOf(name) == 0) {
					return c.substring(name.length, c.length);
				}
			}
			return "";
		}
		
		/*
		 * Return the username of the user that is currently logged in from the cookie.
		 */
		getCurrentUsername = function() {
			
			return getCookie("LSG-username");
		}
		
		/*
		 * Return the role of the user that is currently logged in from the cookie.
		 */
		getCurrentUserRole = function() {
			
			return getCookie("LSG-role");
		}
		
		/*
		 * Replace the #username-anchor element in the navbar with the username of the current user.
		 */
		addUsernameToNavbar = function() {
			
			// get current user
			var username = getCurrentUsername();
			
			// sanitize username by removing quotes
			username = username.substring(1, username.length - 1);
			
			// add to navbar
			$("#username-anchor").replaceWith(username);
			
		}
		
		/*
		 * Given an id for an <input> element, return the text entered by the user
		 * 
		 * Example: getInputFieldContent(test) will return the text where id="test"
		 */
		getInputFieldContent = function(fieldId) {
			
			var key = "#".concat(fieldId);
			return $(key).val();
		}
		
		/*
		 * Return true if empty or only whitespace. If not, return false.
		 */
		isEmptyString = function(str) {
			
			if (!str.trim()) {
				return true;
			}
			
			return false;
		}
		
		/*
		 * Helper function to check whether #id is in the DOM
		 */
		isInDOM = function(id) {
			
			var key = "#".concat(id);
			
			if ($(key).length) {
				return true;
			}
			
			return false;
		}
		
		/*
		 * Given a username String, return true if the user exists. Return false otherwise.
		 */
		verifyUserExistsAJAX = function(username) {
			
			try {
				
				$.ajax({
					type: "POST",
					async: false,
					url: URL + "/user/user_check",
					contentType: "application/json",
					dataType: "json",
					data: JSON.stringify({
						"username": username
					}),
					success: function(data) {
						if (data["code"] == 1) {
							if (data["message"].valueOf() == "User does not exist") {
								
								userExists = false;
								
							} else if (data["message"].valueOf() == "User already exists") {
								
								userExists = true;
								
							}
						}
					},
					error: function(e) {
						console.log("Error event in AJAX");
					}
				});
				
			} catch (err) {
				console.log("Error when making AJAX call");
				console.log(err);
			}

		}
		
		/*
		 * Handler method to confirm whether the provided new username and password are valid
		 */
		verifyValidUsername = function() {
			
			var username = $('#username').val();
			var password = $('#password').val();
			
			// client side validation
			var valid = verifyValidUser(username) && !isEmptyString(password);
			
			// check validation before making DB/REST call
			if (!valid) {
				
				showModal('errorModal');
				
				return false;
			}
			
			// check if username is already taken
			var exists = verifyUserExists(username);
			
			if (exists == true) {
				
				// username is taken
				showModal('errorModal');
				
			} else if (exists == false) {
				
				// username is not taken
				showModal('createUserModal');
				
			} else if (exists == -1) {
				toastr.error("There was an error processing your request with our servers. Please try again later.", "Error");
			} else {
				toastr.error("There was an error processing your request with our servers. Please try again later.", "Error");
			}
			
		}
		
		/*
		 * Client side form validation
		 */
		verifyValidUser = function(username) {
			
			// check that username is not empty
			if (isEmptyString(username)) {
				
				return false;
			}
			
			return true;
		}
		
		/*
		 * Given a username, make AJAX call and return boolean as to whether user exists in the database
		 */
		verifyUserExists = function(username) {
			
			// make the AJAX call
			verifyUserExistsAJAX(username);
			
			// handle result of call
			if (userExists == true) {
				return true;
			} else if (userExists == false) {
				return false;
			} else {
				return -1;
			}
			
			// reset value
			userExists = -1;
		}
		
		/*
		 * AJAX call to create a new user
		 */
		createNewUserAJAX = function(username, password) {
			
			try {
				
				$.ajax({
					type: "POST",
					async: false,
					url: URL + "/user/create",
					contentType: "application/json",
					dataType: "json",
					data: JSON.stringify({
						"username": username,
						"password": password
					}),
					success: function(data) {
						if (data["code"] == 1) {
							userCreateSuccess = true;
						} else if (data["code"] == -1) {
							userCreateSuccess = false;
						} else {
							userCreateSuccess = -1;
						}
					},
					error: function(e) {
						console.log("Error event in AJAX");
					}
				});
				
			} catch (err) {
				console.log("Error when making AJAX call");
				console.log(err);
				toastr.warning("We're sorry for the inconvenience but the server was unable to complete your request.", "Server Response");
			}
			
		}
		
		/*
		 * Handler method to create new user
		 */
		createNewUser = function() {
			
			// get the username and password
			var username = $('#username').val();
			var password = $('#password').val();
			
			// make the AJAX call
			createNewUserAJAX(username, password);
			
			// hide the modal
			hideModal("createUserModal");
			
			if (userCreateSuccess == true) {
				
				// remove any existing error message
				$('.login-error').remove();
				
				// show a toastr success message
				toastr.success("User successfully created", "User Created");
				
			} else if (userCreateSuccess == false) {
				
				// show an error message
				toastr.warning("We're sorry for the inconvenience but the server was unable to complete your request. Please check back later.", "User Create Error");
			}
			
			// reset
			userCreateSuccess = -1;
			
		}
		
		/*
		 * Handler method to delete a recipe
		 */
		deleteRecipe = function(id) {
			
			// log the recipe id
			recipeDeleteId = id;
			
			// show the modal
			showModal('deleteRecipeModal');
			
		}
		
		/*
		 * AJAX call to delete a recipe
		 */
		deleteRecipeAJAX = function(id) {
			
			try {
				
				$.ajax({
					type: "DELETE",
					async: false,
					url: URL + "/recipe/" + id + "/delete",
					success: function(data) {
						if (data["code"] == 1) {
							recipeDeleteSuccess = true;
						} else if (data["code"] == -1) {
							recipeDeleteSuccess = false;
						} else {
							recipeDeleteSuccess = -1;
						}
					},
					error: function(e) {
						console.log("Error event in AJAX");
					}
				});
				
			} catch (err) {
				console.log("Error when making the AJAX call");
				console.log(err);
				toastr.warning("We're sorry for the inconvenience but the server was unable to complete your request.", "Server Response");
			}
			
		}
		
		/*
		 * Handler method to make the AJAX call
		 */
		deleteRecipeConfirm = function() {
			
			// make the AJAX call
			deleteRecipeAJAX(recipeDeleteId);
			
			// hide the modal
			hideModal('deleteRecipeModal');
			
			if (recipeDeleteSuccess == true) {
				
				// remove the row
				removeDatatableRow('deleteButton'.concat(recipeDeleteId), 'recipe-list');
				
				// show a success message
				toastr.success("Recipe successfully deleted", "Recipe deleted");
				
			} else if (recipeDeleteSuccess == false) {
				
				// show an error message
				toastr.warning("We're sorry for the inconvenience but the server was unable to complete your request. Please check back later.", "Recipe Delete Error");
			}
			
			// reset
			recipeDeleteId = -1;
			recipeDeleteSuccess = -1;
		}
		
		/*
		 * Handler method to delete a location
		 */
		deleteLocation = function(id) {
			
			// log the location id
			locationDeleteId = id;
			
			// show the modal
			showModal('deleteLocationModal');
		}
		
		/*
		 * AJAX call to delete a location
		 */
		deleteLocationAJAX = function(id) {
			
			try {
				
				$.ajax({
					type: "DELETE",
					async: false,
					url: URL + "/location/" + id + "/delete",
					success: function(data) {
						if (data["code"] == 1) {
							locationDeleteSuccess = true;
						} else if (data["code"] == -1) {
							locationDeleteSuccess = false;
						} else {
							locationDeleteSuccess = -1;
						}
					},
					error: function(e) {
						console.log("Error event in AJAX");
					}
				});
				
			} catch (err) {
				console.log("Error when making the AJAX call");
				console.log(err);
				toastr.warning("We're sorry for the inconvenience but the server was unable to complete your request.", "Server Response");
			}
		}
		
		/*
		 * Handler method to make the AJAX call
		 */
		deleteLocationConfirm = function() {
			
			// make the AJAX call
			deleteLocationAJAX(locationDeleteId);
			
			// hide the modal
			hideModal('deleteLocationModal');
			
			if (locationDeleteSuccess == true) {
				
				// remove the row
				removeDatatableRow('deleteButton'.concat(locationDeleteId), 'location-list');
				
				// show a success message
				toastr.success("Location successfully deleted", "Location deleted");
			} else if (locationDeleteSuccess == false) {
				
				// show an error message
				toastr.warning("We're sorry for the inconvenience but the server was unable to complete your request. Please check back later.", "Location Delete Error");
				
			}
			
			// reset
			locationDeleteId = -1;
			locationDeleteSuccess = -1;
		}
		
		/*
		 * Handler method to delete a type
		 */
		deleteType = function(id) {
			
			// log the type id
			typeDeleteId = id;
			
			// show the modal
			showModal('deleteTypeModal');
		}
		
		/*
		 * AJAX call to delete a type
		 */
		deleteTypeAJAX = function(id) {
			
			try {
				
				$.ajax({
					type: "DELETE",
					async: false,
					url: URL + "/type/" + id + "/delete",
					success: function(data) {
						if (data["code"] == 1) {
							typeDeleteSuccess = true;
						} else if (data["code"] == -1) {
							typeDeleteSuccess = false;
						} else {
							typeDeleteSuccess = -1;
						}
					},
					error: function(e) {
						console.log("Error event in AJAX");
					}
				});
				
			} catch (err) {
				console.log("Error when making the AJAX call");
				console.log(err);
				toastr.warning("We're sorry for the inconvenience but the server was unable to complete your request.", "Server Response");
			}
			
		}
		
		/*
		 * Handler method to make the AJAX call
		 */
		deleteTypeConfirm = function() {
			
			// make the AJAX call
			deleteTypeAJAX(typeDeleteId);
			
			// hide the modal
			hideModal('deleteTypeModal');
			
			if (typeDeleteSuccess == true) {
				
				// remove the row
				removeDatatableRow('deleteButton'.concat(typeDeleteId), 'type-list');
				
				// show a success message
				toastr.success("Type successfully deleted", "Type deleted");
				
			} else if (typeDeleteSuccess == false) {
				
				// show an error message
				toastr.warning("We're sorry for the inconvenience but the server was unable to complete your request. Please check back later.", "Type Delete Error");
			}
			
			// reset
			typeDeleteId = -1;
			typeDeleteSuccess = -1;
		}
		
		/*
		 * Handler method to update a location
		 */
		updateLocation = function(id) {
			
			// get the user input
			var userValue = $('#locationValueInput').val();
			
			// cast to integer
			// id = parseInt(id);
			
			var now = new Date();
			
			// create a LocationModel of current user data
			CurrentLocation = new LocationModel(id, userValue, now, now, true);
			
			// show the modal
			showModal('updateLocationModal');
		}
		
		/*
		 * AJAX call to update a location
		 */
		updateLocationAJAX = function() {
			
			try {
				
				$.ajax({
					type: "PUT",
					async: false,
					url: URL + "/location/" + CurrentLocation.id + "/update",
					contentType: "application/json",
					data: JSON.stringify({
						"id": CurrentLocation.id,
						"is_valid": CurrentLocation.is_valid,
						"value": CurrentLocation.value
					}),
					success: function(data) {
						if (data["code"] == 1) {
							locationUpdateSuccess = true;
						} else if (data["code"] == -1) {
							locationUpdateSuccess = false;
						} else {
							locationUpdateSuccess = -1;
						}
					},
					error: function(e) {
						console.log("Error event in AJAX");
					}
				});
				
			} catch (err) {
				console.log("Error when making AJAX call");
				console.log(err);
				toastr.warning("We're sorry for the inconvenience but the server was unable to complete your request.", "Server Response");
			}
		}
		
		/*
		 * Handler method to make the AJAX call
		 */
		updateLocationConfirm = function() {
			
			// make the AJAX call
			updateLocationAJAX();
			
			// hide the modal
			hideModal('updateLocationModal');
			
			if (locationUpdateSuccess == true) {
				toastr.success("Location successfully updated", "Location Updated");
			} else {
				toastr.warning("We're sorry for the inconvenience but the server was unable to complete your request. Please check back later.", "Location Update Error");
			}
			
			// reset
			CurrentLocation = -1;
			locationUpdateSuccess = -1;
		}
		
		/*
		 * Handler method to update a type
		 */
		updateType = function(id) {
			
			// get the user input
			var userValue = $('#typeValueInput').val();
			var userCategory = $('#typeCategorySelect').val();
			
			var now = new Date();
			
			// create TypeModel of the current user data
			CurrentType = new TypeModel(id, userValue, userCategory, now, now, true);
			
			// show the modal
			showModal('updateTypeModal');

		}
		
		/*
		 * AJAX call to update a type
		 */
		updateTypeAJAX = function() {
			
			try {
				
				$.ajax({
					type: "PUT",
					async: false,
					url: URL + "/type/" + CurrentType.id + "/update",
					contentType: "application/json",
					dataType: "json",
					data: JSON.stringify({
						"id": CurrentType.id,
						"category": CurrentType.category,
						"is_valid": CurrentType.is_valid,
						"value": CurrentType.value
					}),
					success: function(data) {
						if (data["code"] == 1) {
							typeUpdateSuccess = true;
						} else if (data["code"] == -1) {
							typeUpdateSuccess = false;
						} else {
							typeUpdateSuccess = -1;
						}
					},
					error: function(e) {
						console.log("Error event in AJAX");
					}
				});
				
			} catch (err) {
				console.log("Error when making AJAX call");
				console.log(err);
				toastr.warning("We're sorry for the inconvenience but the server was unable to complete your request.", "Server Response");
			}
		}
		
		/*
		 * Handler method to make the AJAX call
		 */
		updateTypeConfirm = function() {
			
			// make the AJAX call
			updateTypeAJAX();
			
			// hide the modal
			hideModal('updateTypeModal');
			
			if (typeUpdateSuccess == true) {
				
				// update UI
				updateTypeUI();
				
				toastr.success("Type successfully updated", "Type Updated");
			} else {
				toastr.warning("We're sorry for the inconvenience but the server was unable to complete your request. Please check back later.", "Type Update Error");
			}
			
			// reset
			CurrentType = -1;
			typeUpdateSuccess = -1;
		}
		
		/*
		 * Make all the UI modifications following a successful update
		 */
		updateTypeUI = function() {
			
			updateSelectBoxCurrent('typeCategorySelect');
		}
		
		/*
		 * Handler method to update a recipe
		 */
		updateRecipe = function(id) {
			
			// get the user input
			var userName = $('#recipeNameInput').val();
			var userType = $('#recipeTypeSelect').val();
			var userLocation = $('#recipeLocationSelect').val();
			var userSeason = $('#recipeSeasonSelect').val();
			var userMeal = $('#recipeMealSelect').val();
			
			var now = new Date();
			
			// create RecipeModel of the current user data
			CurrentRecipe = new RecipeModel(
				id, 
				userName, 
				userType, 
				userLocation,
				userSeason,
				userMeal, 
				now, 
				now, 
				true
			);
			
			// show the modal
			showModal('updateRecipeModal');
		}
		
		/*
		 * AJAX call to update a recipe
		 */
		updateRecipeAJAX = function() {
			
			try {
				
				$.ajax({
					type: "PUT",
					async: false,
					url: URL + "/recipe/" + CurrentRecipe.id + "/update",
					contentType: "application/json",
					dataType: "json",
					data: JSON.stringify({
						"id": CurrentRecipe.id,
						"name": CurrentRecipe.name,
						"type": CurrentRecipe.type,
						"location": CurrentRecipe.location,
						"season": CurrentRecipe.season,
						"meal": CurrentRecipe.meal,
						"is_valid": CurrentRecipe.is_valid
					}),
					success: function(data) {
						if (data["code"] == 1) {
							recipeUpdateSuccess = true;
						} else if (data["code"] == -1) {
							recipeUpdateSuccess = false;
						} else {
							recipeUpdateSuccess = -1;
						}
					},
					error: function(e) {
						console.log("Error event in AJAX");
					}
				});
				
			} catch (err) {
				console.log("Error when making AJAX call");
				console.log(err);
				toastr.warning("We're sorry for the inconvenience but the server was unable to complete your request.", "Server Response");
			}
		}
		
		/*
		 * Handler method to make the AJAX call
		 */
		updateRecipeConfirm = function() {
			
			// make the AJAX call
			updateRecipeAJAX();
			
			// hide the modal
			hideModal('updateRecipeModal');
			
			if (recipeUpdateSuccess == true) {
				
				// update UI
				updateRecipeUI();
				
				toastr.success("Recipe successfully updated", "Recipe Updated");
			} else {
				toastr.warning("We're sorry for the inconvenience but the server was unable to complete your request. Please check back later.", "Recipe Update Error");
			}
			
			// reset
			CurrentRecipe = -1;
			recipeUpdateSuccess = -1;
			
		}
		
		/*
		 * Make all the UI modifications following a successful update
		 */
		updateRecipeUI = function() {
			
			updateSelectBoxCurrent('recipeTypeSelect');
			updateSelectBoxCurrent('recipeLocationSelect');
			updateSelectBoxCurrent('recipeSeasonSelect');
			updateSelectBoxCurrent('recipeMealSelect');
		}
		
		/*
		 * Handler method to send a demo schedule
		 */
		emailDemoSchedule = function() {
			
			// get the user input
			var userName = getCurrentUsername();
			var userRole = getCurrentUserRole();
			
			CurrentUser = new UserModel(userName, userRole);
			
			// show the modal
			showModal('emailDemoScheduleModal');
		}
		
		/*
		 * AJAX call to send a demo schedule
		 */
		emailDemoScheduleAJAX = function() {
			
			try {
				
				$.ajax({
					type: "POST",
					async: false,
					url: URL + "/schedule/emaildemo",
					contentType: "application/json",
					dataType: "json",
					data: JSON.stringify({
						"recipient": CurrentUser.username
					}),
					success: function(data) {
						if (data["code"] == 1) {
							emailDemoScheduleSuccess = true;
						} else if (data["code"] == -1) {
							emailDemoScheduleSuccess = false;
						} else {
							emailDemoScheduleSuccess = -1;
						}
					},
					error: function(e) {
						console.log("Error event in AJAX");
					}
				});
				
			} catch (err) {
				console.log("Error when making AJAX call");
				console.log(err);
				toastr.warning("We're sorry for the inconvenience but the server was unable to complete your request.", "Server Response");
			}
		}
		
		/*
		 * Handler method to make the AJAX call
		 */
		emailDemoScheduleConfirm = function() {
			
			// make the AJAX call
			emailDemoScheduleAJAX();
			
			// hide the modal
			hideModal('emailDemoScheduleModal');
			
			if (emailDemoScheduleSuccess == true) {
				toastr.success("Email successfully sent", "Email Sent");
			} else {
				toastr.warning("We're sorry for the inconvenience but the server was unable to complete your request. Please check back later.", "Email Send Error")
			}
			
			// reset
			CurrentUser = -1;
			emailDemoScheduleSuccess = -1;
		}
		
		/*
		 * Given an id for a modal, show the modal
		 */
		showModal = function(modalId) {
			
			var key = "#".concat(modalId);
			$(key).modal('show');
			
			return self;
		}
		
		/*
		 * Given an id for a modal, hide the modal
		 */
		hideModal = function(modalId) {
			
			var key = "#".concat(modalId);
			$(key).modal('hide');
		}
		
		/*
		 * Given a <select> element, append the string " (current)" to the selected 
		 * <option> in the dropdown and remove the string " (current)" from all other 
		 * <option> elements.
		 */
		updateSelectBoxCurrent = function(selectBoxId) {
			
			// make a JQuery key
			var key = ("#".concat(selectBoxId)).concat(" option");
			
			// update the selected option in the select box and remove old text
			$(key).each(function() {
				
				
				var selectedOptionText = $(this).text();
				
				// if the <option> element is selected
				if ($(this).is(':selected')) {
					
					// the suffix is not " (current)"
					if (!(selectedOptionText.endsWith(" (current)"))) {
						
						// append " (current)" to the current option's text
						var selectedOptionText = selectedOptionText.concat(" (current)");
						$(this).text(selectedOptionText);
					}
					
				} else {
					
					// all other <option> elements
					if (selectedOptionText.endsWith(" (current)")) {
						
						// remove " (current)" if it exists
						$(this).text(selectedOptionText.slice(0, -10));
						
					}
				}
			});
		}
		
		/*
		 * Given two string, return true if they are equal. If not, return false.
		 */
		strcmp = function(str1, str2) {
			
			if (str1 == str2) {
				return true;
			}
			
			return false;
		}
		
		/*
		 * Given the id of a delete button and the id of a table, find the row in the datatable and 
		 * remove it dynamically.
		 * 
		 * i.e. removeDatatableRow('deleteButton1', 'recipe-list') will remove the row containing 
		 * #deleteButton1 in table #recipe-list
		 */
		removeDatatableRow = function(buttonId, table) {
			
			var key = '#'.concat(buttonId); // i.e. "#deleteButton1"
			var tableKey = '#'.concat(table); // i.e. "#recipe-list"
			
			// get the dataTable instance
			var oTable = $(tableKey).dataTable();
			
			// get the row
			var row = $(key).closest("tr").get(0);
			
			// remove the row from the table
			oTable.fnDeleteRow(oTable.fnGetPosition(row));
		}
		
		/*
		 * Find any #toastr-message elements and replace them with a toastr message
		 */
		runToastrMessage = function() {
			
			// if the element exists
			if ($('#toastr-message').length > 0) {
				
				// build the message
				var messageType = $('#toastr-message > .type').html();
				var header = $('#toastr-message > .header').html();
				var content = $('#toastr-message > .message').html();
				
				// remove from the DOM
				$('#toastr-message').remove();
				
				// run a toastr message
				if (messageType == "Success") {
					toastr.success(content, header);
				} else if (messageType == "Error") {
					toastr.warning(content, header);
				}
			}
		}
		
		/*
		 * Main method to execute functions that run on all pages
		 */
		main = function() {
			
			// run functions that should be run on all pages
			addUsernameToNavbar();
			runToastrMessage();
			
		}
		
		return {
			
			init: function() {
				
				// run main method
				main();
			},

			showModal: function(modalId) {
				showModal(modalId);
			},
			
			verifyValidUsername: function() {
				verifyValidUsername();
			},
			
			createNewUser: function() {
				createNewUser();
			},
			
			deleteRecipe: function(id) {
				deleteRecipe(id);
			},
			
			deleteRecipeConfirm: function() {
				deleteRecipeConfirm();
			},
			
			deleteLocation: function(id) {
				deleteLocation(id);
			},
			
			deleteLocationConfirm: function() {
				deleteLocationConfirm();
			},
			
			deleteType: function(id) {
				deleteType(id);
			},
			
			deleteTypeConfirm: function() {
				deleteTypeConfirm();
			},
			
			updateLocation: function(id) {
				updateLocation(id);
			},
			
			updateLocationConfirm: function() {
				updateLocationConfirm();
			},
			
			updateType: function(id) {
				updateType(id);
			},
			
			updateTypeConfirm: function() {
				updateTypeConfirm();
			},
			
			updateRecipe: function(id) {
				updateRecipe(id);
			},
			
			updateRecipeConfirm: function() {
				updateRecipeConfirm();
			},
			
			emailDemoSchedule: function() {
				emailDemoSchedule();
			},
			
			emailDemoScheduleConfirm: function() {
				emailDemoScheduleConfirm();
			}
		}
		
	}();
	
	// launch the LSGModule
	LSGModule.init();
	
});


