<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!-- CONTENT START -->

<div id="content" class="span11">

	<!-- breadcrumb navigation -->
	<ul class="breadcrumb">
		<li>
			<i class="icon-home"></i>
			<a href="/welcome">Home</a>
			<i class="icon-angle-right"></i>
		</li>
		<li>
			<a href="/location/">Locations</a>
		</li>
	</ul>

	<div class="row-fluid">
	
		<h1>Locations</h1>
		
		<c:if test="${not empty message}">
			
			<!-- Message that will be displayed by toastr -->
			<div id="toastr-message">
				<div class="type">Success</div>
				<div class="message">${message}</div>
				<div class="header">Location Created</div>
			</div>
			
		</c:if>
		
		<div class="box span9">
			
			<div class="box-header" data-original-title>
				<h2><i class="icon-map-marker"></i><span class="break"></span>Locations</h2>
			</div>
			
			<div class="box-content">
				<table id="location-list" width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
					<thead>
						<tr>
							<th class="center">Value</th>
							<th class="center">Modify</th>
						</tr>
					</thead>
					<tbody>
						<!-- Get the role of the user -->
						<c:forEach var="location" items="${locations}">
							<tr>
								<td>${location.value}</td>
								<td class="center">
									<c:if test="${location.update == true || role == 'ROLE_ADMIN'}">
										<span>
											<a class="btn btn-info" title="Edit" href="<c:url value='/location/${location.id}'></c:url>">
												<i class="halflings-icon white edit"></i>
											</a>
										</span>
										<span>
											<a id="deleteButton${location.id}" class="btn btn-danger" title="Delete" onclick="deleteLocation('${location.id}')">
												<i class="halflings-icon white trash"></i>
											</a>
										</span>
									</c:if>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
			
			<a class="btn btn-info span3 new-entity-btn" href="/location/add">New Location</a>
			
		</div>
	
	</div>

</div>

<!-- CONTENT END -->

<!-- MODALS START -->

<!-- Confirm Location Delete -->
<div class="modal hide fade" id="deleteLocationModal">
	
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">x</button>
		<h3>Location Delete Confirm</h3>
	</div>
	
	<div class="modal-body">
		<p>Are you sure you want to delete this location?</p>
	</div>
	
	<div class="modal-footer">
		<a href="#" class="btn" data-dismiss="modal">Cancel</a>
		<a href="#" class="btn" onclick="deleteLocationConfirm()">Delete</a>
	</div>
	
</div>

<!-- MODALS END -->
