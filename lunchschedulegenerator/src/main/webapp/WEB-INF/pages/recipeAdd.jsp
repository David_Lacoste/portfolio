<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!-- CONTENT START -->

<div id="content" class="span11">
	
	<!-- breadcrumb navigation -->
	<ul class="breadcrumb">
		<li>
			<i class="icon-home"></i>
			<a href="/welcome">Home</a>
			<i class="icon-angle-right"></i>
		</li>
		<li>
			<a href="/recipe/">Recipes</a>
		</li>
	</ul>
	
	<div class="container">
		
		<h1>New Recipe</h1>
		
		<c:if test="${not empty error}">
			<!-- Message that will be displayed by toastr -->
			<div id="toastr-message">
				<div class="type">Error</div>
				<div class="message">${error}</div>
				<div class="header">Recipe Creation Error</div>
			</div>
		</c:if>
		
		<form:errors path="recipe.*" cssClass="login-error" element="div" />
		
		<form name="recipeCreateForm" class="form-horizontal" action="/recipe" method="post">
			<fieldset class="new-entity-form">
				
				<div class="control-group">
					<label class="control-label" for="name" style="text-align: left; width: auto;">Name:</label>
					<div class="controls" style="margin-left: 0px;">
						<input id="name" class="input-xlarge focused" style="margin-left: 10px;" type="text" name="name" />
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label" for="typeSelect" style="text-align: left; width: auto;">Type:</label>
					<div class="controls" style="margin-left: 0px;">
						<select id="typeSelect" style="margin-left: 10px;" name="type" >
							<c:forEach var="type" items="${types}">
								<option id="type${type.id}" value="${type.id}">${type.value}</option>
							</c:forEach>
						</select>
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label" for="locationSelect" style="text-align: left; width: auto;">Location:</label>
					<div class="controls" style="margin-left: 0px;">
						<select id="locationSelect" style="margin-left: 10px;" name="location" >
							<c:forEach var="location" items="${locations}">
								<option id="location${location.id}" value="${location.id}">${location.value}</option>
							</c:forEach>
						</select>
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label" for="seasonSelect" style="text-align: left; width: auto;">Season:</label>
					<div class="controls" style="margin-left: 0px;">
						<select id="seasonSelect" style="margin-left: 10px;" name="season" >
							<c:forEach var="season" items="${seasons}">
								<option id="season${season.id}" value="${season.id}">${season.value}</option>
							</c:forEach>
						</select>
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label" for="mealSelect" style="text-align: left; width: auto;">Meal:</label>
					<div class="controls" style="margin-left: 0px;">
						<select id="mealSelect" style="margin-left: 10px;" name="meal" >
							<c:forEach var="meal" items="${meals}">
								<option id="meal${meal.id}" value="${meal.id}">${meal.value}</option>
							</c:forEach>
						</select>
					</div>
				</div>
			</fieldset>
			
			<a href="/recipe/list" class="btn btn-danger">Cancel</a>
			<button type="submit" class="btn btn-primary">Create</button>
		</form>
		
	</div>
	
</div>

<!-- CONTENT END -->
