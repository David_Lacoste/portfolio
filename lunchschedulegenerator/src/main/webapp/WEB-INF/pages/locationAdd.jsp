<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!-- CONTENT START -->

<div id="content" class="span11">
	
	<!-- breadcrumb navigation -->
	<ul class="breadcrumb">
		<li>
			<i class="icon-home"></i>
			<a href="/welcome">Home</a>
			<i class="icon-angle-right"></i>
		</li>
		<li>
			<a href="/location/">Locations</a>
		</li>
	</ul>
	
	<div class="container">
		
		<h1>New Location</h1>
		
		<c:if test="${not empty error}">
		
			<!-- Message that will be displayed by toastr -->
			<div id="toastr-message">
				<div class="type">Error</div>
				<div class="message">${error}</div>
				<div class="header">Location Creation Error</div>
			</div>
			
		</c:if>
		
		<form:errors path="location.*" cssClass="login-error" element="div" />
		
		<form name="locationCreateForm" class="form-horizontal" action="/location" method="post">
			<fieldset class="new-entity-form">
				
				<div class="control-group">
					<label class="control-label" for="value" style="text-align: left; width: auto;">Value:</label>
					<div class="controls" style="margin-left: 0px;">
						<input id="value" class="input-xlarge focused" style="margin-left: 10px;" type="text" name="value" />
					</div>
				</div>
				
			</fieldset>
			
			<a href="/location/list" class="btn btn-danger">Cancel</a>
			<button type="submit" class="btn btn-primary">Create</button>
		</form>

	</div>
	
</div>

<!-- CONTENT END -->
