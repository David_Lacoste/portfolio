<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<spring:url value="/resources/img/bg-login.jpg" var="backgroundPic" />

<style type="text/css">
	body {
		background: url(${backgroundPic}) !important;
	}
</style>

<div class="row-fluid">
	
	<!-- login box -->
	<div class="login-box">
		
		<h2>Login</h2>
		
		<!-- Error handling -->
		<c:choose>
			<c:when test="${not empty error}">
				<div class="login-error">
					<c:out value="${error}" />
				</div>
			</c:when>
			<c:when test="${not empty logout}">
				<div class="login-error">
					<c:out value="${logout}" />
				</div>
			</c:when>
		</c:choose>
		
		<form:errors path="user.*" cssClass="login-error" element="div" />
		
		<form name="loginForm" class="form-horizontal" action="<c:url value='/j_spring_security_check'></c:url>" method="post">
			<fieldset>
				
				<div class="input-prepend" title="Username">
					<span class="add-on"><i class="halflings-icon user"></i></span>
					<input class="input-large span10" name="username" id="username" type="text" placeholder="type email"/>
				</div>
				<div class="clearfix"></div>

				<div class="input-prepend" title="Password">
					<span class="add-on"><i class="halflings-icon lock"></i></span>
					<input class="input-large span10" name="password" id="password" type="password" placeholder="type password"/>
				</div>
				<div class="clearfix"></div>
				
				<!-- 
				"remember" class shifts left
				<label class="remember" for="remember">
					<input type="checkbox" id="remember" />
					Remember me
				</label>
				-->
				
				<div class="remember">
					<button type="button" class="btn btn-primary" onclick="verifyValidUsername()">Sign Up</button>
				</div>

				<div class="button-login">	
					<button type="submit" class="btn btn-primary">Login</button>
				</div>
				<div class="clearfix"></div>
				
			</fieldset>
		</form>
		
	</div>
</div>

<!-- MODALS START -->

<!-- Create User Modal -->
<div class="modal hide fade" id="createUserModal">
	
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">x</button>
		<h3>Create User</h3>
	</div>
	
	<div class="modal-body">
		<p>Are you sure you want to create a user with the provided email and password?</p>
	</div>
	
	<div class="modal-footer">
		<a href="#" class="btn" data-dismiss="modal">Cancel</a>
		<a href="#" class="btn" onclick="createNewUser()">Create</a>
	</div>
	
</div>

<!-- Error Modal -->
<div class="modal hide fade" id="errorModal">
	
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">x</button>
		<h3>Error</h3>
	</div>
	
	<div class="modal-body">
		<p>Please enter a valid email and password</p>
	</div>
	
	<div class="modal-footer">
		<a href="#" class="btn" data-dismiss="modal">OK</a>
	</div>
	
</div>

<!-- MODALS END -->
