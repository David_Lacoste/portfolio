<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!-- CONTENT START -->

<div id="content" class="span12">
	
	<!-- breadcrumb navigation -->
	<ul class="breadcrumb">
		<li>
			<i class="icon-home"></i>
			<a href="/welcome">Home</a>
			<i class="icon-angle-right"></i>
		</li>
		<li>
			<a href="/weeklymenu">Weekly Menu</a>
		</li>
	</ul>
	
	<div class="row-fluid sortable">
		
		<h1>This Week's Menu</h1>
		
		<br>
		
		<p>The weekly menu generation feature is still under development. However, you may have a demo weekly menu emailed to you.</p>
		<p>Click the button below to have a sample weekly menu PDF sent to your email address (your username)</p>
		
		<br>
			
		<div class="box span11">
			<div class="box-header">
				<h2>Menus for: June 18, 2016 - June 24, 2016</h2>
			</div>
			<div class="box-content">
				<table class="table">
					  <thead>
						  <tr>
							  <th class="center">Jour</th>
							  <th class="center">Samedi</th>
							  <th class="center">Dimanche</th>
							  <th class="center">Lundi</th>
							  <th class="center">Mardi</th>
							  <th class="center">Mercredi</th>
							  <th class="center">Jeudi</th>
							  <th class="center">Vendredi</th>                                          
						  </tr>
					  </thead>   
					  <tbody>
						<tr>
							<td class="center row-title">Date</td>
							<td class="center">18</td>
							<td class="center">19</td>
							<td class="center">20</td>
							<td class="center">21</td>
							<td class="center">22</td>
							<td class="center">23</td>
							<td class="center">24</td>
						</tr>
						<tr>
							<td align="right" class="center row-title">Info</td>
							<td class="center">
								<span class="center">- &nbsp;</span>
								<span class="center">
									<a class="btn btn-info" title="Edit" href="#">
										<i class="halflings-icon white edit"></i>
									</a>
								</span>
							</td>
							<td class="center">
								<span class="center">- &nbsp;</span>
								<span class="center">
									<a class="btn btn-info" title="Edit" href="#">
										<i class="halflings-icon white edit"></i>
									</a>
								</span>
							</td>
							<td class="center">
								<span class="center">- &nbsp;</span>
								<span class="center">
									<a class="btn btn-info" title="Edit" href="#">
										<i class="halflings-icon white edit"></i>
									</a>
								</span>
							</td>
							<td class="center">
								<span class="center">- &nbsp;</span>
								<span class="center">
									<a class="btn btn-info" title="Edit" href="#">
										<i class="halflings-icon white edit"></i>
									</a>
								</span>
							</td>
							<td class="center">
								<span class="center">- &nbsp;</span>
								<span class="center">
									<a class="btn btn-info" title="Edit" href="#">
										<i class="halflings-icon white edit"></i>
									</a>
								</span>
							</td>
							<td class="center">
								<span class="center">- &nbsp;</span>
								<span class="center">
									<a class="btn btn-info" title="Edit" href="#">
										<i class="halflings-icon white edit"></i>
									</a>
								</span>
							</td>
							<td class="center">
								<span class="center">- &nbsp;</span>
								<span class="center">
									<a class="btn btn-info" title="Edit" href="#">
										<i class="halflings-icon white edit"></i>
									</a>
								</span>
							</td>
						</tr>
						<tr>
							<td class="center row-title">D�ner</td>
							<td class="center">
								<span class="center">Hot Dog (Recettes) &nbsp;</span>
								<span class="center">
									<a class="btn btn-info" title="Edit" href="#">
										<i class="halflings-icon white edit"></i>
									</a>
								</span>
							</td>
							<td class="center">
								<span class="center">Kraft Dinner (Recettes) &nbsp;</span>
								<span class="center">
									<a class="btn btn-info" title="Edit" href="#">
										<i class="halflings-icon white edit"></i>
									</a>
								</span>
							</td>
							<td class="center">
								<span class="center">- &nbsp;</span>
								<span class="center">
									<a class="btn btn-info" title="Edit" href="#">
										<i class="halflings-icon white edit"></i>
									</a>
								</span>
							</td>
							<td class="center">
								<span class="center">- &nbsp;</span>
								<span class="center">
									<a class="btn btn-info" title="Edit" href="#">
										<i class="halflings-icon white edit"></i>
									</a>
								</span>
							</td>
							<td class="center">
								<span class="center">- &nbsp;</span>
								<span class="center">
									<a class="btn btn-info" title="Edit" href="#">
										<i class="halflings-icon white edit"></i>
									</a>
								</span>
							</td>
							<td class="center">
								<span class="center">- &nbsp;</span>
								<span class="center">
									<a class="btn btn-info" title="Edit" href="#">
										<i class="halflings-icon white edit"></i>
									</a>
								</span>
							</td>
							<td class="center">
								<span class="center">- &nbsp;</span>
								<span class="center">
									<a class="btn btn-info" title="Edit" href="#">
										<i class="halflings-icon white edit"></i>
									</a>
								</span>
							</td>
						</tr>
						<tr>
							<td class="center row-title">Souper</td>
							<td class="center">
								<span class="center">Sweet BBQ Chicken Kebobs (Recettes) &nbsp;</span>
								<span class="center">
									<a class="btn btn-info" title="Edit" href="#">
										<i class="halflings-icon white edit"></i>
									</a>
								</span>
							</td>
							<td class="center">
								<span class="center">Jambon po�le (Recettes) &nbsp;</span>
								<span class="center">
									<a class="btn btn-info" title="Edit" href="#">
										<i class="halflings-icon white edit"></i>
									</a>
								</span>
							</td>
							<td class="center">
								<span class="center">P�tes pesto (Classic Pasta) &nbsp;</span>
								<span class="center">
									<a class="btn btn-info" title="Edit" href="#">
										<i class="halflings-icon white edit"></i>
									</a>
								</span>
							</td>
							<td class="center">
								<span class="center">Family Classic Meatloaf (Recettes) &nbsp;</span>
								<span class="center">
									<a class="btn btn-info" title="Edit" href="#">
										<i class="halflings-icon white edit"></i>
									</a>
								</span>
							</td>
							<td class="center">
								<span class="center">P�tes bolognese (Classic Pasta) &nbsp;</span>
								<span class="center">
									<a class="btn btn-info" title="Edit" href="#">
										<i class="halflings-icon white edit"></i>
									</a>
								</span>
							</td>
							<td class="center">
								<span class="center">Shakshouka (Recettes) &nbsp;</span>
								<span class="center">
									<a class="btn btn-info" title="Edit" href="#">
										<i class="halflings-icon white edit"></i>
									</a>
								</span>
							</td>
							<td class="center">
								<span class="center">Gaby et Yoyo &nbsp;</span>
								<span class="center">
									<a class="btn btn-info" title="Edit" href="#">
										<i class="halflings-icon white edit"></i>
									</a>
								</span>
							</td>
						</tr>
					  </tbody>
				 </table>
			</div>
		</div><!--/span-->
		
		<button type="button" class="btn btn-info span3" onclick="emailDemoSchedule()">Email Demo Weekly Menu</button>

	</div>
	
</div>

<!-- CONTENT END -->

<!-- MODALS START -->

<!-- Confirm Send Demo Schedule -->
<div class="modal hide fade" id="emailDemoScheduleModal">
	
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">x</button>
		<h3>Email Demo Weekly Menu Confirm</h3>
	</div>
	
	<div class="modal-body">
		<p>Are you sure you want to send a demo email to the following email address?</p>
		<p>Email: <c:out value="${username}"/></p>
	</div>
	
	<div class="modal-footer">
		<a href="#" class="btn" data-dismiss="modal">Cancel</a>
		<a href="#" class="btn" onclick="emailDemoScheduleConfirm()">Send Email</a>
	</div>
	
</div>

<!-- MODALS END -->