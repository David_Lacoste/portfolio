<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!-- CONTENT START -->

<div id="content" class="span11">

	<!-- breadcrumb navigation -->
	<ul class="breadcrumb">
		<li>
			<i class="icon-home"></i>
			<a href="/welcome">Home</a>
			<i class="icon-angle-right"></i>
		</li>
		<li>
			<a href="/type/">Types</a>
		</li>
	</ul>
	
	<div class="row-fluid">
	
		<h1>Types</h1>
		
		<c:if test="${not empty message}">
			
			<!-- Message that will be displayed by toastr -->
			<div id="toastr-message">
				<div class="type">Success</div>
				<div class="message">${message}</div>
				<div class="header">Type Created</div>
			</div>
			
		</c:if>
		
		<div class="box span9">
		
			<div class="box-header" data-original-title>
				<h2><i class="icon-tag"></i><span class="break"></span>Types</h2>
			</div>
			
			<div class="box-content">
				<table id="type-list" width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
					<thead>
						<tr>
							<th class="center">Value</th>
							<th class="center">Category</th>
							<th class="center">Modify</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="type" items="${types}">
							<tr>
								<td>${type.value}</td>
								<td>${type.category.value}</td>
								<td class="center">
									<c:if test="${type.update == true || role == 'ROLE_ADMIN'}">
										<span>
											<a class="btn btn-info" title="Edit" href="<c:url value='/type/${type.id}'></c:url>">
												<i class="halflings-icon white edit"></i>
											</a>
										</span>
										<span>
											<a id="deleteButton${type.id}" title="Delete" class="btn btn-danger" onclick="deleteType('${type.id}')">
												<i class="halflings-icon white trash"></i>
											</a>
										</span>
									</c:if>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
			
			<a class="btn btn-info span3 new-entity-btn" href="/type/add">New Type</a>
		
		</div>
	
	</div>

</div>

<!-- CONTENT END -->

<!-- MODALS START -->

<!-- Confirm Type Delete -->
<div class="modal hide fade" id="deleteTypeModal">
	
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">x</button>
		<h3>Type Delete Confirm</h3>
	</div>
	
	<div class="modal-body">
		<p>Are you sure you want to delete this type?</p>
	</div>
	
	<div class="modal-footer">
		<a href="#" class="btn" data-dismiss="modal">Cancel</a>
		<a href="#" class="btn" onclick="deleteTypeConfirm()">Delete</a>
	</div>
	
</div>

<!-- MODALS END -->
