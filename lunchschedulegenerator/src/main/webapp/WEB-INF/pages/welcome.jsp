<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!-- CONTENT START -->

<div id="content" class="span10">
	
	<!-- breadcrumb navigation -->
	<ul class="breadcrumb">
		<li>
			<i class="icon-home"></i>
			<a href="#">Welcome</a> 
		</li>
	</ul>
	
	<div class="row-fluid">
		
		<h1>Welcome to the Lunch Schedule Generator tool!</h1>
		
		<br>
		
		<!-- 
		<div class="box black span11">
		
			<div class="box-header">
				<h2>
					<i class="halflings-icon white list"></i>
					<span class="break"></span>
					Updates
				</h2>
			</div>
			
			<div class="box-content">
				<ul class="dashboard-list metro">
					<li>
						<a href="#">
							<i class="icon-ok green"></i>
							Schedule sent for the week of <strong>July 9, 2016 - July 15, 2016</strong>
						</a>
					</li>
					<li>
						<a href="#">
							<i class="icon-ok green"></i>
							Schedule sent for the week of <strong>July 9, 2016 - July 15, 2016</strong>
						</a>
					</li>
					<li>
						<a href="#">
							<i class="icon-ok green"></i>
							Schedule sent for the week of <strong>July 9, 2016 - July 15, 2016</strong>
						</a>
					</li>
					<li>
						<a href="#">
							<i class="icon-remove red"></i>
							Error sending schedule for the week of <strong>July 9, 2016 - July 15, 2016</strong>
						</a>
					</li>
					<li>
						<a href="#">
							<i class="icon-ok green"></i>
							Schedule sent for the week of <strong>July 9, 2016 - July 15, 2016</strong>
						</a>
					</li>
					<li>
						<a href="#">
							<i class="icon-ok green"></i>
							Schedule sent for the week of <strong>July 9, 2016 - July 15, 2016</strong>
						</a>
					</li>
				</ul>
			</div>
		</div>
		 -->
		 
		<div class="box black span11">
			
			<div class="box-header">
				<h2>
					<i class="halflings-icon white list"></i>
					<span class="break"></span>
					Features Changelog
				</h2>
			</div> <!-- /box-header -->
			
			<div class="box-content">
				
				<ul class="dashboard-list metro">
					<li>
						<a href="#">
							<i class="icon-ok green"></i>
							User creation
						</a>
					</li>
					<li>
						<a href="#">
							<i class="icon-ok green"></i>
							Recipe CRUD
						</a>
					</li>
					<li>
						<a href="#">
							<i class="icon-ok green"></i>
							Location CRUD
						</a>
					</li>
					<li>
						<a href="#">
							<i class="icon-ok green"></i>
							Type CRUD
						</a>
					</li>
					<li>
						<a href="#">
							<i class="icon-ok green"></i>
							Email notification functionality
						</a>
					</li>
					<li>
						<a href="#">
							<i class="icon-remove red"></i>
							Menu update/delete
						</a>
					</li>
					<li>
						<a href="#">
							<i class="icon-remove red"></i>
							Menu generation automation
						</a>
					</li>
				</ul> <!-- /ul -->
				
			</div> <!-- /box-content -->
			
		</div> <!-- /box -->
		
	</div>
	
	<p><strong>**CRUD: </strong>Create/Update/Delete</p>
	
</div>

<!-- CONTENT END -->