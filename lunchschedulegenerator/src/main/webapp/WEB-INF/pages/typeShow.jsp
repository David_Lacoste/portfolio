<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!-- CONTENT START -->

<div id="content" class="span11">
	
	<!-- breadcrumb navigation -->
	<ul class="breadcrumb">
		<li>
			<i class="icon-home"></i>
			<a href="/welcome">Home</a>
			<i class="icon-angle-right"></i>
		</li>
		<li>
			<a href="/type/">Types</a>
		</li>
	</ul>
	
	<div class="container">
		
		<h1>Edit Type</h1>
		
		<table class="table table-striped span12">
			<tr>
				<td class="row-title">Value:</td>
				<td>
					<div class="control-group no-margins">
						<div class="controls">
							<input id="typeValueInput" class="input-xlarge focused no-margins" type="text" value="${type.value}">
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="row-title">Category:</td>
				<td>
					<div class="control-group no-margins">
						<div class="controls">
							<select id="typeCategorySelect" class="no-margins">
								<c:forEach var="category" items="${categories}">
									<c:choose>
										<c:when test="${type.category.id == category.id}">
											<option id="category${category.id}" value="${category.id}" selected>${category.value} (current)</option>
										</c:when>
										<c:otherwise>
											<option id="category${category.id}" value="${category.id}">${category.value}</option>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</select>
						</div>
					</div>
				</td>
			</tr>
		</table>
		
		<button type="button" class="btn btn-info" onclick="updateType(${type.id})">Update</button>
		<a href="/type/list" class="btn btn-danger">Cancel</a>
		
	</div>
	
</div>

<!-- CONTENT END -->

<!-- MODALS START -->

<!-- Confirm Location Update -->
<div class="modal hide fade" id="updateTypeModal">
	
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">x</button>
		<h3>Type Update Confirm</h3>
	</div>
	
	<div class="modal-body">
		<p>Changes made to this type will affect any recipes using this type.</p>
		<p>Are you sure you want to update this type?</p>
	</div>
	
	<div class="modal-footer">
		<a href="#" class="btn" data-dismiss="modal">Cancel</a>
		<a href="#" class="btn" onclick="updateTypeConfirm()">Update</a>
	</div>
	
</div>

<!-- MODALS END -->
