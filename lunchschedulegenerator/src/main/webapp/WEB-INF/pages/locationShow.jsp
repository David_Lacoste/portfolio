<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!-- CONTENT START -->

<div id="content" class="span11">

	<!-- breadcrumb navigation -->
	<ul class="breadcrumb">
		<li>
			<i class="icon-home"></i>
			<a href="/welcome">Home</a>
			<i class="icon-angle-right"></i>
		</li>
		<li>
			<a href="/location/">Locations</a>
		</li>
	</ul>
	
	<div class="container">
		
		<h1>Edit Location</h1>
		
		<table class="table table-striped span12">
			<tr>
				<td class="row-title">Value:</td>
				<td>
					<div class="control-group no-margins">
						<div class="controls">
							<input id="locationValueInput" class="input-xlarge focused no-margins" type="text" value="${location.value}">
						</div>
					</div>
				</td>
			</tr>
		</table>
		
		<button type="button" class="btn btn-info" onclick="updateLocation(${location.id})">Update</button>
		<a href="/location/list" class="btn btn-danger">Cancel</a>
		
	</div>

</div>

<!-- CONTENT END -->

<!-- MODALS START -->

<!-- Confirm Location Update -->
<div class="modal hide fade" id="updateLocationModal">
	
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">x</button>
		<h3>Location Update Confirm</h3>
	</div>
	
	<div class="modal-body">
		<p>Changes made to this location will affect any recipes using this location.</p>
		<p>Are you sure you want to update this location?</p>
	</div>
	
	<div class="modal-footer">
		<a href="#" class="btn" data-dismiss="modal">Cancel</a>
		<a href="#" class="btn" onclick="updateLocationConfirm()">Update</a>
	</div>
	
</div>

<!-- MODALS END -->
