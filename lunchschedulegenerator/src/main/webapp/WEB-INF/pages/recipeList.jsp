<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!-- CONTENT START -->

<div id="content" class="span11">

	<!-- breadcrumb navigation -->
	<ul class="breadcrumb">
		<li>
			<i class="icon-home"></i>
			<a href="/welcome">Home</a>
			<i class="icon-angle-right"></i>
		</li>
		<li>
			<a href="/recipe/">Recipes</a>
		</li>
	</ul>
	
	<div class="row-fluid">
		
		<h1>Recipes</h1>
		
		<c:if test="${not empty message}">
			<!-- Message that will be displayed by toastr -->
			<div id="toastr-message">
				<div class="type">Success</div>
				<div class="message">${message}</div>
				<div class="header">Recipe Created</div>
			</div>
		</c:if>
		
		<div class="box span12">
			
			<div class="box-header" data-original-title>
				<h2><i class="icon-list-alt"></i><span class="break"></span>Recipes</h2>
			</div>
			
			<div class="box-content">
				<table id="recipe-list" width="100%" class="table table-striped table-bordered bootstrap-datatable datatable">
				<thead>
					<tr>
					  <th class="center">Name</th>
					  <th class="center">Type</th>
					  <th class="center">Location</th>
					  <th class="center">Season</th>
					  <th class="center">Meal</th>
					  <th class="center">Modify</th>                                          
				  </tr>
				</thead>
				<tbody>
					<c:forEach var="recipe" items="${recipes}">
						<tr>
							<td>${recipe.name}</td>
							<td>${recipe.type.value}</td>
							<td>${recipe.location.value}</td>
							<td>${recipe.season.value}</td>
							<td>${recipe.meal.value}</td>
							<td class="center">
								<span>
									<a class="btn btn-info" title="Edit" href="<c:url value='/recipe/${recipe.id}'></c:url>">
										<i class="halflings-icon white edit"></i>
									</a>
								</span>
								<span>
									<a id="deleteButton${recipe.id}" title="Delete" class="btn btn-danger" onclick="deleteRecipe('${recipe.id}')">
										<i class="halflings-icon white trash"></i>
									</a>
								</span>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			</div>
			
			<a class="btn btn-info span3 new-entity-btn" href="/recipe/add">New Recipe</a>
		</div>
		
	</div>

</div>

<!-- CONTENT END -->

<!-- MODALS START -->

<!-- Confirm Recipe Delete -->
<div class="modal hide fade" id="deleteRecipeModal">
	
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">x</button>
		<h3>Recipe Delete Confirm</h3>
	</div>
	
	<div class="modal-body">
		<p>Are you sure you want to delete this recipe?</p>
	</div>
	
	<div class="modal-footer">
		<a href="#" class="btn" data-dismiss="modal">Cancel</a>
		<a href="#" class="btn" onclick="deleteRecipeConfirm()">Delete</a>
	</div>
	
</div>

<!-- MODALS END -->
