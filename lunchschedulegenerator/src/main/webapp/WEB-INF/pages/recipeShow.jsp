<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!-- CONTENT START -->

<div id="content" class="span11">
	
	<!-- breadcrumb navigation -->
	<ul class="breadcrumb">
		<li>
			<i class="icon-home"></i>
			<a href="/welcome">Home</a>
			<i class="icon-angle-right"></i>
		</li>
		<li>
			<a href="/recipe/">Recipes</a>
		</li>
	</ul>
	
	<div class="container">
		
		<h1>Edit Recipe</h1>
		
		<table class="table table-striped span12">
			<tr>
				<td class="row-title">Name:</td>
				<td>
					<div class="control-group no-margins">
						<div class="controls">
							<input id="recipeNameInput" class="input-xlarge focused no-margins" type="text" value="${recipe.name}">
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="row-title">Type:</td>
				<td>
					<div class="control-group no-margins">
						<div class="controls">
							<select id="recipeTypeSelect" class="no-margins">
								<c:forEach var="type" items="${types}">
									<c:choose>
										<c:when test="${recipe.type.value == type.value}">
											<option id="type${type.id}" value="${type.id}" selected>${type.value} (current)</option>
										</c:when>
										<c:otherwise>
											<option id="type${type.id}" value="${type.id}">${type.value}</option>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</select>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="row-title">Location:</td>
				<td>
					<div class="control-group no-margins">
						<div class="controls">
							<select id="recipeLocationSelect" class="no-margins">
								<c:forEach var="location" items="${locations}">
									<c:choose>
										<c:when test="${recipe.location.value == location.value}">
											<option id="location${location.id}" value="${location.id}" selected>${location.value} (current)</option>
										</c:when>
										<c:otherwise>
											<option id="location${location.id}" value="${location.id}">${location.value}</option>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</select>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="row-title">Season:</td>
				<td>
					<div class="control-group no-margins">
						<div class="controls">
							<select id="recipeSeasonSelect" class="no-margins">
								<c:forEach var="season" items="${seasons}">
									<c:choose>
										<c:when test="${recipe.season.value == season.value}">
											<option id="season${season.id}" value="${season.id}" selected>${season.value} (current)</option>
										</c:when>
										<c:otherwise>
											<option id="season${season.id}" value="${season.id}">${season.value}</option>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</select>
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="row-title">Meal:</td>
				<td>
					<div class="control-group no-margins">
						<div class="controls">
							<select id="recipeMealSelect" class="no-margins">
								<c:forEach var="meal" items="${meals}">
									<c:choose>
										<c:when test="${recipe.meal.id == meal.id}">
											<option id="meal${meal.id}" value="${meal.id}" selected>${meal.value} (current)</option>
										</c:when>
										<c:otherwise>
											<option id="meal${meal.id}" value="${meal.id}">${meal.value}</option>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</select>
						</div>
					</div>
				</td>
			</tr>
		</table>
		
		<button type="button" class="btn btn-info" onclick="updateRecipe(${recipe.id})">Update</button>
		<a href="/recipe/list" class="btn btn-danger">Cancel</a>
		
	</div>
	
</div>

<!-- CONTENT END -->

<!-- MODALS START -->

<!-- Confirm Location Update -->
<div class="modal hide fade" id="updateRecipeModal">
	
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">x</button>
		<h3>Recipe Update Confirm</h3>
	</div>
	
	<div class="modal-body">
		<p>Are you sure you want to update this recipe?</p>
	</div>
	
	<div class="modal-footer">
		<a href="#" class="btn" data-dismiss="modal">Cancel</a>
		<a href="#" class="btn" onclick="updateRecipeConfirm()">Update</a>
	</div>
	
</div>

<!-- MODALS END -->
