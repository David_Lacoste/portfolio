<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
	<!-- MOBILE SPECIFIC START -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- MOBILE SPECIFIC END -->
	
	<title>
		<tiles:insertAttribute name="title" ignore="true" />
	</title>
	
	<!-- Make glyphicons available as a resource -->
	<spring:url value="/resources/img/glyphicons_halflings.svg" var="glyphicons" />
	
	<!-- FAVICONS START -->
	
	<spring:url value="/resources/img/favicons/favicon-16x16.png" var="favicon16" />
	<spring:url value="/resources/img/favicons/favicon-32x32.png" var="favicon32" />
	<spring:url value="/resources/img/favicons/favicon-96x96.png" var="favicon96" />
	<link rel="icon" type="image/png" sizes="16x16" href="${favicon16}">
	<link rel="icon" type="image/png" sizes="32x32" href="${favicon32}">
	<link rel="icon" type="image/png" sizes="96x96" href="${favicon96}">
	
	<!-- FAVICONS END -->
	
	<!-- CSS START -->

	<!-- Bootstrap and responsive bootstrap -->
	<spring:url value="/resources/css/bootstrap.css" var="bootstrapCSS" />
	<spring:url value="/resources/css/bootstrap-responsive.css" var="bootstrapResponsiveCSS" />
	<link id="bootstrap-style" href="${bootstrapCSS}" rel="stylesheet"/>
	<link href="${bootstrapResponsiveCSS}" rel="stylesheet" />
	
	<!-- Custom stylesheets -->
	<spring:url value="/resources/css/style.css" var="customCSS" />
	<spring:url value="/resources/css/style-responsive.css" var="customResponsiveCSS" />
	<link id="base-style" href="${customCSS}" rel="stylesheet" />
	<link id="base-style-responsive" href="${customResponsiveCSS}" rel="stylesheet" />
	
	<!-- Toastr -->
	<spring:url value="/resources/css/toastr.css" var="toastr" />
	<link href="${toastr}" rel="stylesheet" />
	
	<!-- Google Fonts API -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
	
	<!-- IE6-8 and IE9 support -->
	<spring:url value="/resources/css/ie.css" var="ieCSS" />
	<spring:url value="/resources/css/ie9.css" var="ie9CSS" />
	<!--[if lt IE 9]>
	  	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<link id="ie-style" href="${ieCSS}" rel="stylesheet">
	<![endif]-->
	
	<!--[if IE 9]>
		<link id="ie9style" href="${ie9CSS}" rel="stylesheet">
	<![endif]-->
	   
	<!-- CSS END -->
    
</head>
<body>
	<tiles:insertAttribute name="navbar" />
	
	<div class="container-fluid-full">
		<div class="row-fluid">
			<tiles:insertAttribute name="header" />
			<tiles:insertAttribute name="sidebar" />
			<tiles:insertAttribute name="body" />
		</div>
	</div>
	
	<tiles:insertAttribute name="footer" />
	<tiles:insertAttribute name="endIncludes" />
</body>
</html>