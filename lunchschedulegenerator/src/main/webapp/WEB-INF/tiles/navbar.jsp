<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>

<spring:url value="/resources/font/fontawesome-webfont-62877.woff" var="fontAwesomeWoff" />
<spring:url value="/resources/font/fontawesome-webfont-62877.ttf" var="fontAwesomeTtf" />
<spring:url value="/resources/img/logos/logo32.png" var="logo32" />

<div class="navbar">
	<div class="navbar-inner">
		<div class="container-fluid">
			
			<!-- button for collapsed menu -->
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			
			<!-- Site name at top-left -->
			<a class="brand" href="/welcome">
				<span><img src="${logo32}"></span>
				&nbsp;
				<span><spring:message code="Label.siteName" /></span>
			</a>
			
			<div class="nav-no-collapse header-nav">
				<ul class="nav pull-right">
					<li class="dropdown">
						<a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
							<i class="halflings-icon white user"></i>
							
							<!-- #username-anchor is replaced by cookie value -->
							<div id="username-anchor"></div>
							
							<span class="caret"></span>
						</a>
						<ul class="dropdown-menu">
							<li class="dropdown-menu-title">
								<span>Options</span>
							</li>
							<!-- 
							<li>
								<a href="#">
									<i class="halflings-icon user"></i>
									Profile
								</a>
							</li>
							 -->
							<li>
								<a href="/login/logout">
									<i class="halflings-icon off"></i>
									Logout
								</a>
							</li>
						</ul>
					</li>
				</ul>
			</div>
			
		</div>
	</div>
</div>