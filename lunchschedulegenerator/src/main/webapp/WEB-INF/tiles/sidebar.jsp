<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>

<!-- SIDEBAR START -->

<div id="sidebar-left" class="span1">
	<div class="nav-collapse sidebar-nav">
		
		<!-- Sidebar menu items -->
		<ul id="sidebar-list" class="nav nav-tabs nav-stacked main-menu">
			<li>
				<a href="/welcome">
					<i class="icon-bar-chart"></i>
					<span class="hidden-tablet"><spring:message code="Label.sidebar.panel1" /></span>
				</a>
			</li>
			<li>
				<a href="/weeklymenu">
					<i class="icon-calendar"></i>
					<span class="hidden-tablet"><spring:message code="Label.sidebar.panel2" /></span>
				</a>
			</li>
			<li>
				<a href="/recipe/list">
					<i class="icon-list-alt"></i>
					<span class="hidden-tablet"><spring:message code="Label.sidebar.panel3" /></span>
				</a>
			</li>
			<li>
				<a href="/location/list">
					<i class="icon-map-marker"></i>
					<span class="hidden-tablet"><spring:message code="Label.sidebar.panel4" /></span>
				</a>
			</li>
			<li>
				<a href="/type/list">
					<i class="icon-tag"></i>
					<span class="hidden-tablet"><spring:message code="Label.sidebar.panel5" /></span>
				</a>
			</li>
		</ul>
	</div>
</div>

<!-- SIDEBAR END -->