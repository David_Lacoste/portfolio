<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>

<!-- SCRIPTS START -->

<!-- jQuery -->
<spring:url value="/resources/js/jquery-1.9.1.min.js" var="jQueryJs" />
<spring:url value="/resources/js/jquery-migrate-1.0.0.min.js" var="jqueryMigrate" />
<spring:url value="/resources/js/jquery-ui-1.10.0.custom.min.js" var="jqueryUi" />
<spring:url value="/resources/js/jquery.ui.touch-punch.js" var="jqueryUiTouchPunch" />
<spring:url value="/resources/js/modernizr.js" var="modernizr" />
<spring:url value="/resources/js/jquery.cookie.js" var="jqueryCookie" />
<spring:url value="/resources/js/fullcalendar.min.js" var="fullcalendar" />
<spring:url value="/resources/js/jquery.dataTables.min.js" var="jqueryDataTables" />
<spring:url value="/resources/js/excanvas.js" var="excanvas" />
<spring:url value="/resources/js/jquery.flot.js" var="jqueryFlot" />
<spring:url value="/resources/js/jquery.flot.pie.js" var="jqueryFlotPie" />
<spring:url value="/resources/js/jquery.flot.stack.js" var="jqueryFlotStack" />
<spring:url value="/resources/js/jquery.flot.resize.min.js" var="jqueryFlotResize" />
<spring:url value="/resources/js/jquery.chosen.min.js" var="jqueryChosen" />
<spring:url value="/resources/js/jquery.uniform.min.js" var="jqueryUniform" />
<spring:url value="/resources/js/jquery.cleditor.min.js" var="jqueryCleditor" />
<spring:url value="/resources/js/jquery.noty.js" var="jqueryNoty" />
<spring:url value="/resources/js/jquery.elfinder.min.js" var="jqueryElfinder" />
<spring:url value="/resources/js/jquery.raty.min.js" var="jqueryRaty" />
<spring:url value="/resources/js/jquery.iphone.toggle.js" var="jqueryIphoneToggle" />
<spring:url value="/resources/js/jquery.uploadify-3.1.min.js" var="jqueryUploadify" />
<spring:url value="/resources/js/jquery.gritter.min.js" var="jqueryGritter" />
<spring:url value="/resources/js/jquery.imagesloaded.js" var="jqueryImagesloaded" />
<spring:url value="/resources/js/jquery.masonry.min.js" var="jqueryMasonry" />
<spring:url value="/resources/js/jquery.knob.modified.js" var="jqueryKnobModified" />
<spring:url value="/resources/js/jquery.sparkline.min.js" var="jquerySparkline" />
<spring:url value="/resources/js/counter.js" var="counter" />
<spring:url value="/resources/js/retina.js" var="retina" />
<spring:url value="/resources/js/custom.js" var="custom" />
<spring:url value="/resources/js/ref/lunch-schedule-generator.js" var="lunchScheduleGenerator" />


<script src="${jQueryJs}"></script>
<script src="${jqueryMigrate}"></script>
<script src="${jqueryUi}"></script>
<script src="${jqueryUiTouchPunch}"></script>
<script src="${modernizr}"></script>
<script src="${jqueryCookie}"></script>
<script src="${fullcalendar}"></script>
<script src="${jqueryDataTables}"></script>
<script src="${excanvas}"></script>
<script src="${jqueryFlot}"></script>
<script src="${jqueryFlotPie}"></script>
<script src="${jqueryFlotStack}"></script>
<script src="${jqueryFlotResize}"></script>
<script src="${jqueryChosen}"></script>
<script src="${jqueryUniform}"></script>
<script src="${jqueryCleditor}"></script>
<script src="${jqueryNoty}"></script>
<script src="${jqueryElfinder}"></script>
<script src="${jqueryRaty}"></script>
<script src="${jqueryIphoneToggle}"></script>
<script src="${jqueryUploadify}"></script>
<script src="${jqueryGritter}"></script>
<script src="${jqueryImagesloaded}"></script>
<script src="${jqueryMasonry}"></script>
<script src="${jqueryKnobModified}"></script>
<script src="${jquerySparkline}"></script>
<script src="${counter}"></script>
<script src="${retina}"></script>
<script src="${custom}"></script>

<!-- Bootstrap -->
<spring:url value="/resources/js/bootstrap.js" var="bootstrapJs" />
<script src="${bootstrapJs}"></script>

<!-- Toastr -->
<spring:url value="/resources/js/toastr.min.js" var="toastrJs" />
<script src="${toastrJs}"></script>

<!-- Application JS -->
<script src="${lunchScheduleGenerator}"></script>

<!-- SCRIPTS END -->