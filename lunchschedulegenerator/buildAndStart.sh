#! /bin/bash

# clean existing WAR
mvn clean

# build test WAR
mvn package -Denv=test -Ddefault

# start the test server at localhost:8080
java -jar target/dependency/webapp-runner.jar target/*.war
