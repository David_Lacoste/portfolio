-- MySQL dump 10.13  Distrib 5.7.11, for osx10.10 (x86_64)
--
-- Host: localhost    Database: lunch_scheduler_web
-- ------------------------------------------------------
-- Server version	5.7.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- DAVID ADDED START
-- Drop database
DROP DATABASE IF EXISTS lunch_scheduler_web;
CREATE DATABASE lunch_scheduler_web;
USE lunch_scheduler_web;
-- DAVID ADDED END

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(50) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_valid` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'Viande','2016-07-22 04:34:16','2016-07-22 04:34:16',1),(2,'Pates','2016-07-22 04:34:16','2016-07-22 04:34:16',1),(3,'Stew','2016-07-22 04:34:16','2016-07-22 04:34:16',1),(4,'Breuvages','2016-07-22 04:34:16','2016-07-22 04:34:16',1),(5,'Salades et legumes','2016-07-22 04:34:16','2016-07-22 04:34:16',1),(6,'Hors d\'oeuvres','2016-07-22 04:34:16','2016-07-22 04:34:16',1),(7,'Desserts','2016-07-22 04:34:16','2016-07-22 04:34:16',1),(8,'Produits cerealiers','2016-07-22 04:34:16','2016-07-22 04:34:16',1),(9,'Autres','2016-07-22 04:34:16','2016-07-22 04:34:16',1);
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `day_menu`
--

DROP TABLE IF EXISTS `day_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `day_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `day` datetime NOT NULL,
  `info` varchar(255) DEFAULT NULL,
  `diner` int(11) NOT NULL,
  `souper` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_valid` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `day_menu`
--

LOCK TABLES `day_menu` WRITE;
/*!40000 ALTER TABLE `day_menu` DISABLE KEYS */;
/*!40000 ALTER TABLE `day_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `location`
--

DROP TABLE IF EXISTS `location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_valid` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `location`
--

LOCK TABLES `location` WRITE;
/*!40000 ALTER TABLE `location` DISABLE KEYS */;
INSERT INTO `location` VALUES (1,'Recettes','2016-07-22 04:34:15','2016-07-22 04:34:15',1),(2,'Pol Martin','2016-07-22 04:34:15','2016-07-22 04:34:15',1),(3,'Classic Pasta','2016-07-22 04:34:15','2016-07-22 04:34:15',1),(4,'Autre','2016-07-22 04:34:15','2016-07-22 04:34:15',1);
/*!40000 ALTER TABLE `location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meal`
--

DROP TABLE IF EXISTS `meal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_valid` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meal`
--

LOCK TABLES `meal` WRITE;
/*!40000 ALTER TABLE `meal` DISABLE KEYS */;
INSERT INTO `meal` VALUES (1,'Dejeuner','2016-07-22 04:34:19','2016-07-22 04:34:19',1),(2,'Diner','2016-07-22 04:34:19','2016-07-22 04:34:19',1),(3,'Souper','2016-07-22 04:34:19','2016-07-22 04:34:19',1),(4,'Autre','2016-07-22 04:34:19','2016-07-22 04:34:19',1);
/*!40000 ALTER TABLE `meal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recipe`
--

DROP TABLE IF EXISTS `recipe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recipe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `type` int(11) NOT NULL,
  `location` int(11) NOT NULL,
  `season` int(11) NOT NULL,
  `meal` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_valid` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recipe`
--

LOCK TABLES `recipe` WRITE;
/*!40000 ALTER TABLE `recipe` DISABLE KEYS */;
INSERT INTO `recipe` VALUES (1,'Aubergines au parmesan',8,1,1,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(2,'Pates fresh tomato basil',4,1,1,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(3,'Pates four-star macaroni and cheese bake',5,1,6,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(4,'Pates margarita penne',4,1,1,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(5,'Pates alla cova',5,1,1,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(6,'Pates alla vodka',4,1,1,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(7,'Pates italie 2011',4,1,1,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(8,'Pates amatriciana',4,1,1,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(9,'Pates alfredo recettes',5,1,1,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(10,'Pates alfredo livre',5,3,1,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(11,'Pates aglio e olio',5,3,1,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(12,'Pates pesto',5,3,1,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(13,'Pates burro e pomodoro',4,3,1,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(14,'Pates pomodoro e basilico',4,3,1,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(15,'Pates primavera',5,3,1,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(16,'Pates bolognese',4,3,1,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(17,'Pates alfredo livre 2',5,3,1,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(18,'Pates carbonara',5,3,1,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(19,'Pates al cognac',5,3,1,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(20,'Pasta faggiolo',6,1,6,2,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(21,'Chili',6,1,6,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(22,'Taco',16,1,1,2,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(23,'Tourtiere',15,1,6,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(24,'Cabernet beef',2,1,6,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(25,'Red wine-flavoured beef',2,1,6,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(26,'Boeuf bourguignon',7,1,6,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(27,'Beef roast crockpot',7,1,6,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(28,'Crockpot BBQ meatballs',7,1,6,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(29,'Cheesy meatball skillet',7,1,1,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(30,'Meatballs in sweet and sour sauce',7,1,1,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(31,'Ragout de boulettes a Manon Robitaille',7,1,6,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(32,'Ragout de boulettes alla mama Marisa',7,1,6,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(33,'Family classic meatloaf',2,1,6,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(34,'Easy pleasing meatloaf',2,1,6,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(35,'BBQ-glazed mini meatloaf',2,1,6,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(36,'Rice and beef wrap',2,1,1,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(37,'Bisquick easy cheeseburger pie',15,1,6,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(38,'Quiche Lorraine',15,2,6,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(39,'Four cheese enchilada bake',15,1,6,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(40,'Barf Chicken - Cuisses de poulet au bacon',1,2,1,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(41,'Jambon de paques',11,1,6,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(42,'Baked ham with dijon mustard glaze',11,1,6,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(43,'Slow cooker honey garlic ham',11,1,6,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(44,'Marinade longe de porc sirop d\'erable',10,1,1,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(45,'Marinade porc Eric et Celine',10,1,1,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(46,'Marinade porc Alex Palumbo',10,1,1,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(47,'Porc tenderloin marinade cooks.com',10,1,1,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(48,'Porc tenderloin with apricot glaze',10,1,1,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(49,'Porc tenderloin slow cooker',10,1,6,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(50,'All-in-one pork chop bake',10,1,1,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(51,'Speedy saucy ribs',10,1,1,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(52,'Slow cooker BBQ short ribs',10,1,6,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(53,'Spare ribs in white beer sauce',10,1,6,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(54,'Sweet BBQ chicken kebobs',1,1,3,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(55,'Cashew chicken',1,1,1,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(56,'Tasty thai chicken',1,1,1,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(57,'Stir fry Marisa',1,1,1,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(58,'Easy everyday pot pie',15,1,6,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(59,'Marinade for chicken breasts',1,1,1,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(60,'Take-out orange chicken',1,1,1,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(61,'One-pan chicken and potato bake',1,1,6,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(62,'Poitrines de poulet aux champignons',1,1,1,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(63,'Honey-garlic slow cooker chicken thighs',1,1,1,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(64,'Sweet citrus chicken',1,1,1,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(65,'Slow cooker coq au vin',7,1,6,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(66,'So-Easy BBQ glazed chicken',1,1,3,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(67,'Cheesy chicken simmer',1,1,6,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(68,'One pot baked chicken',1,1,6,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(69,'Poulet pane',1,1,1,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(70,'Chicken fingers with BBQ sauce',1,1,1,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(71,'Parmesan crusted chicken',1,1,1,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(72,'Piccata al limone',13,1,1,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(73,'Veau pizzaiola',13,1,1,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(74,'Pate chinois',9,1,6,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(75,'Shakshouka',6,1,1,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(76,'Saucisses BBQ',12,1,3,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(77,'Steaks sur BBQ',2,1,3,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(78,'Veau pane',13,1,1,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(79,'Jambon poele',11,1,6,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(80,'Chicken wings',11,1,1,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(81,'Poulet entier achete',11,1,1,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(82,'Fajitas ou pitas',16,1,1,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(83,'Vols-au-vent',11,1,1,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(84,'Hot dog',16,1,1,2,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(85,'Hamburger',16,1,1,2,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(86,'Boulettes ikea',2,1,1,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(87,'Croque-monsieurs',16,1,1,2,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(88,'Sous-marins (froid ou chaud)',16,1,1,2,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(89,'Grilled cheese',16,1,1,2,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(90,'Fajita pizza',16,1,3,2,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(91,'Egg mcmuffin',16,1,1,2,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(92,'Western sandwich',16,1,1,2,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(93,'Omelettes',17,1,1,2,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(94,'Crepes jambon-fromage',16,1,1,2,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(95,'Jambon hache',16,1,1,2,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(96,'Kraft dinner',5,1,1,2,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(97,'Cannelloni',4,1,6,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(98,'Lasagne',4,1,6,3,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(99,'Soupe a l\'oignon',6,1,6,2,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(100,'Potage',6,1,6,2,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(101,'Stracciatella',6,1,6,2,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(102,'Salade verte maison',14,1,1,2,'2016-07-22 04:34:20','2016-07-22 04:34:20',1),(103,'Salade de choux',14,1,1,2,'2016-07-22 04:34:20','2016-07-22 04:34:20',1);
/*!40000 ALTER TABLE `recipe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `season`
--

DROP TABLE IF EXISTS `season`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `season` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(50) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_valid` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `season`
--

LOCK TABLES `season` WRITE;
/*!40000 ALTER TABLE `season` DISABLE KEYS */;
INSERT INTO `season` VALUES (1,'All','2016-07-22 04:34:18','2016-07-22 04:34:18',1),(2,'Hiver','2016-07-22 04:34:18','2016-07-22 04:34:18',1),(3,'Ete','2016-07-22 04:34:18','2016-07-22 04:34:18',1),(4,'Automne','2016-07-22 04:34:18','2016-07-22 04:34:18',1),(5,'Printemps','2016-07-22 04:34:18','2016-07-22 04:34:18',1),(6,'Automne-Hiver','2016-07-22 04:34:18','2016-07-22 04:34:18',1),(7,'Autres','2016-07-22 04:34:18','2016-07-22 04:34:18',1);
/*!40000 ALTER TABLE `season` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type`
--

DROP TABLE IF EXISTS `type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(255) NOT NULL,
  `category` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_valid` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type`
--

LOCK TABLES `type` WRITE;
/*!40000 ALTER TABLE `type` DISABLE KEYS */;
INSERT INTO `type` VALUES (1,'Poulet',1,'2016-07-22 04:34:17','2016-07-22 04:34:17',1),(2,'Boeuf',1,'2016-07-22 04:34:17','2016-07-22 04:34:17',1),(3,'Poisson',1,'2016-07-22 04:34:17','2016-07-22 04:34:17',1),(4,'Sauces rouges',2,'2016-07-22 04:34:17','2016-07-22 04:34:17',1),(5,'Sauces blanches',2,'2016-07-22 04:34:17','2016-07-22 04:34:17',1),(6,'Soupe',3,'2016-07-22 04:34:17','2016-07-22 04:34:17',1),(7,'Ragout',3,'2016-07-22 04:34:17','2016-07-22 04:34:17',1),(8,'Legumes',5,'2016-07-22 04:34:17','2016-07-22 04:34:17',1),(9,'Boeuf hache',1,'2016-07-22 04:34:17','2016-07-22 04:34:17',1),(10,'Porc',1,'2016-07-22 04:34:17','2016-07-22 04:34:17',1),(11,'Jambon',1,'2016-07-22 04:34:17','2016-07-22 04:34:17',1),(12,'Saucisses',1,'2016-07-22 04:34:17','2016-07-22 04:34:17',1),(13,'Veau',1,'2016-07-22 04:34:17','2016-07-22 04:34:17',1),(14,'Salade',5,'2016-07-22 04:34:17','2016-07-22 04:34:17',1),(15,'Tartes',8,'2016-07-22 04:34:17','2016-07-22 04:34:17',1),(16,'Wraps et sandwichs',8,'2016-07-22 04:34:17','2016-07-22 04:34:17',1),(17,'Autres',NULL,'2016-07-22 04:34:17','2016-07-22 04:34:17',1);
/*!40000 ALTER TABLE `type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(60) NOT NULL,
  `role` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_valid` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin@admin.ca','$2a$10$Cc6vgWYuob3lotWFxnq12ewzQhTN9jbrh9Eoa3ueXVtiz9SWJopoe',2,'2016-07-22 04:34:22','2016-07-22 04:34:22',1),(2,'user1@user1.ca','$2a$10$Cc6vgWYuob3lotWFxnq12ewzQhTN9jbrh9Eoa3ueXVtiz9SWJopoe',1,'2016-07-22 04:34:22','2016-07-22 04:34:22',1);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(45) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_valid` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_role`
--

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` VALUES (1,'ROLE_USER','2016-07-22 04:34:21','2016-07-22 04:34:21',1),(2,'ROLE_ADMIN','2016-07-22 04:34:21','2016-07-22 04:34:21',1);
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `week_menu`
--

DROP TABLE IF EXISTS `week_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `week_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_from` datetime NOT NULL,
  `date_to` datetime NOT NULL,
  `day1` int(11) NOT NULL,
  `day2` int(11) NOT NULL,
  `day3` int(11) NOT NULL,
  `day4` int(11) NOT NULL,
  `day5` int(11) NOT NULL,
  `day6` int(11) NOT NULL,
  `day7` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_valid` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `week_menu`
--

LOCK TABLES `week_menu` WRITE;
/*!40000 ALTER TABLE `week_menu` DISABLE KEYS */;
/*!40000 ALTER TABLE `week_menu` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-07-22  0:35:48
