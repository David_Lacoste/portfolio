-- Drop database
DROP DATABASE IF EXISTS lunch_scheduler_web;
CREATE DATABASE lunch_scheduler_web;
USE lunch_scheduler_web;

-- Drop tables
SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS recipe;
DROP TABLE IF EXISTS meal;
DROP TABLE IF EXISTS category;
DROP TABLE IF EXISTS type;
DROP TABLE IF EXISTS prep_time; -- old table
DROP TABLE IF EXISTS location;
DROP TABLE IF EXISTS season;
DROP TABLE IF EXISTS day_menu;
DROP TABLE IF EXISTS week_menu;
DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS user_role;
SET FOREIGN_KEY_CHECKS=1;

-- Create role
CREATE TABLE user_role(
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	role VARCHAR(45) NOT NULL,
	created_at TIMESTAMP NULL DEFAULT NULL,
	updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	is_valid BOOLEAN NOT NULL
);

-- Create user
CREATE TABLE user(
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	username VARCHAR(45) NOT NULL,
	password VARCHAR(60) NOT NULL,
	role INT NOT NULL REFERENCES user_role(id),
	created_at TIMESTAMP NULL DEFAULT NULL,
	updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	is_valid BOOLEAN NOT NULL
);

-- Create season
CREATE TABLE season(
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	value VARCHAR(50) NOT NULL,
	created_at TIMESTAMP NULL DEFAULT NULL,
	updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	is_valid BOOLEAN NOT NULL
);

-- Create location
CREATE TABLE location(
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	value VARCHAR(255) NOT NULL,
	created_at TIMESTAMP NULL DEFAULT NULL,
	updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	is_valid BOOLEAN NOT NULL
);

-- Create category
CREATE TABLE category(
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	value VARCHAR(50) NOT NULL,
	created_at TIMESTAMP NULL DEFAULT NULL,
	updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	is_valid BOOLEAN NOT NULL
);

-- Create type
CREATE TABLE type(
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	value VARCHAR(255) NOT NULL,
	category INT REFERENCES category(id),
	created_at TIMESTAMP NULL DEFAULT NULL,
	updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	is_valid BOOLEAN NOT NULL
);

-- Create meal
CREATE TABLE meal(
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	value VARCHAR(255) NOT NULL,
	created_at TIMESTAMP NULL DEFAULT NULL,
	updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	is_valid BOOLEAN NOT NULL
);

-- Create recipe
CREATE TABLE recipe(
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(255) NOT NULL,
	type INT NOT NULL REFERENCES type(id),
	location INT NOT NULL REFERENCES location(id),
	season INT NOT NULL REFERENCES season(id),
	meal INT NOT NULL REFERENCES meal(id),
	created_at TIMESTAMP NULL DEFAULT NULL,
	updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	is_valid BOOLEAN NOT NULL
);

-- Create day_menu
CREATE TABLE day_menu(
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	day DATETIME NOT NULL,
	info VARCHAR(255),
	diner INT NOT NULL REFERENCES recipe(id),
	souper INT NOT NULL REFERENCES recipe(id),
	created_at TIMESTAMP NULL DEFAULT NULL,
	updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	is_valid BOOLEAN NOT NULL
);

-- Create week_menu
CREATE TABLE week_menu(
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	date_from DATETIME NOT NULL,
	date_to DATETIME NOT NULL,
	day1 INT NOT NULL REFERENCES day_menu(id),
	day2 INT NOT NULL REFERENCES day_menu(id),
	day3 INT NOT NULL REFERENCES day_menu(id),
	day4 INT NOT NULL REFERENCES day_menu(id),
	day5 INT NOT NULL REFERENCES day_menu(id),
	day6 INT NOT NULL REFERENCES day_menu(id),
	day7 INT NOT NULL REFERENCES day_menu(id),
	created_at TIMESTAMP NULL DEFAULT NULL,
	updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	is_valid BOOLEAN NOT NULL
);

-- Add data into location table
-- Must use '\r' instead of '\n' because it is how Excel created the file
LOAD DATA INFILE '/Users/david/projects/workspace/lunchschedulegenerator/scripts/sql/csv/locations.csv'
INTO TABLE location
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\r'
(id, value, @created_at, @updated_at, is_valid)
SET created_at = now(), updated_at = now();

-- Add data into category table
LOAD DATA INFILE '/Users/david/projects/workspace/lunchschedulegenerator/scripts/sql/csv/categories.csv'
INTO TABLE category
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\r'
(id, value, @created_at, @updated_at, is_valid)
SET created_at = now(), updated_at = now();

-- Add data into type table
LOAD DATA INFILE '/Users/david/projects/workspace/lunchschedulegenerator/scripts/sql/csv/types.csv'
INTO TABLE type
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\r'
(id, value, category, @created_at, @updated_at, is_valid)
SET created_at = now(), updated_at = now();

-- Add data into season table
LOAD DATA INFILE '/Users/david/projects/workspace/lunchschedulegenerator/scripts/sql/csv/seasons.csv'
INTO TABLE season
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\r'
(id, value, @created_at, @updated_at, is_valid)
SET created_at = now(), updated_at = now();

-- Add data into meal table
LOAD DATA INFILE '/Users/david/projects/workspace/lunchschedulegenerator/scripts/sql/csv/meals.csv'
INTO TABLE meal
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\r'
(id, value, @created_at, @updated_at, is_valid)
SET created_at = now(), updated_at = now();

-- Add data into recipes table
LOAD DATA INFILE '/Users/david/projects/workspace/lunchschedulegenerator/scripts/sql/csv/recipes.csv'
INTO TABLE recipe
FIELDS TERMINATED BY ','
LINES TERMINATED BY '\r'
(id, name, type, location, season, meal, @created_at, @updated_at, is_valid)
SET created_at = now(), updated_at = now();

-- Add data into user_role table
INSERT INTO user_role(role, created_at, updated_at, is_valid)
VALUES ("ROLE_USER", now(), now(), true), ("ROLE_ADMIN", now(), now(), true);

-- Add date into user table
-- NOTE: The hashed password for default users is "123456"
INSERT INTO user(username, password, role, created_at, updated_at, is_valid)
VALUES 
	("admin@admin.ca", "$2a$10$Cc6vgWYuob3lotWFxnq12ewzQhTN9jbrh9Eoa3ueXVtiz9SWJopoe", 2, now(), now(), true),
	("user1@user1.ca", "$2a$10$Cc6vgWYuob3lotWFxnq12ewzQhTN9jbrh9Eoa3ueXVtiz9SWJopoe", 1, now(), now(), true);
