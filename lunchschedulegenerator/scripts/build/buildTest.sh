#! /bin/bash

# move to the js directory
cd ../../src/main/webapp/resources/js

# run JS preprocessing
echo Start JS preprocessing
preprocess lunch-schedule-generator.js . -ENV=test > test/lunch-schedule-generator.js

# copy to ref
preprocess lunch-schedule-generator.js . -ENV=test > ref/lunch-schedule-generator.js
echo End JS preprocessing

# move to the root of repo
cd ../../../../..

# run XML preprocessing
echo Start XML preprocessing
m4 -Dtest src/main/webapp/resources/pre/spring-database-pre.xml > src/main/webapp/WEB-INF/spring-database.xml
m4 -Dtest src/main/webapp/resources/pre/hibernate.cfg-pre.xml > src/main/resources/hibernate.cfg.xml
echo End XML preprocessing
