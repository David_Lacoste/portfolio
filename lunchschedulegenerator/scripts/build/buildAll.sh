#! /bin/bash

# This file does not preprocess XML

# move to the js directory
cd ../../src/main/webapp/resources/js

# run JS preprocessing for test env
preprocess lunch-schedule-generator.js . -ENV=test > test/lunch-schedule-generator.js

# run JS preprocessing for prod env
preprocess lunch-schedule-generator.js . -ENV=prod > prod/lunch-schedule-generator.js
