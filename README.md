David Lacoste's Portfolio
==============

This is the repository for my personal projects.

Projects
--------------

* **Weighted Grade Calculator:** Java-based application for students that calculates a final weighted course mark.
* **IdeaPedia:** PHP/Codeigniter web application allowing users to submit ideas for start-up companies. This was a school project for CSC309 Web Development.
* **UIExperiment:** Hybrid mobile application using the Ionic framework built for a CHI research experiment to calculate typing speed and accuracy.
* **uiexperimentrest:** Ruby/Rails web application to serve as a RESTful API to be consumed by the UIExperiment mobile application listed above.

**NOTE:** Both the "UIExperiment" and the "uiexperimentrest" projects were build as a school project for CSC428 Human-Computer Interaction.

Installation
--------------

### Weighted Grade Calculator Installation

**Prerequisites:**

* Apache ANT
* JRE 6 or later
* OS: UNIX-based OS is required if running the launch script
* Assumes ANT and the JRE are configured on the PATH

**Install:**

Assuming that you are in the root of the repo, build the project with ANT:

    cd WeightedGradeCalculator
    ant -buildfile build.xml

**Launch:**

Launch the application through the terminal via:

    cd dist
    ./run.sh

---

### IdeaPedia Installation

**Prerequisites:**

* Apache HTTP Server 2.4
* PHP 5
* MySQL 14
* OS: Assumes a UNIX-based OS is installed

**Install:**

Assuming that you are in the root of the repo, run the SQL scripts to set up the database:

    cd ideaPedia

    # create the database
    mysql -u {USERNAME} -p < scripts/ipProductionSetup.sql
    
    # create the database user used by the application
    mysql -u {USERNAME} -p < scripts/createDbUser.sql

Move the application into the apache root directory:

    # move to the root of the repo
    cd ..
    
    # move the application to the apache root directory
    mv ideaPedia {APACHE ROOT}

**Launch:**

Launch the application through a web browser and going to <http://localhost/ideaPedia>.

By default, the application is installed with a default user and a default admin with the following credentials:

Default User:

* username: tmp@tmp.com
* password: 123

Default Admin:

* username: admin@admin.com
* password: 123

___

### UIExperiment Installation

**Prerequisites:**

* Ionic Framework 1.2.4
* Apache Cordova 5.3.3
* OS: Assumes a UNIX-based OS is installed
* Assumes Ionic is configured to work with Android and iOS emulators

**Install:**

Assuming that you are in the root of the repo, add the android and iOS platforms:

    # change into the directory
    cd UIExperiment
    
    # add the android platform
    ionic platform add android

    # add the iOS platform
    ionic platform add ios

Build the project for the respective platforms:

    # build the android app
    ionic build android

    # build the iOS app
    ionic build ios

**Launch:**

Platform's emulator:

    # android emulator
    ionic emulate android

    # ios emulator
    ionic emulate ios

Web browser:

    # ionic server
    ionic serve

To deploy the application to a physical device:

    # android device
    ionic run android

    # ios device
    ionic run ios

Developers should review the Apple licensing documentation before deploying the application onto Apple devices.

___

### Uiexperimentrest Installation

**Prerequisites:**

* Ruby 4.2.5 with bundler (1.1.12) and rake (11.1.1) gems
* PostgreSQL 9.4
* OS: Assumes a UNIX-based OS is installed

**Install:**

Assuming you are in the root of the repo, run the following to install the necessary gems:

    # change into the directory
    cd uiexperimentrest

    # install the necessary gems
    bundle install

Run the SQL scripts to set up the database:

    # create the database
    psql -U {USERNAME} -a -f scripts/uiexperimentSetup.sql

    # create the database user used by the application
    psql -U {USERNAME} -a -f scripts/createDbUser.sql

Run a database migration to set up the tables in the database:

    rake db:migrate

**Launch:**

Start the server by running the following:

    rails s -p 3000

Launch the application through a web browser by and going to <http://localhost:3000/>.

Whenever you wish to stop the server, cancel the running process in the terminal window with the following:

* **Mac OS X:** CTRL + C
* **Linux:** CTRL + C

**NOTE:** For Linux users, it is best to check with the distribution's documentation about how to kill a running process in a terminal window.

Documentation
--------------

The documentation for each specific project will be contained in the project's {ROOT}/docs directory.

Contact
--------------

If you have questions, please email me at davidlacoste3@gmail.com

License
--------------

All projects in my portfolio are released under the GPLv3 license.

A copy of the license can be found at the root of the repo.