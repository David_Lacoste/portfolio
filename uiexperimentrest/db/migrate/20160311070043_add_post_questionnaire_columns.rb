class AddPostQuestionnaireColumns < ActiveRecord::Migration
  def self.up
  	add_column :participants, :mental_demand, :integer
  	add_column :participants, :physical_demand, :integer
  	add_column :participants, :temporal_demand, :integer
  	add_column :participants, :performance, :integer
  	add_column :participants, :effort, :integer
  	add_column :participants, :frustration, :integer
  end

  def self.down
  	remove_column :participants, :mental_demand
  	remove_column :participants, :physical_demand
  	remove_column :participants, :temporal_demand
  	remove_column :participants, :performance
  	remove_column :participants, :effort
  	remove_column :participants, :frustration
  end
end
