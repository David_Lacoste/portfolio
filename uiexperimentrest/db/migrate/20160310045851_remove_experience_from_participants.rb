class RemoveExperienceFromParticipants < ActiveRecord::Migration
  def change
  	remove_column :participants, :experience
  end
end
