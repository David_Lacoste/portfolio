class CreateParticipant < ActiveRecord::Migration
  def change
    create_table :participants do |t|

    	t.column 	:correct_entry_pct, 	:float,     null: false
    	t.column 	:wpm, 	:float,   null: false

    	t.timestamps
    end
  end
end
