class AddParticipantInfo < ActiveRecord::Migration
  
  def self.up
  	add_column :participants, :input_type, :integer
  	add_column :participants, :gender, :integer
  	add_column :participants, :age, :integer
  	add_column :participants, :experience, :integer
  end

  def self.down
  	remove_column :participants, :input_type
  	remove_column :participants, :gender
  	remove_column :participants, :age
  	remove_column :participants, :experience
  end

end
