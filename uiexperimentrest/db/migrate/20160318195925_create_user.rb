class CreateUser < ActiveRecord::Migration
  def change
    create_table :users do |t|

    	t.column 	:first_name, 	:string, 	null: false
    	t.column 	:last_name, 	:string, 	null: false

    	t.timestamps
    end
  end
end
