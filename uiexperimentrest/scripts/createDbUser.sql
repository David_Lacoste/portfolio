-- create the user
CREATE USER csc428 WITH PASSWORD 'bahenCentre';

-- grant privileges to database
GRANT ALL PRIVILEGES ON DATABASE "uiexperiment" to csc428;