class UserSerializer < ActiveModel::Serializer

	attributes :id,
	:first_name, 
	:last_name, 
	:created_at, 
	:updated_at

	def first_name

		# create the encryptor object to hash the strings
		encryptor = Encryptor.new("my_secret_key", "my_secret_salt")

		if object.first_name.present?
			encryptor.decrypt(User.find_by_id(object.id).first_name)
		else
			"Error"
		end
	end

	def last_name

		# create the encryptor object to hash the strings
		encryptor = Encryptor.new("my_secret_key", "my_secret_salt")

		if object.last_name.present?
			encryptor.decrypt(User.find_by_id(object.id).last_name)
		else
			"Error"
		end
	end
end