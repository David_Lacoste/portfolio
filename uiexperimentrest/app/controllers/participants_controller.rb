class ParticipantsController < ApplicationController

	skip_before_filter :verify_authenticity_token
	
	respond_to :json

	# GET all participants
	def index

		# return all participants
		@participants = Participant.all.order(:id)

		respond_to do |format|
			format.html
			format.json { render json: @participants }
		end
	end

	# POST call to create a new participant
	# data: {"input_type": 1, "user_id": 1, "gender": 1, "age": 1, "correct_entry_pct": 95.23, "wpm": 75.3, "mental_demand": 1, "physical_demand": 1, "temporal_demand": 1, "performance": 1, "effort": 1, "frustration": 1}
	def create

		@participant = Participant.new

		# add parameters to the record before saving it
		@participant.input_type = params[:input_type]
		@participant.user_id = params[:user_id]
		@participant.gender = params[:gender]
		@participant.age = params[:age]
		@participant.correct_entry_pct = params[:correct_entry_pct]
		@participant.wpm = params[:wpm]
		@participant.mental_demand = params[:mental_demand]
		@participant.physical_demand = params[:physical_demand]
		@participant.temporal_demand = params[:temporal_demand]
		@participant.performance = params[:performance]
		@participant.effort = params[:effort]
		@participant.frustration = params[:frustration]

		if @participant.save
			render json: { "message" => "It worked", "code" => 1 }
		else
			render json: { "message" => "It did not work", "code" => -1 }
		end
	end

	# POST call to get the last id available for use
	# http://localhost:3000/participants/check_id_num
	def check_id_num
		
		@last_part_id = Participant.maximum(:id)

		render json: { "result" => @last_part_id }
	end

	# DELETE call to delete a participant
	# http://localhost:3000/participants/{id}
	def destroy

		record = Participant.find(params[:id])
		
		if record.destroy
			render json: { "message" => "It worked", "code" => 1 }
		else
			render json: { "message" => "It did not work", "code" => -1 }
		end
	end

	# PATCH call to update a participant
	# http://localhost:3000/participants/{id}
	# data: {"input_type": 1, "user_id": 1, "gender": 1, "age": 1, "correct_entry_pct": 95.23, "wpm": 75.3, "mental_demand": 1, "physical_demand": 1, "temporal_demand": 1, "performance": 1, "effort": 1, "frustration": 1}
	def update

		record = Participant.find(params[:id])

		# update all the record info
		record.update(input_type: params[:input_type])
		record.update(user_id: params[:user_id])
		record.update(gender: params[:gender])
		record.update(age: params[:age])
		record.update(correct_entry_pct: params[:correct_entry_pct])
		record.update(wpm: params[:wpm])
		record.update(mental_demand: params[:mental_demand])
		record.update(physical_demand: params[:physical_demand])
		record.update(temporal_demand: params[:temporal_demand])
		record.update(performance: params[:performance])
		record.update(effort: params[:effort])
		record.update(frustration: params[:frustration])

		if record.save
			render json: { "message" => "It worked", "code" => 1, "payload" => record }
		else
			render json: { "message" => "It did not work", "code" => -1 }
		end
	end

end