class UsersController < ApplicationController

	skip_before_filter :verify_authenticity_token
	
	respond_to :json

	# GET all users
	# http://localhost:3000/users/
	# NOTE: The user information will be returned as encrypted
	def index

		# return all users
		@users = User.all.order(:id)

		respond_to do |format|
			format.html
			format.json { render json: { "message" => "It worked", "payload" => @users } }
		end
	end


	# POST call to create a new user
	# data: {"first_name": "David", "last_name": "Lacoste"}
	def create

		# create the encryptor object to hash the strings
		# NOTE: Should not hard-code the key and salt
		encryptor = Encryptor.new("my_secret_key", "my_secret_salt")

		# create the new user record
		@user = User.new

		# add parameters to the record before saving it
		@user.first_name = encryptor.encrypt(params[:first_name])
		@user.last_name = encryptor.encrypt(params[:last_name])

		if @user.save

			# JSON objects within an array
			users = ActiveModel::ArraySerializer.new(User.all, each_serializer: UserSerializer).as_json
			found_user = nil

			users.each do |user|

				if user[:first_name].eql? params[:first_name] and user[:last_name].eql? params[:last_name]
					found_user = user
				else
					found_user = nil
				end

			end

			if found_user.eql? nil
				render json: { "message" => "It did not work", "code" => -1 }
			else
				render json: { "message" => "It worked", "code" => 1, "payload" => found_user }
			end
		else
			render json: { "message" => "User was not persisted to the database", "code" => -2 }
		end
	end

	# GET call to get a specific user
	# http://localhost:3000/users/1
	def show

		# return a specific user with param "id"
		@user = User.where(id: params[:id])

		result = ActiveModel::ArraySerializer.new(@user, each_serializer: UserSerializer)

		render json: { "message" => "It worked", "code" => 1, "payload" => result }

	end

	# POST call to get a user given a first and/or last name
	# http://localhost:3000/users/get_info_by_name
	# data: {"first_name": "David", "last_name": "Lacoste"}
	def get_info_by_name

		# JSON objects within an array
		users = ActiveModel::ArraySerializer.new(User.all, each_serializer: UserSerializer).as_json
		valid_users = Array.new()
		found_user = nil

		if not params[:first_name].eql? "" and not params[:last_name].eql? ""
			# Case: "David" "Lacoste"

			users.each do |user|
				if user[:first_name].eql? params[:first_name] and user[:last_name].eql? params[:last_name]
					# add user to valid_users array
					valid_users << user
				end
			end

		elsif not params[:first_name].eql? "" and params[:last_name].eql? ""
			# Case: "David" ""

			users.each do |user|
				if user[:first_name].eql? params[:first_name]
					# add user to valid_users array
					valid_users << user
				end
			end

		elsif params[:first_name].eql? "" and not params[:last_name].eql? ""
			# Case: "" "Lacoste"

			users.each do |user|
				if user[:last_name].eql? params[:last_name]
					# add user to valid_users array
					valid_users << user
				end
			end
		end

		result = {}
		result["valid_users"] = valid_users.as_json

		render json: { "message" => "It worked", "code" => 1, "payload" => result }
	end

	# POST call to get the next available id in the database
	# http://localhost:3000/users/get_next_available_id
	def get_next_available_id

		value = User.maximum(:id).next

		# check that result has a value
		if not (defined?(value)).nil?

			result = {}
			result["value"] = value

			render json: { "message" => "It worked", "code" => 1, "payload" => result }
		else
			render json: { "message" => "It did not work", "code" => -1 }
		end
	end

end