/**
 * An item in a course.
 */

package com.david.calc;

public class Item {
	
	String name;
	double grade;
	double weight;
	double weightedGrade;
	
	public Item(String name, double grade, double weight) {
		this.name = name;
		this.grade = grade;
		this.weight = weight;
		this.weightedGrade= this.calculateWeightedGrade(grade, weight);
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public double getGrade() {
		return this.grade;
	}
	
	public void setGrade(int grade) {
		this.grade = grade;
	}
	
	public double getWeight() {
		return this.weight;
	}
	
	public void setWeight(int weight) {
		this.weight = weight;
	}
	
	public double getWeightedGrade() {
		return this.weightedGrade;
	}
	
	public void setWeightedGrade(double weightedGrade) {
		this.weightedGrade = weightedGrade;
	}
	
	/**
	 * Calculates the weighted grade between for the item.
	 * 
	 * In practice, this should be used on the "grade" and the "weight"
	 * parameters that are passed into the object upon creation.
	 */
	public double calculateWeightedGrade(double num1, double num2) {
		
		return num1 * num2;
	}
	
}
