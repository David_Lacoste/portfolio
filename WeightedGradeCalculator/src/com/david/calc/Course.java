/**
 * A course which contains items.
 */

package com.david.calc;

import java.util.ArrayList;

public class Course {
	
	String name;
	ArrayList<Item> itemList = new ArrayList<Item>();
	long totalWeightCalc;
	long finalMark;
	
	public Course(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public ArrayList<Item> getItemList() {
		return this.itemList;
	}
	
	public void setItemList(ArrayList<Item> itemList) {
		this.itemList = itemList;
	}
	
	public long getTotalWeightCalc() {
		return this.totalWeightCalc;
	}
	
	public void setTotalWeightCalc(long totalWeightCalc) {
		this.totalWeightCalc = totalWeightCalc;
	}
	
	public long getFinalMark() {
		return this.finalMark;
	}
	
	public void setFinalMark(long finalMark) {
		this.finalMark = finalMark;
	}
	
	/**
	 * Empty all Items from the itemList.
	 */
	public void emptyList() {
		ArrayList<Item> tempList = new ArrayList<Item>();
		this.itemList = tempList;
	}
	
	/**
	 * Adds an Item to Course.itemList
	 */
	public void addItem(Item toAddItem) {
		itemList.add(itemList.size(), toAddItem);
	}
	
	/**
	 * The sum of all the course weights.
	 */
	public double calculateTotalWeight() {
		
		double sum = 0;
		Item currItem;
		
		for (int i = 0; i < this.itemList.size(); i++) {
			currItem = this.itemList.get(i);
			sum = sum + currItem.getWeight();
		}
		
		return sum;
	}
	
	/**
	 * Calculate the final mark in the course
	 */
	public double calculateFinalMark() {
		
		double sum = 0;
		Item currItem;
		
		for (int i = 0; i < this.itemList.size(); i++) {
			currItem = this.itemList.get(i);
			sum = sum + currItem.getWeightedGrade();
		}
		
		return sum;
	}
	
}
