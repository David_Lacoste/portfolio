/**
 * The GUI for the application
 */

package com.david.ui;

import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import com.david.calc.Course;
import com.david.calc.Item;
import com.david.ui.table.MyTableModel;
import com.david.ui.table.TableRow;

public class GUI {
	
	// --- CONSTANTS --- //
	
	// FONTS
	private final Font DEFAULT_FONT = new JLabel().getFont();
	private final Font TITLE_FONT = new Font(DEFAULT_FONT.getFontName(), DEFAULT_FONT.getStyle(), DEFAULT_FONT.getSize() + 8);
	
	// LOGO
	private final String LOGO_PATH = Messages.getString("GUI.logoPath"); //$NON-NLS-1$
	private final String LOGO_SMALL_PATH = Messages.getString("GUI.logoSmallPath"); //$NON-NLS-1$
	private final ImageIcon LOGO = new ImageIcon(LOGO_PATH);
	private final ImageIcon LOGO_SMALL = new ImageIcon(LOGO_SMALL_PATH);
	
	// --- GUI COMPONENTS --- //
	
	// FRAMES
	private JFrame frame = new JFrame(Messages.getString("GUI.frameTitle")); //$NON-NLS-1$
	
	// PANELS
	private JPanel mainPanel = new JPanel(new GridBagLayout());
	private JPanel secondRowPanel = new JPanel(new FlowLayout(FlowLayout.LEFT)); // this is used for subTitleLabel1 and textBox1
	private JPanel tablePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
	private JPanel fourthRowPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
	private JPanel containerPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
	private JPanel resultsPanel = new JPanel(new GridLayout(0,2));
	
	// DIALOGS
	private JOptionPane errorDialog = new JOptionPane();
	private JOptionPane addItemDialog = new JOptionPane();
	
	// LABELS
	private JLabel titleLabel = new JLabel(Messages.getString("GUI.titleLabel")); //$NON-NLS-1$
	private JLabel subTitleLabel1 = new JLabel(Messages.getString("GUI.subTitleLabel1")); //$NON-NLS-1$
	private JLabel subTitleLabel2 = new JLabel(Messages.getString("GUI.subTitleLabel2")); //$NON-NLS-1$
	private JLabel subTitleLabel3 = new JLabel(Messages.getString("GUI.subTitleLabel3")); //$NON-NLS-1$
	private JLabel subTitleLabel4 = new JLabel(Messages.getString("GUI.subTitleLabel4")); //$NON-NLS-1$
	private JLabel subTitleLabel5 = new JLabel(Messages.getString("GUI.subTitleLabel5")); //$NON-NLS-1$
	private JLabel logoImageLabel = new JLabel(LOGO_SMALL);
	
	// BUTTONS
	private JButton button1 = new JButton(Messages.getString("GUI.button1Title")); //$NON-NLS-1$
	private JButton button2 = new JButton(Messages.getString("GUI.button2Title")); //$NON-NLS-1$
	private JButton button3 = new JButton(Messages.getString("GUI.button3Title")); //$NON-NLS-1$
	
	// TEXT BOXES
	private JTextField textBox1 = new JTextField(20);
	
	// TABLE COMPONENTS
	private MyTableModel model = new MyTableModel();
	private JTable table = new JTable(model);
	
	// COURSE COMPONENTS
	Course course = new Course(""); //$NON-NLS-1$
	
	public GUI() {
		this.addComponents();
		this.addListeners();
	}

	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

	public JPanel getMainPanel() {
		return mainPanel;
	}

	public void setMainPanel(JPanel mainPanel) {
		this.mainPanel = mainPanel;
	}

	public JPanel getSecondRowPanel() {
		return secondRowPanel;
	}

	public void setSecondRowPanel(JPanel secondRowPanel) {
		this.secondRowPanel = secondRowPanel;
	}

	public JPanel getTablePanel() {
		return tablePanel;
	}

	public void setTablePanel(JPanel tablePanel) {
		this.tablePanel = tablePanel;
	}

	public JPanel getFourthRowPanel() {
		return fourthRowPanel;
	}

	public void setFourthRowPanel(JPanel fourthRowPanel) {
		this.fourthRowPanel = fourthRowPanel;
	}

	public JPanel getContainerPanel() {
		return containerPanel;
	}

	public void setContainerPanel(JPanel containerPanel) {
		this.containerPanel = containerPanel;
	}

	public JPanel getResultsPanel() {
		return resultsPanel;
	}

	public void setResultsPanel(JPanel resultsPanel) {
		this.resultsPanel = resultsPanel;
	}

	public JOptionPane getErrorDialog() {
		return errorDialog;
	}

	public void setErrorDialog(JOptionPane errorDialog) {
		this.errorDialog = errorDialog;
	}

	public JOptionPane getAddItemDialog() {
		return addItemDialog;
	}

	public void setAddItemDialog(JOptionPane addItemDialog) {
		this.addItemDialog = addItemDialog;
	}

	public JLabel getTitleLabel() {
		return titleLabel;
	}

	public void setTitleLabel(JLabel titleLabel) {
		this.titleLabel = titleLabel;
	}

	public JLabel getSubTitleLabel1() {
		return subTitleLabel1;
	}

	public void setSubTitleLabel1(JLabel subTitleLabel1) {
		this.subTitleLabel1 = subTitleLabel1;
	}

	public JLabel getSubTitleLabel2() {
		return subTitleLabel2;
	}

	public void setSubTitleLabel2(JLabel subTitleLabel2) {
		this.subTitleLabel2 = subTitleLabel2;
	}

	public JLabel getSubTitleLabel3() {
		return subTitleLabel3;
	}

	public void setSubTitleLabel3(JLabel subTitleLabel3) {
		this.subTitleLabel3 = subTitleLabel3;
	}

	public JLabel getSubTitleLabel4() {
		return subTitleLabel4;
	}

	public void setSubTitleLabel4(JLabel subTitleLabel4) {
		this.subTitleLabel4 = subTitleLabel4;
	}

	public JLabel getSubTitleLabel5() {
		return subTitleLabel5;
	}

	public void setSubTitleLabel5(JLabel subTitleLabel5) {
		this.subTitleLabel5 = subTitleLabel5;
	}

	public JLabel getLogoImageLabel() {
		return logoImageLabel;
	}

	public void setLogoImageLabel(JLabel logoImageLabel) {
		this.logoImageLabel = logoImageLabel;
	}

	public JButton getButton1() {
		return button1;
	}

	public void setButton1(JButton button1) {
		this.button1 = button1;
	}

	public JButton getButton2() {
		return button2;
	}

	public void setButton2(JButton button2) {
		this.button2 = button2;
	}

	public JButton getButton3() {
		return button3;
	}

	public void setButton3(JButton button3) {
		this.button3 = button3;
	}

	public JTextField getTextBox1() {
		return textBox1;
	}

	public void setTextBox1(JTextField textBox1) {
		this.textBox1 = textBox1;
	}

	public MyTableModel getModel() {
		return model;
	}

	public void setModel(MyTableModel model) {
		this.model = model;
	}

	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public Font getDEFAULT_FONT() {
		return DEFAULT_FONT;
	}

	public Font getTITLE_FONT() {
		return TITLE_FONT;
	}

	public String getLOGO_PATH() {
		return LOGO_PATH;
	}

	public String getLOGO_SMALL_PATH() {
		return LOGO_SMALL_PATH;
	}

	public ImageIcon getLOGO() {
		return LOGO;
	}

	public ImageIcon getLOGO_SMALL() {
		return LOGO_SMALL;
	}

	/**
	 * Create the layout and add all components to the GUI
	 */
	private void addComponents() {
		
		// configure frame1
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setIconImage(LOGO.getImage()); // set the window icon
		
		// configure the layout
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		
		// --- APPLICATION LOGO --- //
		
		c.gridx = 2;
		c.gridy = 0;
		mainPanel.add(logoImageLabel, c);
		
		// --- TITLES AND SUBTITLES --- //
		
		// title
		titleLabel.setFont(TITLE_FONT);
		c.insets.bottom = 10;
		c.gridx = 0;
		c.gridy = 0;
		mainPanel.add(titleLabel, c);
		
		c.gridwidth = 0; // reset to zero
		c.insets.bottom = 0; // reset to zero
		
		// subtitle 1
		secondRowPanel.add(subTitleLabel1);
		
		// --- TEXT BOXES --- //
		
		secondRowPanel.add(textBox1);
		
		// --- BUTTONS --- //
		
		// button 2
		fourthRowPanel.add(button2);
		
		// TODO: Add button1 and button3 in future release
		
		// --- BASE TABLE --- //
		
		// make scrollpane the same size as table
		this.table.setPreferredScrollableViewportSize(table.getPreferredSize());
		
		// make the table vertically scrollable
		JScrollPane scrollPane = new JScrollPane(this.table);
		
		// add the horizontal and vertical scrollbars
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		
		//tablePanel.add(table);
		tablePanel.add(scrollPane);
		
		// ADD ROW-PANELS TO MAIN PANEL
		
		// add second row
		
		c.gridx = 0;
		c.gridy = 1;
		mainPanel.add(secondRowPanel, c);
		
		// add table row
		
		c.gridx = 0;
		c.gridy = 2;
		mainPanel.add(tablePanel, c);
		
		// add fourth row
		
		c.gridx = 0;
		c.gridy = 3;
		mainPanel.add(fourthRowPanel, c);
		
		// add results components
		
		// center text in certain labels
		subTitleLabel4.setHorizontalAlignment(SwingConstants.CENTER);
		subTitleLabel5.setHorizontalAlignment(SwingConstants.CENTER);
		
		resultsPanel.add(subTitleLabel2);
		resultsPanel.add(subTitleLabel4);
		resultsPanel.add(subTitleLabel3);
		resultsPanel.add(subTitleLabel5);
		containerPanel.add(resultsPanel);
		c.gridx = 0;
		c.gridy = 4;
		mainPanel.add(containerPanel, c);
		
		// PACK THE FRAME
		frame.add(mainPanel);
		frame.pack();
		frame.setVisible(true);
		frame.setResizable(false);
	}
	
	/**
	 * Remove all rows from the GUI's table which are selected.
	 */
	public void removeSelectedRows(JTable table) {
		
		MyTableModel tempModel = (MyTableModel) this.table.getModel();
		Collection<TableRow> c = new ArrayList<TableRow>();
		
		int[] selection = table.getSelectedRows();
		for (int i = 0; i < selection.length; i++) {
			System.out.println(selection[i]);
			
			// TODO: Implement this method in future release
			
		}
	}
	
	/**
	 * Populate course instance variable with data inputed in the GUI by the user.
	 */
	public void generateCourseContentFromModel() {
		
		// empty the list if there was previous data
		this.course.emptyList();
		
		ArrayList<TableRow> tempRows = (ArrayList<TableRow>) this.model.getRows();
		
		// add the row info to the Course
		for (int i = 0; i < tempRows.size(); i++) {
			TableRow tempRow = tempRows.get(i);
			
			// get the info from the tempRow
			String itemParam = tempRow.getItem();
			double weightParam = tempRow.getWeight();
			double gradeParam = tempRow.getGrade();
			
			// create item with info from tempRow
			Item tempItem = new Item(itemParam, gradeParam, weightParam);
			
			// add the item to the Course
			this.course.addItem(tempItem);
		}
	}
	
	/**
	 * Add the action listeners for the buttons
	 */
	public void addListeners() {
		
		// button 2 - Calculate Weight and Final Mark
		this.button2.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent e) {
				
				// get the user input before calculating
				generateCourseContentFromModel();
				
				double finalMark = course.calculateFinalMark();
				double totalWeight = course.calculateTotalWeight();
				
				// multiply by 100 to convert to percentage format
				finalMark = finalMark * 100;
				totalWeight = totalWeight * 100;
				
				// subTitleLabel4 and subTitleLabel5
				subTitleLabel4.setText(Double.toString(totalWeight) + " %"); //$NON-NLS-1$
				subTitleLabel5.setText(Double.toString(finalMark) + " %"); //$NON-NLS-1$
			}
		});	
	}
	
	/**
	 * The main method to start the application.
	 */
	public static void main(String[] args) throws InterruptedException {
		new GUI();
	}
	
}
