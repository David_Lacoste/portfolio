package com.david.ui.table;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

@SuppressWarnings("serial")
public class MyTableModel extends AbstractTableModel {
	
	// CONSTANTS
	
	private String[] columnNames = {"Item", "Weight", "Grade", "Notes"};
	private List<TableRow> rows = new ArrayList<TableRow>();
	
	// Constructors
	
	public MyTableModel(ArrayList<TableRow> rows) {
		this.rows = rows;
	}
	
	public MyTableModel() {
		this.initializeModel();
	}
	
	public String[] getColumnNames() {
		return columnNames;
	}

	public void setColumnNames(String[] columnNames) {
		this.columnNames = columnNames;
	}

	public List<TableRow> getRows() {
		return rows;
	}

	public void setRows(List<TableRow> rows) {
		this.rows = rows;
	}

	/**
	 * Helper method used by constructor in the case where there is no data passed in.
	 * 
	 * Constructs a model with 10 rows.
	 */
	public void initializeModel() {
		
		// CREATE ROW DATA
		
		// create rows
		TableRow row1 = new TableRow("", 0, 0, "");
		TableRow row2 = new TableRow("", 0, 0, "");
		TableRow row3 = new TableRow("", 0, 0, "");
		TableRow row4 = new TableRow("", 0, 0, "");
		TableRow row5 = new TableRow("", 0, 0, "");
		TableRow row6 = new TableRow("", 0, 0, "");
		TableRow row7 = new TableRow("", 0, 0, "");
		TableRow row8 = new TableRow("", 0, 0, "");
		TableRow row9 = new TableRow("", 0, 0, "");
		TableRow row10 = new TableRow("", 0, 0, "");
		
		// add rows to data
		this.rows.add(row1);
		this.rows.add(row2);
		this.rows.add(row3);
		this.rows.add(row4);
		this.rows.add(row5);
		this.rows.add(row6);
		this.rows.add(row7);
		this.rows.add(row8);
		this.rows.add(row9);
		this.rows.add(row10);
	}
	
	/**
	 * Add new row to model
	 */
	public void addNewRowToModel() {
		// TODO: Implement this method in future release
	}
	
	/**
	 * Return the number of columns in the model
	 */
	public int getColumnCount() {
		return columnNames.length;
	}
	
	/**
	 * Return the number of rows in the model
	 */
	public int getRowCount() {
		//return dataOld.length;
		return rows.size();
	}
	
	/**
	 * Return the name of the column index passed as a parameter
	 */
	public String getColumnName(int column) {
		String name = "";
		
		switch (column) {
		case 0:
			name = columnNames[0];
			break;
		case 1:
			name = columnNames[1];
			break;
		case 2:
			name = columnNames[2];
			break;
		case 3:
			name = columnNames[3];
			break;
		}
		return name;
	}
	
	/**
	 * Return true if the cell, specified by the row and col parameters, is editable.
	 * 
	 * If not, return false.
	 */
	public boolean isCellEditable(int row, int col) {
		return true;
	}
	
	/**
	 * Return the class of the column index passed in as a parameter
	 */
	@SuppressWarnings("rawtypes")
	public Class<?> getColumnClass(int columnIndex) {
		Class type = String.class;
		
		switch (columnIndex) {
		case 0:
			type = String.class;
			break;
		case 1:
			type = Double.class;
			break;
		case 2:
			type = Double.class;
			break;
		case 3:
			type = String.class;
			break;
		}
		return type;
	}
	
	/**
	 * Return the Object in the specified rowIndex and columnIndex
	 */
	public Object getValueAt(int rowIndex, int columnIndex) {
		TableRow row = rows.get(rowIndex);
		Object value = null;
		
		switch (columnIndex) {
		case 0:
			value = row.getItem();
			break;
		case 1:
			value = row.getWeight();
			break;
		case 2:
			value = row.getGrade();
			break;
		case 3:
			value = row.getNotes();
			break;
		}
		return value;
	}
	
	/**
	 * Add value to the model at the specified rowIndex and columnIndex
	 */
	public void setValueAt(Object value, int rowIndex, int columnIndex) {
		
		// get the old data and place it into a temp row
		TableRow tempRow = this.rows.get(rowIndex);
		
		// modify the temp row
		switch (columnIndex) {
		case 0:
			tempRow.setItem((String) value);
			break;
		case 1:
			tempRow.setWeight(new Double(value.toString()));
			break;
		case 2:
			tempRow.setGrade(new Double(value.toString()));
			break;
		case 3:
			tempRow.setNotes((String) value);
			break;
		}
		
		// insert the temp row back into the original dataset
		this.rows.set(rowIndex, tempRow);
		
		// update the model with the new data
		fireTableCellUpdated(rowIndex, columnIndex);
	}
	
	/**
	 * Add a new row to the model with the values ["", 0, 0, ""].
	 * 
	 * The values are of type String, double, double, String, respectively.
	 */
	public void addRow() {
		this.rows.add(new TableRow("", 0, 0, ""));
		int row = this.rows.size() - 1;
		this.fireTableRowsInserted(row, row);
		
		// TODO: Further test this method
	}
	
}
