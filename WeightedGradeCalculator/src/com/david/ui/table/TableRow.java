package com.david.ui.table;

public class TableRow {
	private String item = "";
	private double weight = 0;
	private double grade = 0;
	private String notes = "";
	
	// Constructors
	
	public TableRow(String item, double weight, double grade, String notes) {
		this.item = item;
		this.weight = weight;
		this.grade = grade;
		this.notes = notes;
	}
	
	public TableRow() {}

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public double getGrade() {
		return grade;
	}

	public void setGrade(double grade) {
		this.grade = grade;
	}
}
